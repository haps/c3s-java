/**
 * 
 */
package org.c3s.eav;


/**
 * @author azajac
 *
 */
public interface Entity {

	public Entity addField(Field field);
	
	public Field getField(int fieldId);
	public Field getField(String fieldUnique);
	
	public Entity removeField(int fieldId);
	public Entity removeField(String fieldUnique);
	
	public Object export();

	public Field getParentField();
	public Entity setParentField();
	
	
	public int getTypeId();
	public int getTypeUniq();
	
	
	
}
