/**
 * 
 */
package org.c3s.eav.structure;

import org.c3s.site.LanguageInterface;

/**
 * @author hapsys
 *
 */
public interface NamesLangs {
	
	/**
	 * @return
	 */
	public String getName();
	
	/**
	 * @return
	 */
	public LanguageInterface getLanguage();
	
	/**
	 * @return
	 */
	public boolean isDefault();
}
