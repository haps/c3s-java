/**
 * 
 */
package org.c3s.eav.structure;

import java.util.List;

import org.c3s.site.LanguageInterface;
/**
 * @author hapsys
 *
 */
public interface AttributeDefinition {
	
	/**
	 * @return
	 */
	public String getUniqueName();
	
	/**
	 * @return
	 */
	public String getName();
	
	/**
	 * @param language
	 * @return
	 */
	public String getName(LanguageInterface language);
	
	
	
	/**
	 * @param languageUniqueName
	 * @return
	 */
	public String getName(String languageUniqueName);
	
	/**
	 * @return
	 */
	public List<NamesLangs> getNames();
	
	/**
	 * @return
	 */
	public AttributeTypeInterface getType();
	
	/**
	 * @return
	 */
	public boolean isNameLanguageIndependent();
	
	/**
	 * @return
	 */
	public boolean isValueLanguageIndependent();
	
}
