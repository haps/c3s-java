/**
 * 
 */
package org.c3s.eav.structure;


/**
 * @author hapsys
 *
 */
public interface TypeAttribute {

	/**
	 * @return
	 */
	public AttributeDefinition getAttribute();
	
	/**
	 * @return
	 */
	public boolean isUnique();
	
	/**
	 * @return
	 */
	public boolean isName();
	
	/**
	 * @return
	 */
	public boolean isRequired();
	
	/**
	 * @return
	 */
	public boolean isArray();
	
	/**
	 * @return
	 */
	public TypeAttribute getLinkedAttribute();
	
	/**
	 * @return
	 */
	public String getHandler();
	
	/**
	 * @return
	 */
	public String getSubType();
}
