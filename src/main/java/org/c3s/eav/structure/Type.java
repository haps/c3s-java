/**
 * 
 */
package org.c3s.eav.structure;

import java.util.List;

import org.c3s.site.LanguageInterface;

/**
 * @author hapsys
 * 
 */
public interface Type {

	/**
	 * @return
	 */
	public String getUniqueName();

	/**
	 * @return
	 */
	public String getName();

	/**
	 * @param language
	 * @return
	 */
	public String getName(LanguageInterface language);

	/**
	 * @param languageUniqueName
	 * @return
	 */
	public String getName(String languageUniqueName);

	/**
	 * @return
	 */
	public List<NamesLangs> getNames();
	
	
	/**
	 * @return
	 */
	public List<Type> getInvolvedTypes();
	
	/**
	 * @return
	 */
	public List<Type> getIncludedTypes();
	

	/**
	 * @return
	 */
	public List<TypeAttribute> getAttributes();
}
