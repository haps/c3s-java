/**
 * 
 */
package org.c3s.eav.structure;

/**
 * @author hapsys
 *
 */
public enum AttributeType implements org.c3s.eav.structure.AttributeTypeInterface {
	
	TYPE_UNKNOWN("unknown"),
	TYPE_STRING("string"),
	TYPE_TEXT("text"),
	TYPE_LIST("list"),
	;

	
	private String	name;
	//private Class<?> handler; 	
	
	private AttributeType(String name) {
		setTypeName(name);
	}
	
	protected void setTypeName(String name) {
		this.name = name;
	}
	
	@Override
	public String getTypeName() {
		// TODO Auto-generated method stub
		return name;
	}

	@Override
	public Object applyHandler(Object value) {
		// TODO Auto-generated method stub
		return null;
	}

	
	
}
