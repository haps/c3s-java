/**
 * 
 */
package org.c3s.eav.structure;

/**
 * @author azajac
 *
 */
public interface AttributeTypeInterface {
	
	/**
	 * @return
	 */
	public String getTypeName();
	
	public Object applyHandler(Object value);
	
}
