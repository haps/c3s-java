/**
 * 
 */
package org.c3s.eav;

import java.util.List;

import org.c3s.eav.structure.AttributeTypeInterface;

/**
 * @author azajac
 *
 */
public interface Field {

	/**
	 * @return
	 */
	public String getLabel();
	/**
	 * @return
	 */
	public String getName();
	/**
	 * @return
	 */
	public String getCommonName();
	
	/**
	 * @return
	 */
	public boolean isItemName();
	/**
	 * @return
	 */
	public boolean isItemUniq();
	/**
	 * @return
	 */
	public boolean isRequired();
	/**
	 * @return
	 */
	public boolean isArray();
	
	/**
	 * @return
	 */
	public Object getValue();
	/**
	 * @param index
	 * @return
	 */
	public Object getValue(int index);
	/**
	 * @param index
	 * @param skipErrors
	 * @return
	 */
	public Object getValue(int index, boolean skipErrors);
	
	
	/**
	 * @return
	 */
	public List<Object> getValues();
	
	
	/**
	 * @return
	 */
	public Field removeValue();
	/**
	 * @param index
	 * @return
	 */
	public Field removeValue(int index);
	
	
	/**
	 * @param value
	 * @return
	 */
	public Field appendValue(Object value);
	/**
	 * @param value
	 * @param index
	 * @return
	 */
	public Field appendValue(Object value, int index);
	
	
	/**
	 * @param type
	 * @return
	 */
	public Field setAttributeType(AttributeTypeInterface type);
	/**
	 * @return
	 */
	public AttributeTypeInterface getAttributeType();
	

	/**
	 * @param exportType
	 * @return
	 */
	public Object export(Object exportType);
	/**
	 * @param source
	 */
	public void importt(Object source);
}
