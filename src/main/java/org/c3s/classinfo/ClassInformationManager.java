package org.c3s.classinfo;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ClassInformationManager {
	
	private static Map<Class<?>, ClassInformation> classes = new ConcurrentHashMap<Class<?>, ClassInformation>();  

	public static ClassInformation getClassInformation(Class<?> clazz) {
		if (!classes.containsKey(clazz)) {
			classes.put(clazz, new ClassInformation(clazz));
		}
		return classes.get(clazz);
	}

	public static ClassInformation getClassInformation(String className) throws ClassNotFoundException {
		return getClassInformation((Class<?>)Class.forName(className));
	}
}
