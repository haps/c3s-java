package org.c3s.classinfo;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class ClassInformation {

	private Class<?> clazz;
	
	private Map<String, Method> methods = new HashMap<String, Method>();
	
	
	/**
	 * @param clazz
	 */
	public ClassInformation(Class<?> clazz) {
		this.clazz = clazz;
	}

	/**
	 * @return
	 */
	public Annotation[] getClassAnnotations() {
		return clazz.getAnnotations();
	}
	
	/**
	 * @param name
	 * @return
	 */
	public Method getMethod(String name) {
		Method method = methods.get(name);
		if (method == null) {
			prepareMethodInfo(name);
			method = methods.get(name);
		}
		return method;
	}

	/**
	 * @param name
	 * @return
	 */
	public Annotation[] getMethodAnnotations(String name) {
		Method method = methods.get(name);
		if (method == null) {
			prepareMethodInfo(name);
			method = methods.get(name);
		}
		return method.getAnnotations();
	}
	
	
	/**
	 * @param name
	 * @return
	 */
	public Annotation[][] getMethodParameterAnnotations(String name) {
		Method method = methods.get(name);
		if (method == null) {
			prepareMethodInfo(name);
			method = methods.get(name);
		}
		return method.getParameterAnnotations();
	}
	
	/**
	 * @param name
	 * @return
	 */
	public Class<?> getMethodReturnType(String name) {
		Method method = methods.get(name);
		if (method == null) {
			prepareMethodInfo(name);
			method = methods.get(name);
		}
		return method.getReturnType();
	}
	
	/**
	 * @param name
	 * @return
	 */
	public Class<?>[] getMethodParameterTypes(String name) {
		Method method = methods.get(name);
		if (method == null) {
			prepareMethodInfo(name);
			method = methods.get(name);
		}
		return method.getParameterTypes();
	}
	
	/**
	 * @param name
	 */
	private void prepareMethodInfo(String name) {
		synchronized (methods) {
			for (Method method: clazz.getMethods()) {
				if (name.equals(method.getName())) {
					methods.put(name, method);
					break;
				}
			}
		}
	}
}
