package org.c3s.lib.parsers;

import java.util.LinkedList;
import java.util.List;

public abstract class AbstractParser {

	protected List<Holder> callbacks = new LinkedList<Holder>(); 
	
	public void registerHandler(InterceptorInterface interceptor, Callable call) {
		callbacks.add(new Holder(interceptor, call));
	}
	
	protected void processInterceptors(Object parameter) throws Exception {
		for(Holder callback: callbacks) {
			if (callback.getInterceptor().check(parameter)) {
				if (!callback.getCall().call(parameter)) {
					break;
				}
			}
		}
	}	
	
	abstract public void process(Object source) throws Exception;
	protected abstract void onStart(Object source) throws Exception;
	protected abstract void onStop(Object source) throws Exception;
	
	private class Holder {
		
		InterceptorInterface interceptor;
		
		public InterceptorInterface getInterceptor() {
			return interceptor;
		}

		Callable call;
		
		public Callable getCall() {
			return call;
		}

		public Holder(InterceptorInterface interceptor, Callable call) {
			this.interceptor = interceptor;
			this.call = call;
		}
	}
}
