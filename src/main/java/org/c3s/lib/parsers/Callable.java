package org.c3s.lib.parsers;

import java.lang.reflect.Method;

public class Callable {
	
	private Object obj;
	private String method;
	
	public Callable(Object obj, String method) {
		this.obj = obj;
		this.method = method;
	}
	
	public boolean call(Object param) throws Exception {
		
		Class<?>[] CallMethodArgsTypes = {param.getClass()};
		
		Method meth = obj.getClass().getDeclaredMethod(method, CallMethodArgsTypes);
		meth.setAccessible(true);
		boolean result = ((Boolean)meth.invoke(obj, param)).booleanValue(); 
		
		return result;
	}

}
