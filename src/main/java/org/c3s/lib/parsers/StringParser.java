package org.c3s.lib.parsers;

public class StringParser extends AbstractParser {
	protected String source = "";
	protected int index = 0;

	@Override
	public void process(Object source) throws Exception {
		this.source = source.toString();
		this.onStart(this.source);
		for (index = 0; index < this.source.length(); index++) {
			this.processInterceptors(this.source.charAt(index));
		}
		this.onStop(this.source);
	}

	protected void error(char ch) {
		throw new RuntimeException("Char '" + ch + "' not avaiable at position " + (index+1));
	}
	
	@Override
	protected void onStart(Object source) {
	}
	@Override
	protected void onStop(Object source) {
	}
	
}
