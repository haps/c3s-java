package org.c3s.lib.parsers;

public interface InterceptorInterface {
	public boolean check(Object param);
}
