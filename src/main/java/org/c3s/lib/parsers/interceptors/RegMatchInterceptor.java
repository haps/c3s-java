package org.c3s.lib.parsers.interceptors;

import java.util.ArrayList;

import org.c3s.lib.parsers.InterceptorInterface;
import org.c3s.utils.RegexpUtils;

public class RegMatchInterceptor implements InterceptorInterface {
	
	private String regexp;
	
	public RegMatchInterceptor(String regexp) {
		this.regexp = regexp;
	}

	@Override
	public boolean check(Object param) {
		return RegexpUtils.preg_match(regexp, param.toString(), new ArrayList<String>());
	}


}
