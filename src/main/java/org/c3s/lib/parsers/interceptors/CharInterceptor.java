package org.c3s.lib.parsers.interceptors;

import org.c3s.lib.parsers.InterceptorInterface;

public class CharInterceptor implements InterceptorInterface {

	private String chars;
	private boolean charCase;
	
	public CharInterceptor(String chars, boolean charCase) {
		this.chars= chars;
		this.charCase = charCase;
	}

	public CharInterceptor(String chars) {
		this(chars, false);
	}
	
	public CharInterceptor(char[] chars, boolean charCase) {
		this(new String(chars), charCase);
	}
	
	public CharInterceptor(char[] chars) {
		this(new String(chars), false);
	}
	
	@Override
	public boolean check(Object param) {
		char ch = ((Character)param).charValue();
		return (chars.length() > 0) && (charCase && chars.indexOf(ch) > -1 || !charCase && chars.toUpperCase().indexOf(Character.toUpperCase(ch)) > -1);
	}

}
