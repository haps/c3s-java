/**
 * 
 */
package org.c3s.xml;

import org.c3s.exceptions.XMLException;
import org.w3c.dom.Document;

/**
 * @author azajac
 *
 */
public interface DocumentInjector {

	public void inject(Document doc) throws XMLException;
	
}
