/**
 * 
 */
package org.c3s.xml;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.xml.transform.Transformer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.c3s.exceptions.XMLException;
import org.c3s.utils.FileSystem;
import org.c3s.utils.Utils;
import org.c3s.xml.utils.XMLUtils;
import org.w3c.dom.Document;

/**
 * @author azajac
 *
 */
public class XMLManager {

	private static Logger logger = LoggerFactory.getLogger(XMLManager.class);
	
	private static Map<String, Transformer>  map = new HashMap<String, Transformer>();
	private static Map<String, Document>  docmap = new HashMap<String, Document>();

	/**
	 * 
	 * @param file
	 * @return
	 * @throws XMLException
	 */
	public static Transformer getTransformer(File file) throws XMLException {
		Transformer transformer = null; 
		
		String path = file.getAbsolutePath();
		String hash = getFileHash(file);

		if (map.containsKey(hash)) {
			transformer = map.get(hash);
		} else {
				logger.info("Reload XSL file: \"" + path + "\"");
				transformer = XMLUtils.getTransformerForFile(file);
				map.put(hash, transformer);
		}
		
		return transformer;
	}
	/**
	 * 
	 * @param filename
	 * @return
	 * @throws XMLException
	 */
	public static Transformer getTransformer(String filename) throws XMLException {
		return getTransformer(FileSystem.newFile(filename));
	}
	
	/**
	 * @param file
	 * @return
	 * @throws XMLException
	 */
	public static Document getDocument(File file) throws XMLException {
		Document doc = null; 
		
		String path = file.getAbsolutePath();

		String hash = getFileHash(file);
		
		if (docmap.containsKey(hash)) {
			doc = docmap.get(hash);
		} else {
			logger.info("Reload XML file: \"" + path + "\"");
			doc = XMLUtils.load(file);
			docmap.put(hash, doc);
		}
		
		return doc;
	}
	
	public static Document getDocument(String filename) throws XMLException {
		synchronized (filename) {
			return getDocument(FileSystem.newFile(filename));
		}
	}
	
	
	private static String getFileHash(File file) {
		String path = file.getAbsolutePath();
		String size = Long.toString(file.length());
		String modify = Long.toString(file.lastModified());
		return Utils.MD5(path + size + modify);
	}
	
	public static boolean isDocumentLoaded(String filename) {
		synchronized (filename) {
			return isDocumentLoaded(FileSystem.newFile(filename));
		}
	}
	
	public static boolean isDocumentLoaded(File file) {
		String hash = getFileHash(file);
		return docmap.containsKey(hash);
	}
}
