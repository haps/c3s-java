/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.c3s.xml;

import java.util.HashMap;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.c3s.exceptions.XMLException;
import org.c3s.utils.Utils;
import org.w3c.dom.Node;

/**
 * 
 * @author azajac
 */
public class DecorXPath {
	protected Map<String, XPathExpression> exps = new HashMap<String, XPathExpression>();
	protected XPath xpath;

	public DecorXPath(XPath xpath) {
		this.xpath = xpath;
	}

	public DecorXPath() {
		this(XPathFactory.newInstance().newXPath());
	}

	public Object query(String expr, Node input, QName type) throws XMLException {
		Object ret = null;
		try {
			String hash = Utils.MD5(expr);
			XPathExpression exp;
			if (!exps.containsKey(hash)) {
				exp = xpath.compile(expr);
				exps.put(hash, exp);
			} else {
				exp = exps.get(hash);
			}

			ret = exp.evaluate(input, type);
		} catch (XPathExpressionException e) {
			throw new XMLException(e);
		}

		return ret;
	}
}
