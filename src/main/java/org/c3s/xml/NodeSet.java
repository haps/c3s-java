package org.c3s.xml;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class NodeSet implements List<Node> {

	NodeList nodes;
	
	public NodeSet(NodeList nodes) {
		this.nodes = nodes;
	}

	@Override
	public int size() {
		return nodes.getLength();
	}

	@Override
	public boolean isEmpty() {
		return nodes == null || size() == 0;
	}

	@Override
	public boolean contains(Object o) {
		return false;
	}

	@Override
	public Iterator<Node> iterator() {
		return new InternalListIterator(nodes, 0);
	}

	@Override
	public Object[] toArray() {
		Node[] list = new Node[nodes.getLength()];
		for(int i = 0; i < nodes.getLength(); i++) {
			list[i] = nodes.item(i);
		}
		return null;
	}

	@Override
	public <T> T[] toArray(T[] a) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean add(Node e) {
		return false;
	}

	@Override
	public boolean remove(Object o) {
		return false;
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return false;
	}

	@Override
	public boolean addAll(Collection<? extends Node> c) {
		return false;
	}

	@Override
	public boolean addAll(int index, Collection<? extends Node> c) {
		return false;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Node get(int index) {
		// TODO Auto-generated method stub
		return nodes.item(index);
	}

	@Override
	public Node set(int index, Node element) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void add(int index, Node element) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Node remove(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int indexOf(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int lastIndexOf(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ListIterator<Node> listIterator() {
		return new InternalListIterator(nodes, 0);
	}

	@Override
	public ListIterator<Node> listIterator(int index) {
		return new InternalListIterator(nodes, index);
	}

	@Override
	public List<Node> subList(int fromIndex, int toIndex) {
		// TODO Auto-generated method stub
		return null;
	}

	
	private class InternalListIterator implements ListIterator<Node> {

		NodeList nodes;
		int i=0;
		
		public InternalListIterator(NodeList nodes, int start_index) {
			this.nodes = nodes;
			this.i = start_index;
		}
		
		@Override
		public boolean hasNext() {
			return i < nodes.getLength();
		}

		@Override
		public Node next() {
			// TODO Auto-generated method stub
			Node node = nodes.item(i);
			i++;
			return node;
		}

		@Override
		public boolean hasPrevious() {
			// TODO Auto-generated method stub
			return i > 0;
		}

		@Override
		public Node previous() {
			Node node = nodes.item(i);
			i--;
			return node;
		}

		@Override
		public int nextIndex() {
			// TODO Auto-generated method stub
			return i+1;
		}

		@Override
		public int previousIndex() {
			// TODO Auto-generated method stub
			return i-1;
		}

		@Override
		public void remove() {
			// TODO Auto-generated method stub
		}

		@Override
		public void set(Node e) {
			// TODO Auto-generated method stub
		}

		@Override
		public void add(Node e) {
			// TODO Auto-generated method stub
		}
		
	}
}
