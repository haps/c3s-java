/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.c3s.xml.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.URIResolver;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.c3s.db.beans.DbBean;
import org.c3s.exceptions.XMLException;
import org.c3s.reflection.XMLReflectionObj;
import org.c3s.utils.FileSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 * 
 * @author admin
 */
public class XMLUtils {
	
	private static Logger logger = LoggerFactory.getLogger(XMLUtils.class);
	/**
	 * @param root
	 * @return
	 * @throws XMLException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	public static Document createXML(String root) throws XMLException {
		return createXML(root, "utf-8");
	}

	/**
	 * @param root
	 * @param enc
	 * @return
	 * @throws XMLException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	public static Document createXML(String root, String enc) throws XMLException {
		/*
		 * Document xml =
		 * DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
		 * Element root_elm = xml.createElement(root); xml.setXmlVersion("1.0");
		 * xml.appendChild(root_elm);
		 */
		Document xml = loadXML("<?xml version=\"1.0\" encoding=\"" + enc + "\"?><" + root + "/>");
		return xml;
	}

	/**
	 * @return
	 */
	public static XPath newXPath() {
		return XPathFactory.newInstance().newXPath();
	}

	/**
	 * @param inputFile
	 * @return
	 * @throws XMLException
	 */
	public static Document load(String inputFile) throws XMLException {
		return load(FileSystem.newFile(inputFile));
	}

	/**
	 * @param inputFile
	 * @return
	 * @throws XMLException
	 * @throws XMLException
	 */
	public static Document load(File inputFile) throws XMLException {
		Document xml = null;
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		DocumentBuilder db;
		try {
			db = factory.newDocumentBuilder();
			xml = db.parse(inputFile);
		} catch (Exception e) {
			throw new XMLException(e);
		}
		return xml;
	}

	/**
	 * @param xmlString
	 * @return
	 * @throws XMLException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	public static Document loadXML(String xmlString) throws XMLException {
		byte[] inputArray = xmlString.getBytes();
		Document xml = null;
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		try {
			DocumentBuilder db = factory.newDocumentBuilder();
			xml = db.parse(new ByteArrayInputStream(inputArray));
		} catch (Exception e) {
			throw new XMLException(e);
		}
		return xml;
	}

	/**
	 * @param xml
	 * @param props
	 * @return
	 * @throws XMLException
	 */
	public static String saveXML(Document xml, Map<String, Object> props) throws XMLException {
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		saveXMLIntoSream(xml, buffer, props);
		try {
			return buffer.toString(xml.getXmlEncoding());
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			throw new XMLException(e);
		}
	}

	/**
	 * @param xml
	 * @return
	 * @throws XMLException
	 */
	public static String saveXML(Document xml) throws XMLException {
		Map<String, Object> props = new HashMap<String, Object>();
		props.put(OutputKeys.INDENT, "yes");
		return saveXML(xml, props);
	}

	/**
	 * @param xml
	 * @param file
	 * @param props
	 * @throws XMLException
	 */
	public static void save(Document xml, File file, Map<String, Object> props) throws XMLException {
		try {
			FileOutputStream buffer = new FileOutputStream(file);
			saveXMLIntoSream(xml, buffer, props);
		} catch (Exception e) {
			throw new XMLException(e);
		}
	}

	/**
	 * @param xml
	 * @param file
	 * @throws XMLException
	 */
	public static void save(Document xml, File file) throws XMLException {
		save(xml, file, null);
	}

	/**
	 * @param xml
	 * @param file
	 * @throws XMLException
	 */
	public static void save(Document xml, String file) throws XMLException {
		save(xml, file, null);
	}

	/**
	 * @param xml
	 * @param file
	 * @param props
	 * @throws XMLException
	 */
	public static void save(Document xml, String file, Map<String, Object> props) throws XMLException {
		save(xml, FileSystem.newFile(file), props);
	}

	/**
	 * @param xml
	 * @param buffer
	 * @param props
	 * @throws XMLException
	 */
	protected static void saveXMLIntoSream(Document xml, OutputStream buffer, Map<String, Object> props) throws XMLException {

		try {
			DOMSource domSource = new DOMSource(xml);
			TransformerFactory factory = TransformerFactory.newInstance();
			Transformer transformer = factory.newTransformer();

			String defencoding = transformer.getOutputProperty(OutputKeys.ENCODING);
			transformer.setOutputProperty(OutputKeys.ENCODING, xml.getXmlEncoding());
			//System.out.println(xml.getXmlEncoding());

			StreamResult result = new StreamResult(buffer);

			if (props != null) {
				Properties tranProps = new Properties();
				tranProps.putAll(props);
				transformer.setOutputProperties(tranProps);
			}
			transformer.transform(domSource, result);
			if (defencoding != null) {
				transformer.setOutputProperty(OutputKeys.ENCODING, defencoding);
			}
		} catch (Exception e) {
			throw new XMLException(e);
		}

	}

	/*
	 * 
	 * Transformation
	 */

	public static Transformer getTransformerForFile(final File file) throws XMLException {
		try {
			Document xsl = load(file);
			DOMSource xslSource = new DOMSource(xsl);
			TransformerFactory factory = TransformerFactory.newInstance();
			factory.setURIResolver(new URIResolver() {
				@Override
				public Source resolve(String href, String base) throws TransformerException {
					Document dom = null;
					DOMSource source = null;
					try {
						dom = load(new File(file.getParentFile(), href));
						source = new DOMSource(dom);
					} catch (XMLException e) {
						new TransformerException(e);
					}
					return source;
				}
			});
			Transformer transformer = factory.newTransformer(xslSource);
			return transformer;
			
		} catch (Exception e) {
			throw new XMLException(e);
		}
		
	}
	
	/**
	 * @param xml
	 * @param xsl
	 * @param parameters
	 * @return
	 * @throws XMLException 
	 */
	public static String transformXML(Document xml, Document xsl, Map<String, Object> parameters) throws XMLException {
		String result = null;

		try {

			// System.out.println(xsl.getDocumentElement().getTagName());

			DOMSource xslSource = new DOMSource(xsl);

			TransformerFactory factory = TransformerFactory.newInstance();
			Transformer transformer = factory.newTransformer(xslSource);

			result = transformXML(xml, transformer, parameters);

		} catch (Exception e) {
			throw new XMLException(e);
		}

		return result;
	}

	public static String transformXML(Document xml, Transformer transformer, Map<String, Object> parameters) throws XMLException {
		String result = null;
		try {

			DOMSource domSource = new DOMSource(xml);
			/*
			 * Set Parameters
			 */
			if (parameters != null) {
				for (String param_name : parameters.keySet()) {
					logger.debug(param_name + " {" + parameters.get(param_name) + "}");
					transformer.setParameter(param_name, parameters.get(param_name));
				}
			}
			/*
			 * Transformation
			 */
			ByteArrayOutputStream buffer = new ByteArrayOutputStream();
			StreamResult streamresult = new StreamResult(buffer);
			transformer.transform(domSource, streamresult);
			result = buffer.toString(transformer.getOutputProperty(OutputKeys.ENCODING));

		} catch (Exception e) {
			throw new XMLException(e);
		}
		return result;
	}

	/**
	 * @param xml
	 * @param xsl_file
	 * @param parameters
	 * @return
	 * @throws XMLException
	 */
	public static String transformXML(Document xml, String xsl_file, Map<String, Object> parameters) throws XMLException {
		return transformXML(xml, load(xsl_file), parameters);
	}

	/**
	 * @param xml
	 * @param xsl_file
	 * @return
	 * @throws XMLException
	 */
	public static String transformXML(Document xml, String xsl_file) throws XMLException {
		return transformXML(xml, load(xsl_file), null);
	}

	/**
	 * @param xml
	 * @param xsl
	 * @return
	 * @throws XMLException 
	 */
	public static String transformXML(Document xml, Document xsl) throws XMLException {
		return transformXML(xml, xsl, null);
	}

	/*
	 *
	 */

	/**
	 * @param str
	 * @return
	 */
	public static String xml2out(String str) {
		String res = str.replace("<", "&lt;");
		res = res.replace(">", "&gt;");
		return res;
	}

	/**
	 * @param xml
	 * @return
	 * @throws XMLException
	 */
	public static String xml2out(Document xml) throws XMLException {
		return xml2out(saveXML(xml));
	}

	/**
	 * @param target
	 * @param child
	 */
	public static void appendClonedNode(Node target, Node child) {
		target.appendChild(target.getOwnerDocument().importNode(child, true));
	}

	/**
	 * @param target
	 * @param child
	 */
	public static void appendClonedNode(Document target, Document child) {
		target.getDocumentElement().appendChild(target.importNode(child.getDocumentElement(), true));
	}
	
	/**
	 * @param target
	 * @param child
	 */
	public static void appendClonedNode(Document target, Node child) {
		target.getDocumentElement().appendChild(target.importNode(child, true));
	}
	
	/**
	 * @param target
	 * @param child
	 */
	public static void appendClonedNode(Node target, Document child) {
		target.appendChild(target.getOwnerDocument().importNode(child.getDocumentElement(), true));
	}
	
	/**
	 * @param nodes
	 * @param root
	 * @return
	 * @throws XMLException
	 */
	public static Node listToNode(List<Node> nodes, String root) throws XMLException {
		Document xml = createXML(root);

		for (Node node : nodes) {
			appendClonedNode(xml.getDocumentElement(), node);
		}
		return xml.getDocumentElement();
	}

	/**
	 * @param nodes
	 * @return
	 * @throws XMLException
	 */
	public static Node listToNode(List<Node> nodes) throws XMLException {
		return listToNode(nodes, "data");
	}

	public static Document arrayToXml(Object array) throws XMLException {
		return arrayToXml(array, null, "data", true);
	}
	
	public static Document arrayToXml(Object array, boolean to_lower) throws XMLException {
		return arrayToXml(array, null, "data", to_lower);
	}
	
	@SuppressWarnings("unchecked")
	public static Document arrayToXml(Object array, Element parent_element, String element_name, boolean to_lower) throws XMLException {
		Document dom = null;
		boolean ret_dom = false;
		
		if (parent_element == null) {
			ret_dom = true;
			dom = XMLUtils.createXML(element_name);
			parent_element = dom.getDocumentElement();
		}
		
		if (array instanceof List<?>) {
			Element itemlist = parent_element.getOwnerDocument().createElement("itemlist");
			for (Object data: (List<Object>)array) {
				arrayToXml(data, itemlist, "", to_lower);
			}
			itemlist.setAttribute("name", to_lower?element_name.toLowerCase():element_name);
			parent_element.appendChild(itemlist);
		} else if (array instanceof Map<?,?>) {
			Element item = parent_element.getOwnerDocument().createElement("item");
			for (String key: ((Map<String, Object>)array).keySet()) {
				Object data = ((Map<String, Object>)array).get(key);
				arrayToXml(data, item, key, to_lower);
			}
			parent_element.appendChild(item);
		/**
		} else if (array instanceof String) {
			Element field = parent_element.getOwnerDocument().createElement("field");
			field.setAttribute("name", to_lower?element_name.toLowerCase():element_name);
			field.setAttribute("value", to_lower?array.toString().toLowerCase():array.toString());
		**/
		} else if (array instanceof DbBean) {
			try {
				appendClonedNode(parent_element, new XMLReflectionObj(array, true).toXML());
			} catch (Exception e) {
				throw new XMLException(e); 
			}
		} else {
			Element field = parent_element.getOwnerDocument().createElement("field");
			field.setAttribute("name", to_lower?element_name.toLowerCase():element_name);
			field.setAttribute("value", to_lower?array.toString().toLowerCase():array.toString());
			parent_element.appendChild(field);
		}
		
		if (ret_dom) {
			return dom;
		}
		return null;
	}
	
	
	@SuppressWarnings("unchecked")
	public static Document arrayToXml1(Object array, Element parent_element, String element_name, boolean to_lower) throws XMLException {
		Document dom = null;
		boolean ret_dom = false;
		
		if (parent_element == null) {
			ret_dom = true;
			dom = XMLUtils.createXML(element_name);
			parent_element = dom.getDocumentElement();
		}
		
		if (array instanceof List<?>) {
			Integer key = 0;
			for (Object data: (List<Object>)array) {
				if (data instanceof List<?>) {
					Element item = parent_element.getOwnerDocument().createElement("item");
					arrayToXml(data, item, "", to_lower);
					parent_element.appendChild(item);
				} else if (data instanceof Map<?,?>) {
					Element itemlist = parent_element.getOwnerDocument().createElement("itemlist");
					arrayToXml(data, itemlist, "", to_lower);
					itemlist.setAttribute("name", to_lower?element_name.toLowerCase():element_name);
					parent_element.appendChild(itemlist);
				} else {
					Element node = parent_element.getOwnerDocument().createElement("field");
					arrayToXml(data, node, key.toString(), to_lower);
					parent_element.appendChild(node);
				}
				key++;
			}
			
		} else if (array instanceof Map<?,?>) {
			for (String key: ((Map<String, Object>)array).keySet()) {
				Object data = ((Map<String, Object>)array).get(key);
				if (data instanceof List<?>) {
					Element item = parent_element.getOwnerDocument().createElement("item");
					arrayToXml(data, item, "", to_lower);
					parent_element.appendChild(item);
				} else if (data instanceof Map<?,?>) {
					Element itemlist = parent_element.getOwnerDocument().createElement("itemlist");
					arrayToXml(data, itemlist, "", to_lower);
					itemlist.setAttribute("name", to_lower?element_name.toLowerCase():element_name);
					parent_element.appendChild(itemlist);
				} else {
					Element node = parent_element.getOwnerDocument().createElement("field");
					arrayToXml(data, node, key, to_lower);
					parent_element.appendChild(node);
				}
			}
			
		} else if (array instanceof String) {
			parent_element.setAttribute("name", to_lower?element_name.toLowerCase():element_name);
			parent_element.setAttribute("value", to_lower?array.toString().toLowerCase():array.toString());
		} else {
			parent_element.setAttribute("name", to_lower?element_name.toLowerCase():element_name);
		}
		
		if (ret_dom) {
			return dom;
		}
		return null;
	}
}
