package org.c3s.exceptions;

public abstract class AbstractException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6329980609685137188L;

	public AbstractException(Exception ex) {
		super(ex.getMessage(), ex);
	}
}
