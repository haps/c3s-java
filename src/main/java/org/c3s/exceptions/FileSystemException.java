/**
 * 
 */
package org.c3s.exceptions;

/**
 * @author azajac
 *
 */
public class FileSystemException extends AbstractException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5089229567896647090L;

	/**
	 * @param cause
	 */
	public FileSystemException(Exception ex) {
		super(ex);
	}

}
