package org.c3s.validator;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class CommonValidator<T> {

	protected T bean = null;
	
	@SuppressWarnings("unchecked")
	public final Map<String, List<String>> validate(T bean) {
		Map<String, List<String>> result = null;
		this.bean = bean;
		Field[] fields = bean.getClass().getDeclaredFields();
		for (Field field: fields) {
			String methodName = "validate" + field.getName().substring(0,1).toUpperCase() + field.getName().substring(1);
			try {
				Method method = this.getClass().getMethod(methodName);
				method.setAccessible(true);
				List<String> errors = (List<String>)method.invoke(this);
				if (errors != null && errors.size() > 0) {
					if (result == null) {
						result = new HashMap<String, List<String>>();
					}
					result.put(field.getName(), errors);
				}
			} catch (Exception e) {
				;
			}
		}
		
		return result;
	}
}
