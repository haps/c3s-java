/**
 * 
 */
package org.c3s.reflection;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.c3s.exceptions.XMLException;
import org.c3s.reflection.annotation.XMLFieldList;
import org.c3s.reflection.annotation.XMLFieldMap;
import org.c3s.reflection.annotation.XMLReflectionField;
import org.c3s.reflection.annotation.XMLSimple;
import org.c3s.xml.utils.XMLUtils;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * @author azajac
 * 
 */

public class XMLReflectionObj {

	private static Map<Class<?>, List<Field>> cache = new ConcurrentHashMap<Class<?>, List<Field>>();
	
	private Object object;
	
	public XMLReflectionObj() {
		super();
		object = this;
	}

	public XMLReflectionObj(Object object) {
		super();
		this.object = object;
	}
	
	public XMLReflectionObj(Object object, boolean isUsePrivate) {
		super();
		this.object = object;
		this.isUsePrivate = isUsePrivate; 
	}
	
	private boolean isUsePrivate = true;
	//private boolean isUseMethod = false;
	
	private Document xml;
	protected Document getInternalXML() {
		return xml;
	}

	protected Field[] getAllFields() {
		return getAllFields(object.getClass());
	}

	protected Field[] getAllFields(Class<?> cls) {
		return getAllFieldsList(cls).toArray(new Field[] {});
	}
	/**
	 * 
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	protected List<Field> getAllFieldsList(Class cls) {

		if (cls == Object.class) {
			return new ArrayList<Field>();
		}
		
		List<Field> fields = cache.get(cls);
		
		if (fields == null) {
			fields = getAllFieldsList(cls.getSuperclass()); 
			Field[] fs = cls.getDeclaredFields();
			for (Field field : fs) {
				if (field.getAnnotation(XMLSimple.class) != null || 
						field.getAnnotation(XMLFieldMap.class) != null ||
						field.getAnnotation(XMLFieldList.class) != null ||
						field.getAnnotation(XMLReflectionField.class) != null
						) {
					fields.add(field);
				}
			}
		}

		return fields;
	}
	
	/**
	 * 
	 * @return
	 * @throws XMLException 
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 * @throws DOMException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	
	public Document toXML() throws XMLException, IllegalArgumentException, IllegalAccessException, DOMException, ParserConfigurationException, SAXException, IOException {

		//Document xml = XMLUtils.createXML("item");
		xml = XMLUtils.createXML("item");
		Element root = xml.getDocumentElement();
		root.setAttribute("class", object.getClass().getName());

		// Field[] fields = object.getClass().getFields();
		Field[] fields = getAllFields();
		//Method[] methods = isUseMethod ? object.getClass().getDeclaredMethods() : null;

		for (Field field : fields) {
			// if ((field.getModifiers() & Modifier.FINAL) == 0) {
			if (isUsePrivate || (field.getModifiers() & Modifier.PRIVATE) == 0) {
				field.setAccessible(true);
				
				Element elm = null;
				if (field.getAnnotation(XMLSimple.class) != null) {
					elm = getSimpleField(field);
				} else if (field.getAnnotation(XMLFieldMap.class) != null) {
					elm = getMapField(field);
				} else if (field.getAnnotation(XMLFieldList.class) != null) {
					elm = getListField(field);
				} else if (field.getAnnotation(XMLReflectionField.class) != null) {
					elm = getReflectionField(field);
				}
				
				if (elm != null) {
					root.appendChild(elm);
				}
			}
			// }
		}

		return xml;
	}

	/**
	 * 
	 * @return
	 * @throws XMLException 
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 * @throws DOMException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	
	public Document __toXML() throws XMLException, IllegalArgumentException, DOMException, IllegalAccessException, ParserConfigurationException, SAXException, IOException  {
		return toXML();
	}

	/**
	 * 
	 * @param field
	 * @return
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	protected Element getSimpleField(Field field) throws IllegalArgumentException, IllegalAccessException {
		
		Element elm = xml.createElement("field");
		String name = field.getAnnotation(XMLSimple.class).value();
		if (name.length() > 0) {
			elm.setAttribute("name", name);
		} else {
			elm.setAttribute("name", field.getName());
		}
		if (field.getType().isArray()) {
			if (field.get(object) != null) {
				Object[] values = (Object[])field.get(object);
				for (Object obj: values) {
					if (obj != null) {
						String value = obj.toString();
						Element val = xml.createElement("value");
						val.setAttribute("value", value);
						String _class = obj.getClass().getName();
						val.setAttribute("class", _class);
						elm.appendChild(val);
					}
				}
			}
		} else {
			String value = "";
			if (field.get(object) != null) {
				value = field.get(object).toString();
			}
			elm.setAttribute("value", value);
			String _class = field.getType().getName();
			elm.setAttribute("class", _class);
		}
		
		
		return elm;
	}
	
	/**
	 * 
	 * @param field
	 * @return
	 * @throws XMLException 
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws DOMException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	protected Element getListField(Field field) throws XMLException, IllegalArgumentException, IllegalAccessException {
		
		Element elm = null;
		
		String name = field.getAnnotation(XMLFieldList.class).value();
		if (name.length() == 0) {
			name = field.getName();
		}
		field.setAccessible(true);
		if (field.get(object) != null) {
			Document doc = new XMLList((List<Object>)field.get(object), isUsePrivate).toXML(name);
			elm = (Element)xml.importNode(doc.getDocumentElement(), true);
		}
		return elm;
	}
	
	/**
	 * 
	 * @param field
	 * @return
	 * @throws XMLException 
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws DOMException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	protected Element getMapField(Field field) throws XMLException, IllegalArgumentException, IllegalAccessException {
		
		Element elm = null;
		
		String name = field.getAnnotation(XMLFieldMap.class).value();
		if (name.length() == 0) {
			name = field.getName();
		}
		if (field.get(object) != null) {
			Document doc = new XMLMap((Map<?,?>)field.get(object), isUsePrivate).toXML(name);
			elm = (Element)xml.importNode(doc.getDocumentElement(), true);
		}
		return elm;
	}
	
	protected Element getReflectionField(Field field) throws XMLException, IllegalArgumentException, IllegalAccessException, DOMException, ParserConfigurationException, SAXException, IOException {
		
		Element elm = null;
		
		String name = field.getAnnotation(XMLReflectionField.class).value();
		if (name.length() == 0) {
			name = field.getName();
		}
		if (field.get(object) != null) {
			Document doc = (new XMLReflectionObj(field.get(object))).toXML();
			elm = (Element)xml.importNode(doc.getDocumentElement(), true);
			elm.setAttribute("name", name);
		}
		return elm;
	}
	
	/**
	 * 
	 * @param doc
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws InstantiationException 
	 */
	public void fromXML(Document doc) throws IllegalArgumentException, IllegalAccessException, InstantiationException {
		fromXML(doc.getDocumentElement());
	}
	
	/**
	 * 
	 * @param doc
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws InstantiationException 
	 */
	public void fromXML(Element elm) throws IllegalArgumentException, IllegalAccessException, InstantiationException {
		Field[] fields = getAllFields();

		for (Field field : fields) {
			// if ((field.getModifiers() & Modifier.FINAL) == 0) {
			if (isUsePrivate || (field.getModifiers() & Modifier.PRIVATE) == 0) {
				
				field.setAccessible(true);
				
				if (field.getAnnotation(XMLSimple.class) != null) {
					setSimpleField(elm, field);
				} else if (field.getAnnotation(XMLFieldMap.class) != null) {
					setMapField(elm, field);
				} else if (field.getAnnotation(XMLFieldList.class) != null) {
					setListField(elm, field);
				} else if (field.getAnnotation(XMLReflectionField.class) != null) {
					setReflectionField(elm, field);
				}
				
			}
			// }
		}
	}
	
	/**
	 * 
	 * @param elm
	 * @param field
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	protected void setSimpleField(Element elm, Field field) throws IllegalArgumentException, IllegalAccessException {
		String name = field.getAnnotation(XMLSimple.class).value();
		if (name.length() == 0) {
			name = field.getName();
		}
		/*
		if (name.length() > 0) {
			elm.setAttribute("name", name);
		} else {
			elm.setAttribute("name", field.getName());
		}
		*/
		NodeList nodes = elm.getElementsByTagName("field");
		for (int i=0; i < nodes.getLength(); i++) {
			Element node = (Element)nodes.item(i);
			if (name.equals(node.getAttribute("name"))) {
				
				String value = node.getAttribute("value");
				//String clazz = node.getAttribute("class");
				if ("null".equals(value)) {
					field.set(object, null);
				} else {
					try {
						field.set(object, value);
					} catch (IllegalArgumentException e) {
						String type = field.getType().getName();
						
						if ("boolean".equals(type)) {
							field.setBoolean(object, value.equals("true")?true:false);
						} else if ("int".equals(type)) {
							field.setInt(object, Integer.parseInt(value));
						} else if ("long".equals(type)) {
							field.setLong(object, Long.parseLong(value));
						} else if ("short".equals(type)) {
							field.setShort(object, Short.parseShort(value));
						} else if ("float".equals(type)) {
							field.setFloat(object, Float.parseFloat(value));
						} else if ("double".equals(type)) {
							field.setDouble(object, Double.parseDouble(value));
						} else {
							throw e;
						}
						/*
						System.out.println(e.getMessage());
						e.printStackTrace();
						*/
					}
				}
				break;
			}
		}
	}
	
	/**
	 * 
	 * @param elm
	 * @param field
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	protected void setMapField(Element elm, Field field) throws IllegalArgumentException, IllegalAccessException, InstantiationException {
		String name = field.getAnnotation(XMLFieldMap.class).value();
		if (name.length() == 0) {
			name = field.getName();
		}
		
		/**
		NodeList nodes = elm.getElementsByTagName(name);
		if (nodes.getLength() > 0) {
			XMLMap<Object, XMLReflectionObj> map = new XMLMap<Object, XMLReflectionObj>();
			map.fromXML((Element) nodes.item(0));
			field.set(object, map);
		}
		**/
	}
	
	
	/**
	 * 
	 * @param elm
	 * @param field
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	protected void setListField(Element elm, Field field) throws IllegalArgumentException, IllegalAccessException, InstantiationException {
		String name = field.getAnnotation(XMLFieldList.class).value();
		if (name.length() == 0) {
			name = field.getName();
		}
		
		NodeList nodes = elm.getElementsByTagName(name);
		if (nodes.getLength() > 0) {
			@SuppressWarnings("unchecked")
			List<Object> map = (List<Object>)field.getDeclaringClass().newInstance();
			/**
			XMLList<XMLReflectionObj> map = new XMLList<XMLReflectionObj>();
			map.fromXML((Element) nodes.item(0));
			**/
			XMLList list = new XMLList(map, isUsePrivate);
			list.fromXML((Element) nodes.item(0));
			field.set(object, list);
		}
	}

	/**
	 * 
	 * @param elm
	 * @param field
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	protected void setReflectionField(Element elm, Field field) throws IllegalArgumentException, IllegalAccessException, InstantiationException {
		String name = field.getAnnotation(XMLReflectionField.class).value();
		if (name.length() == 0) {
			name = field.getName();
		}
		/*
		if (name.length() > 0) {
			elm.setAttribute("name", name);
		} else {
			elm.setAttribute("name", field.getName());
		}
		*/
		NodeList nodes = elm.getElementsByTagName("item");
		for (int i=0; i < nodes.getLength(); i++) {
			Element node = (Element)nodes.item(i);
			if (name.equals(node.getAttribute("name"))) {
				String class_name = node.getAttribute("class");
				XMLReflectionObj value;
				try {
					value = (XMLReflectionObj)Class.forName(class_name).newInstance();
					value.fromXML(node);
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					throw new InstantiationException(e.getMessage());
				}
				if (value != null) {
					field.set(object, value);
				}
			}
		}
	}
	
}
