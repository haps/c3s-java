package org.c3s.reflection.annotation;

import java.lang.annotation.*;


@Retention(RetentionPolicy.RUNTIME)
public @interface XMLSimple {
	String value() default "";
} 

