package org.c3s.reflection.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


@Retention(RetentionPolicy.RUNTIME)
public @interface XMLFieldList {
	String value() default "";
}
