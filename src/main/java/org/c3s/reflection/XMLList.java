package org.c3s.reflection;

import java.util.List;

import org.c3s.exceptions.XMLException;
import org.c3s.xml.utils.XMLUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XMLList {

	private List<Object> object;
	boolean isUsePrivate = false;
	
	@SuppressWarnings("unchecked")
	public XMLList(List<?> object, boolean isUsePrivate) {
		super();
		this.object = (List<Object>)object;
		this.isUsePrivate = isUsePrivate;
	}
	/**
	 * 
	 */
	public Document toXML(String root) throws XMLException  {
		Document doc = XMLUtils.createXML(root);
		Element elm = doc.getDocumentElement();
		
		int size = object.size();
		for (int i = 0; i < size; i++) {
			Object item = object.get(i);
			try {
				elm.appendChild(doc.importNode(new XMLReflectionObj(item, isUsePrivate).toXML().getDocumentElement(), true));
			} catch (Exception e) {
				throw new XMLException(e);
			}
		}
		
		return doc;
	}
	
	public void fromXML(Document doc) throws InstantiationException, IllegalAccessException {
		fromXML(doc.getDocumentElement());
	}
	
	public void fromXML(Element elm) throws InstantiationException, IllegalAccessException {
		NodeList childs = elm.getChildNodes();
		for (int i=0; i < childs.getLength(); i++) {
			Node child = childs.item(i);
			if (child.getNodeType() == Node.ELEMENT_NODE) {

				Object value = null;
				
				String clazz = ((Element)child).getAttribute("class");
				try {
					value = Class.forName(clazz).newInstance();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					System.out.println(clazz);
					throw new InstantiationException(e.getMessage());
				}
				if (value != null) {
					new XMLReflectionObj(value, isUsePrivate).fromXML((Element)child);
					object.add(value);
				}
			}
		}
	}
	
}
