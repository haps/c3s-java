package org.c3s.reflection;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.c3s.exceptions.XMLException;
import org.c3s.xml.utils.XMLUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XMLMap {

	/**
	 * 
	 */
	private Map<Object, Object> object;
	boolean isUsePrivate = false;
	
	@SuppressWarnings("unchecked")
	public XMLMap(Map<?, ?> object, boolean isUsePrivate) {
		this.object = (Map<Object, Object>)object;
		this.isUsePrivate = isUsePrivate;
	}

	public Document toXML(String root) throws XMLException {
		Document doc = XMLUtils.createXML(root);
		Element elm = doc.getDocumentElement();

		for (Object key: object.keySet()) {
			Object item =  object.get(key);
			Element node = doc.createElement("field");
			node.setAttribute("name", key.toString());
			node.setAttribute("value", item.toString());
			elm.appendChild(node);
		}
		/**
		Iterator<K> keys = object.keySet().iterator();

		while (keys.hasNext()) {
			K key = keys.next();
			T item = get(key);
			try {
				Element elmitem = (Element) doc.importNode(item.toXML().getDocumentElement(), true);
				elmitem.setAttribute("name", key.toString());
				elm.appendChild(elmitem);
			} catch (Exception e) {
				throw new XMLException(e);
			}
		}
		**/
		return doc;
	}

	/**
	 * 
	 * @param doc
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public void fromXML(Document doc) throws InstantiationException, IllegalAccessException {
		fromXML(doc.getDocumentElement());
	}
	
	@SuppressWarnings("unchecked")
	public void fromXML(Element elm) throws InstantiationException, IllegalAccessException {
		/**
		NodeList childs = elm.getChildNodes();
		for (int i = 0; i < childs.getLength(); i++) {
			Node child = childs.item(i);
			if (child.getNodeType() == Node.ELEMENT_NODE) {

				String name = ((Element) child).getAttribute("name");
				if (name == null || name.length() == 0) {
					name = "" + i;
				}

				String key = name;

				T value = null;

				String clazz = ((Element) child).getAttribute("class");
				try {
					value = (T) Class.forName(clazz).newInstance();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					System.out.println(clazz);
					throw new InstantiationException(e.getMessage());
				}
				if (value != null) {
					value.fromXML((Element) child);
					put((K) key, value);
				}
			}
		}
		*/
	}

}
