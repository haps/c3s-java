package org.c3s.dispatcher;

import org.c3s.redirect.RedirectInterface;

public interface RedirectControlerInterface {
	
	public RedirectInterface getRedirect();
	public boolean hasRedirect();
	public void setRedirect(RedirectInterface redirect);
	
}
