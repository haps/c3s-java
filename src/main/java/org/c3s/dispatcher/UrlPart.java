package org.c3s.dispatcher;

public interface UrlPart {
	public String getUrlParsed();
	public String getPattern();
	public String getUrlReminder();
	public String[] getUrlParsedAsArray();
	public String[] ReminderAsArray();
}
