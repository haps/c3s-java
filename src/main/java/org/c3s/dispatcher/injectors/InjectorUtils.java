/**
 * 
 */
package org.c3s.dispatcher.injectors;

/**
 * @author alexeyz
 *
 */
public class InjectorUtils {

	public static String getAssociatedClassMethodName(String clazz, String method) {
		StringBuffer sb = new StringBuffer();
		sb
			.append("storedClass@")
			.append(clazz)
			.append("@@@")
			.append(method);
		return sb.toString();
	}
	
}
