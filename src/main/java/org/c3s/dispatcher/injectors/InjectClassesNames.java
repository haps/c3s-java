/**
 * 
 */
package org.c3s.dispatcher.injectors;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.c3s.content.ContentObject;
import org.c3s.dispatcher.ActionParameters;
import org.c3s.dispatcher.Dispatcher;
import org.c3s.dispatcher.PatternerInterface;
import org.c3s.dispatcher.RedirectControlerInterface;
import org.c3s.dispatcher.UrlPart;

/**
 * @author hapsys
 *
 */
public enum InjectClassesNames {
	
	SERVLET(Servlet.class),
	REQUEST(ServletRequest.class),
	RESPONSE(ServletResponse.class),
	DISPATCHER(Dispatcher.class),
	CONTENTOBJECT(ContentObject.class),
	PARAMETERS(ActionParameters.class),
	URLPART(UrlPart.class),
	URLPARTS(UrlPart[].class),
	REDIRECT(RedirectControlerInterface.class),
	PATTERNER(PatternerInterface.class),
	INJECTOR(ClassInjectorInterface.class),
	;
	
	private Class<?> clazz;
	
	private InjectClassesNames(Class<?> clazz) {
		this.clazz = clazz;
	}
	
	public Class<?> getInjectClass() {
		return this.clazz;
	}
	
	public static InjectClassesNames[] findClass(Class<?> findClass) {
		//InjectClassesNames result = null;
		List<InjectClassesNames> result = new ArrayList<InjectClassesNames>();
		for (InjectClassesNames value: values()) {
			if (value.getInjectClass().isAssignableFrom(findClass)) {
				result.add(value);
			}
		}
		return result.toArray(new InjectClassesNames[result.size()]);
	}
}
