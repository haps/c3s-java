/**
 * 
 */
package org.c3s.dispatcher.injectors;

import java.math.BigInteger;

import org.c3s.data.cast.CastType;

/**
 * @author admin
 *
 */
public class StringTo implements CastType {

	/* (non-Javadoc)
	 * @see org.c3s.data.cast.CastType#cast(java.lang.Class, java.lang.Class, java.lang.Object)
	 */
	@Override
	public Object cast(Class<?> targetClass, Class<?> sourceClass, Object value) {
		Object result = null;
		if (sourceClass.equals(String.class)) {
			result = cast(targetClass, value);
		}
		return result;
	}

	public Object cast(Class<?> targetClass, Object value) {

		String s = (String)value;
		Object result = value;
		
		if (value != null) {
			if (BigInteger.class.isAssignableFrom(targetClass)) {
				result = Integer.valueOf(s);
			} else if (Byte.class.isAssignableFrom(targetClass)) {
				result = Byte.valueOf(s);
			} else if (Short.class.isAssignableFrom(targetClass)) {
				result = Short.valueOf(s);
			} else if (Integer.class.isAssignableFrom(targetClass)) {
				result = Integer.valueOf(s);
			} else if (Long.class.isAssignableFrom(targetClass)) {
				result = Integer.valueOf(s);
			} else if (Float.class.isAssignableFrom(targetClass)) {
				result = Float.valueOf(s);
			} else if (Double.class.isAssignableFrom(targetClass)) {
				result = Double.valueOf(s);
			} else if (Boolean.class.isAssignableFrom(targetClass)) {
				result = ("true".equalsIgnoreCase(s))?Boolean.valueOf(true):Boolean.valueOf(false);
			}
		}
		
		return result;
	}
}
