package org.c3s.dispatcher.injectors;

public interface ClassInjectorInterface {

	public Object invokeMethod(String methodName, Object object) throws Exception;
	public void setNamedData(String name, Object data);
	
}
