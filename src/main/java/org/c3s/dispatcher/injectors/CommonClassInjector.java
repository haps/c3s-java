package org.c3s.dispatcher.injectors;

import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.List;
import java.util.Map;

import org.c3s.annotations.ActionUrl;
import org.c3s.annotations.AnyObject;
import org.c3s.annotations.CurrentUrl;
import org.c3s.annotations.Parameter;
import org.c3s.annotations.ParameterFile;
import org.c3s.annotations.ParameterGet;
import org.c3s.annotations.ParameterPost;
import org.c3s.annotations.ParameterRequest;
import org.c3s.annotations.Parameters;
import org.c3s.annotations.StoredObject;
import org.c3s.classinfo.ClassInformation;
import org.c3s.classinfo.ClassInformationManager;
import org.c3s.data.cast.CastType;
import org.c3s.query.ParametersHolder;
import org.c3s.query.RequestType;
import org.c3s.query.UploadedFile;
import org.c3s.storage.StorageFactory;
import org.c3s.storage.StorageInterface;
import org.c3s.storage.StorageType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CommonClassInjector extends EmptyClassInjector {
	
	private static Logger logger = LoggerFactory.getLogger(CommonClassInjector.class);
	
	protected CastType caster = null;
	
	public CommonClassInjector() {
		caster = new StringTo();
	}

	public Object invokeMethod(String methodName, Object object) throws Exception {
		Object result = null;
		
		ClassInformation information = ClassInformationManager.getClassInformation(object.getClass());
		Method method = information.getMethod(methodName);
		
		Object[] methodArgs = invokeParameters(method); 
		
		if ((method.getModifiers() & Modifier.STATIC) != 0) {
			result = method.invoke(null, methodArgs);
		} else {
			try {
				/*
				logger.debug("Try to invoke {} method, with parameters", methodName);
				for (Object obj: methodArgs) {
					String res = obj == null?null:obj.getClass().getName();
					logger.debug("{}",res);
				}
				*/
				result = method.invoke(object, methodArgs);
			} catch (Exception e) {
				if (e.getCause() != null && e.getCause() instanceof Exception) {
					throw (Exception)e.getCause();
				} else {
					throw e;
				}
			}
		}
		
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	protected Object[] invokeParameters(Method method) throws InstantiationException, IllegalAccessException {
		Class<?>[] parametersTypes = method.getParameterTypes();
		Annotation[][] parametersAnnotations = method.getParameterAnnotations();
		
		Object[] result = new Object[parametersTypes.length];
		
		for (int i = 0; i < parametersTypes.length; i++) {
			Class<?> clazz = parametersTypes[i];
			Annotation[] annotations = parametersAnnotations[i];
			/**
			 * Process class
			 */
			Object object = null;
			InjectClassesNames[] found = InjectClassesNames.findClass(clazz);
			if (found.length > 0) {
				logger.debug("Found {" + found[0].name() + "} for parameter class {" + clazz + "}");
				object = this.getNamedData().get(found[0].name());
			} else {
				/**
				 * Process Annotations
				 */
				for (Annotation annotation: annotations) {
					logger.debug("Try to find annotation: {" + annotation.annotationType().getName() + "}");
					if (annotation.annotationType().equals(CurrentUrl.class)) {
						object = this.getNamedData().get(CurrentUrl.class.getName());
						
					} else if (annotation.annotationType().equals(ActionUrl.class)) {
						object = this.getNamedData().get(ActionUrl.class.getName());
						
					} else if (annotation.annotationType().equals(ParameterGet.class) 
							|| annotation.annotationType().equals(ParameterPost.class)
							|| annotation.annotationType().equals(ParameterRequest.class)) {
						
						RequestType what = RequestType.NONE;
						String name = null;
						if (annotation.annotationType().equals(ParameterGet.class)) {
							what = RequestType.GET;
							name = ((ParameterGet)annotation).value();
						} else if (annotation.annotationType().equals(ParameterPost.class)) {
							what = RequestType.POST;							
							name = ((ParameterPost)annotation).value();
						} else if (annotation.annotationType().equals(ParameterRequest.class)) {
							what = RequestType.REQUEST;
							name = ((ParameterRequest)annotation).value();
						}
						
						// !!!!!!
						ParametersHolder<String> holder = (ParametersHolder<String>)StorageFactory.getStorage(StorageType.REQUEST).get(what);
						if (holder != null) {
							if (ParametersHolder.class.isAssignableFrom(clazz)) {
								object = holder;
							} else if (clazz.isArray()) {
								object = holder.getParameterValues(name);
							} else if (Map.class.isAssignableFrom(clazz)) {
								/**
								 * Try to get Map or String
								 */
								object = holder.getTreeParameter(name);
							} else {
								object = caster.cast(clazz, String.class, holder.getParameter(name));
							}
						}
					/**
					} else if (annotation.annotationType().equals(ParameterPost.class)) {
						String name = ((ParameterPost)annotation).value();
						ParametersHolder<String> holder = (ParametersHolder<String>)StorageFactory.getStorage(StorageType.REQUEST).get(RequestType.POST);
						if (holder != null) {
							if (ParametersHolder.class.isAssignableFrom(clazz)) {
								object = holder;
							} else if (clazz.isArray()) {
								object = holder.getParameterValues(name);
							} else {
								object = caster.cast(clazz, String.class,holder.getParameter(name));
							}
							if (object == null) {
								if (Map.class.isAssignableFrom(clazz)) {
									object = holder.getTreeParameter(name);
								} else {
									object = caster.cast(clazz, String.class,holder.getStringParameter(name));
								}
							}
						}
					*/
					} else if (annotation.annotationType().equals(ParameterFile.class)) {
						String name = ((ParameterFile)annotation).value();
						ParametersHolder<UploadedFile> holder = (ParametersHolder<UploadedFile>)StorageFactory.getStorage(StorageType.REQUEST).get(RequestType.FILE);
						if (holder != null) {
							if (ParametersHolder.class.isAssignableFrom(clazz)) {
								object = holder;
							} else if (clazz.isArray()) {
								UploadedFile[] files = (UploadedFile[]) holder.getParameterValues(name);
								if (UploadedFile[].class.isAssignableFrom(clazz)) {
									object = files;
								} else if (File[].class.isAssignableFrom(clazz)) {
									object = new File[files.length];
									int k = 0;
									for (UploadedFile file: files) {
										((Object[])object)[k] = file.getUploadedFile();
										k++;		
									}
								} else if (String[].class.isAssignableFrom(clazz)) {
									object = new String[files.length];
									int k = 0;
									for (UploadedFile file: files) {
										((Object[])object)[k] = file.getUploadedName();
										k++;		
									}
								}
							} else {
								UploadedFile files = (UploadedFile) holder.getParameter(name); 
								if (UploadedFile.class.isAssignableFrom(clazz)) {
									object = files;
								} else if (File.class.isAssignableFrom(clazz)) {
									object = files.getUploadedFile();
								} else if (String.class.isAssignableFrom(clazz)) {
									object = files.getUploadedName();
								}
							}
							/**
							 * Try to get Map or String
							 */
							/**
							 * TODO 
							 */
						}
						
					} else if (annotation.annotationType().equals(Parameters.class)) {
						object = this.getNamedData().get(Parameters.class.getName());
						
					} else if (annotation.annotationType().equals(Parameter.class)) {
						String name = ((Parameter)annotation).value();
						Map<String, List<String>> map = (Map<String, List<String>>)this.getNamedData().get(Parameters.class.getName());
						List<String> values = map.get(name);
						if (values != null && values.size() > 0) {
							if (clazz.isArray()) {
								object = values.toArray(new String[values.size()]);
							} else if (List.class.isAssignableFrom(clazz)) {
								object = values;
							} else {
								object = values.get(values.size() - 1);
							}
						}
						
					} else if (annotation.annotationType().equals(StoredObject.class)) {
						
						StoredObject storedObject = (StoredObject)annotation;
						StorageInterface storage = StorageFactory.getStorage(storedObject.storage());
						if (storage != null) {
							String name = storedObject.name();
							if (name != null && !name.isEmpty()) {
								if (storedObject.request() != RequestType.NONE) {
									ParametersHolder<String> holder = (ParametersHolder<String>)StorageFactory.getStorage(StorageType.REQUEST).get(storedObject.request());
									if (holder != null && holder.getParameter(name) != null) {
										name = holder.getParameter(name);
									}
								}
								
								/**
								object = storage.get(name);
								if ((object = storage.get(name)) == null) {
									RequestStoredData storageData = new RequestStoredData(storage);
									storage.put(storageData.getGuid(), storageData);
									object = storageData;
								}
								*/
							} else {
								name = InjectorUtils.getAssociatedClassMethodName(method.getDeclaringClass().getName(), method.getName());
							}
							object = storage.get(name);
							if (storedObject.remove()) {
								storage.remove(name);
							}
						}
						
					} else if (annotation.annotationType().equals(Parameters.class)) {
						object = this.getNamedData().get(AnyObject.class.getName());
					}
					if (object != null) {
						break;
					}
				}
				/**
				 * Check by class name
				 */
				if (object == null) {
					object = getNamedData().get(parametersTypes[i].getName());
				}
			}
			
			result[i] = object;
		}
		
		return result;
	}
}
