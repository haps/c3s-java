package org.c3s.dispatcher.injectors;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

import org.c3s.classinfo.ClassInformation;
import org.c3s.classinfo.ClassInformationManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EmptyClassInjector implements ClassInjectorInterface {

	private static Logger logger = LoggerFactory.getLogger(EmptyClassInjector.class);
	
	private Map<String, Object> namedData = new HashMap<String, Object>();
	
	protected Map<String, Object> getNamedData() {
		return namedData;
	}
	
	@Override
	public Object invokeMethod(String methodName, Object object) throws Exception {
		Object result = null;
		ClassInformation information = ClassInformationManager.getClassInformation(object.getClass());
		
		Method method = information.getMethod(methodName);

		Object[] methodArgs = {};
		
		if ((method.getModifiers() & Modifier.STATIC) != 0) {
			result = method.invoke(null, methodArgs);
		} else {
			result = method.invoke(object, methodArgs);
		}
		
		return result;
	}

	@Override
	public void setNamedData(String name, Object data) {
		logger.debug("Add data {" + name + " = " + data.toString() + "}");
		getNamedData().put(name, data);
	}

}
