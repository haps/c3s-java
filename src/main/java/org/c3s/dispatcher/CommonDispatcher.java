/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.c3s.dispatcher;

import java.io.IOException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.c3s.actions.ActionMapInterface;
import org.c3s.actions.ActionMapModuleInterface;
import org.c3s.core.ClassHolder;
import org.c3s.dispatcher.exceptions.ForwardException;
import org.c3s.dispatcher.exceptions.SkipSubLevelsExeption;
import org.c3s.dispatcher.modules.Dispatched;
import org.c3s.dispatcher.modules.Parameterized;
import org.c3s.dispatcher.modules.Singleton;
import org.c3s.redirect.RedirectInterface;
import org.c3s.web.redirect.DirectRedirect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author admin
 */
public class CommonDispatcher implements Dispatcher {

	private static Logger logger = LoggerFactory.getLogger(CommonDispatcher.class);
	/**
	 *
	 */

	public CommonDispatcher(Object... objects) {
		for (Object object: objects) {
			injectedObjects.add(object);
			logger.debug("Iject object: " + object.getClass().getName());
		}
	}

	private List<Object> injectedObjects = new ArrayList<Object>();
	protected List<Object> getInjectedObjects() {
		return injectedObjects;
	}

	protected String requestUri;
	protected String queryString;

	public void processUrl(String requestUrl) {
		String[] str = requestUrl.split("\\?", 2);
		String uri, query = null;
		uri = str[0];
		if (str.length > 1) {
			query = str[1];
		}
		processUrl(uri, query);
	}

	public void processUrl(String requestUri, String queryString) {

		//Debug.getInstance().out(requestUri, Debug.E_INFO);
		//Debug.getInstance().out(queryString, Debug.E_INFO);
		logger.debug("Dispatcher request URI {" + requestUri + "}");
		logger.debug("Dispatcher request Query {" + queryString + "}");

		this.requestUri = requestUri;
		this.queryString = queryString;
		try {
			this.getUrl();
			//Dbg.prn(this.getCurUrl());
			if (!this.getCurUrl().equals(requestUri)) {
				this.setRedirect(new DirectRedirect(this.getCurUrl(), this.queryString));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 *
	 */

	private ArrayList<String> urls = new ArrayList<String>();

	private ArrayList<String> url_translated = null;

	public ArrayList<String> getPatterns() {
		return urls;
	}

	/**
	 *
	 */

	private void getUrl() throws IOException {
		//Dbg.prn(_request.getRequestURI());
		//Dbg.prn(_request.getQueryString());

		//_request.getRequestURI().sp
		this.makeUrls();
	}

	private void makeUrls() {
		makeUrls(this.requestUri);
	}

	private void makeUrls(String url) {

		urls = new ArrayList<String>();
		url_translated = new ArrayList<String>();

		String[] real_urls = url.split("/");


		for(String sub : real_urls) {
			if(sub.trim().length() != 0) {
				urls.add(sub.trim());
			}
		}

		int cnt = urls.size();
		url_translated.add("/");

		for(int i=0; i < cnt; i++) {
			/*
			 * test
			 */

			//Dbg.prn(Boolean.valueOf(urls.get(i).matches("^.+\\.html$")));
			//
			if (i < cnt-1 || !urls.get(i).matches("^.+\\.html$")) {
				url_translated.add(urls.get(i) + "/");
			} else {
				url_translated.add(urls.get(i));
			}
		}
	}

	public String getCurrentUrl() {
		return getCurUrl();
	}

	
	public String getCurUrl() {
		int cnt = url_translated.size();
		String ret_url = "";
		for (int i=0; i < cnt; i++) {
			ret_url += url_translated.get(i);
		}
		return ret_url;
	}

	/*
	 * Redirect section
	 */

	private RedirectInterface redirect = null;

	/**
	 * @return the redirect
	 */
	public RedirectInterface getRedirect() {
		return redirect;
	}

	public boolean hasRedirect() {
		return redirect != null && redirect.getUrl() != null && redirect.getUrl().length() != 0;
	}

	/**
	 * @param redirect the redirect to set
	 */
	public void setRedirect(RedirectInterface redirect) {
		this.redirect = redirect;
		/**
		 * 
		 */
		StackTraceElement[] trace = Thread.currentThread().getStackTrace();
		logger.info("Set redirect to: {} from {}.{} at line ", redirect.getUrl(), 
				trace[2].getClassName(), trace[2].getMethodName(), trace[2].getLineNumber());
	}

	/**
	 *
	 */
	private List<DispatchedUrlPart> url_parsed = new ArrayList<DispatchedUrlPart>();

	public List<DispatchedUrlPart> getWorkedUrls() {
		return url_parsed;
	}

	public DispatchedUrlPart getWorkUrl() {
		DispatchedUrlPart ret_url = null;
		int size = url_parsed.size();
		ret_url = url_parsed.get(size-1);
		return ret_url;
	}

	public String getActionUrl() {
		DispatchedUrlPart current = getWorkUrl();
		return (current.getUrlParsed() + "/" + current.getPattern() + "/").replaceAll("/+", "/"); 
	}
	/**
	 *
	 */
	private void parsedUrl(int level) {
		DispatchedUrlPart url = new DispatchedUrlPart();

		String str = "";
		for(int i=0; i < level; i++) {
			str += url_translated.get(i);
		}
		//url.put("urlparsed", str);
		url.setUrlParsed(str);
		//url.put("pattern", url_translated.get(level));
		url.setPattern(url_translated.get(level));

		str = "";

		for (int i = level+1; i < url_translated.size(); i++) {
			str += url_translated.get(i);
		}
		//url.put("urlremainder", str);
		url.setUrlReminder(str);

		url_parsed.add(url);
	}

	/*
	 *
	 */

	public void dispatch(ActionMapInterface map) throws Exception {
		processModules(map);
	}


	private Map<ActionMapModuleInterface, Object> skipSubModules = new HashMap<ActionMapModuleInterface, Object>();
	
	protected void processModules(ActionMapInterface map) throws Exception {
		skipSubModules.clear();
		String wrk_url = "";
		int cnt = url_translated.size();
		for(int level=0; level < cnt; level++) {
			String url = url_translated.get(level);
			parsedUrl(level);
			wrk_url += url;
			try {
				List<ActionMapModuleInterface> modules = map.getModules();
				int mcount = modules.size();
				for (int i=0; i < mcount; i++) {
					processModuleUrl(wrk_url, modules.get(i));
				}
			} catch (Exception e) {
				throw e;
			}
		}
	}

	protected void processModuleUrl(String url, ActionMapModuleInterface module) throws Exception {
	
		String regexp = module.getRegexp();

		logger.debug("Url: {" + url + "}, regexp: {" + regexp + "}, class {" + module.getClassName() + "}");
		if ((!module.isLast() || url_parsed.get(url_parsed.size() - 1).getUrlReminder().length() == 0) && url.matches(regexp)) {
			//System.out.println(">>>" + module.getClassName());
			String class_name = module.getClassName();

			/**
			if (class_name != null) {
				System.out.print(url);
				System.out.print("\t");
				System.out.print(regexp);
				System.out.print("\t");
				System.out.print(module.hashCode());
				System.out.print("\t");
				System.out.println(class_name);
			}
			*/
			
			
			if (class_name != null && class_name.length() > 0) {
				try {
					long start = System.currentTimeMillis();
					logger.debug("Loading module {" + module.getClassName() + "} method {" + module.getAction() + "} on pattern {" + url + "}" );
					processModule(module);
					logger.debug("Module working " + (Double.valueOf(System.currentTimeMillis() - start)/1000) + " sec." );
				} catch (Exception e) {
					//e.printStackTrace();
					if (e.getCause() instanceof ForwardException) {
						throw (ForwardException)e.getCause();
					} else if (e instanceof SkipSubLevelsExeption) {
						skipSubModules.put(module, new Object());
					} else {
						throw e;
					}
				}
			}
			
		}

		if (skipSubModules.get(module) == null) {
			for(int i=0; i < module.getModules().size(); i++) {
				processModuleUrl(url, module.getModules().get(i));
			}
		}
	}

	
	protected void processModule(ActionMapModuleInterface module) throws Exception {
		
		Class<?> cls = Class.forName(module.getClassName());
		String action = module.getAction();
		Object obj = null;
		if (action == null || action.length() == 0) {
			if (Singleton.class.isAssignableFrom(cls)) {
				obj = ClassHolder.getObject(cls.getName());
				if (obj == null) {
					obj = cls.newInstance();
					ClassHolder.putObject(obj);
				}
			} else {
				obj = cls.newInstance();
			}
		} else {
			Class<?>[] MethodArgsTypes = {};
			Object[] MethodArgs = {};

			Method mth = cls.getMethod(action, MethodArgsTypes);
			if ((mth.getModifiers() & Modifier.STATIC) != 0) {
				mth.invoke(null, MethodArgs);
			} else {
				if (Singleton.class.isAssignableFrom(cls)) {
					//Dbg.prn(">Singleton!!!");
					obj = ClassHolder.getObject(cls.getName());
					if (obj == null) {
						obj = cls.newInstance();
						ClassHolder.putObject(obj);
					}
				} else {
					obj = cls.newInstance();
				}
				//Dbg.prn(obj.getClass().getName());
				if (obj instanceof Dispatched) {
					((Dispatched)obj).setDispatcher(this);
				}
				if (obj instanceof Parameterized) {
					((Parameterized)obj).setParameters(module.getParameters());
				}
				mth.invoke(obj, MethodArgs);
			}
		}
		
	}
	
}
