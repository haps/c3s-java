/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.c3s.dispatcher;

import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author admin
 */
public class DispatchedUrlPart implements UrlPart {
	private String urlParsed;
	private String pattern;
	private String urlReminder;


	public DispatchedUrlPart() {
		
	}

	public DispatchedUrlPart(String urlParsed, String pattern, String urlReminder) {
		this.urlParsed = urlParsed;
		this.pattern = pattern;
		this.urlReminder = urlReminder;
	}

	/**
	 * @return the urlParsed
	 */
	public String getUrlParsed() {
		return urlParsed;
	}

	/**
	 * @param urlParsed the urlParsed to set
	 */
	public void setUrlParsed(String urlParsed) {
		this.urlParsed = urlParsed;
	}

	/**
	 * @return the pattern
	 */
	public String getPattern() {
		return pattern;
	}

	/**
	 * @param pattern the pattern to set
	 */
	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	/**
	 * @return the urlReminder
	 */
	public String getUrlReminder() {
		return urlReminder;
	}

	/**
	 * @param urlReminder the urlReminder to set
	 */
	public void setUrlReminder(String urlReminder) {
		this.urlReminder = urlReminder;
	}

	public String toString() {
		return "{urlparsed: \""+urlParsed+"\", pattern: \""+pattern+"\", urlreminder: \""+urlReminder+"\"}";
	}

	@Override
	public String[] getUrlParsedAsArray() {
		List<String> parsed = new ArrayList<String>();
		StringBuffer sb = new StringBuffer();
		for (int i=0; i < urlParsed.length(); i++) {
			sb.append(urlParsed.charAt(i));
			if (urlParsed.charAt(i) == '/') {
				parsed.add(sb.toString());
				sb = new StringBuffer();
			}
		}
		if (sb.length() > 0) {
			parsed.add(sb.toString());
		}
		return parsed.toArray(new String[parsed.size()]);
	}

	@Override
	public String[] ReminderAsArray() {
		List<String> parsed = new ArrayList<String>();
		StringBuffer sb = new StringBuffer();
		for (int i=0; i < urlReminder.length(); i++) {
			sb.append(urlReminder.charAt(i));
			if (urlReminder.charAt(i) == '/') {
				parsed.add(sb.toString());
				sb = new StringBuffer();
			}
		}
		if (sb.length() > 0) {
			parsed.add(sb.toString());
		}
		return parsed.toArray(new String[parsed.size()]);
	}
}
