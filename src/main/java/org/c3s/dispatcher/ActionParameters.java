package org.c3s.dispatcher;

import java.util.List;
import java.util.Map;

public class ActionParameters {

	private Map<String, List<String>> parameters;
	
	public ActionParameters(Map<String, List<String>> parameters) {
		this.parameters = parameters;
	}

	/*
	 * Parameters
	 */

	public void setParameters(Map<String, List<String>> parameters) {
		this.parameters = parameters;
	}

	protected boolean hasParameter(String name) {
		return this.parameters != null && this.parameters.get(name) != null;
	}

	protected boolean isMultipleParameter(String name) throws Exception {
		return getParameters(name).size() > 1;
	}

	protected String getParameter(String name, int index) throws Exception  {
		return getParameters(name).get(index);
	}

	protected String getParameter(String name) throws Exception  {
		return getParameter(name,0);
	}


	protected List<String> getParameters(String name) throws Exception  {
		if (!hasParameter(name)) {
			throw new Exception("Parameter \""+name+"\" not set!");
		}
		return this.parameters.get(name);
	}

	protected int getParameterLength(String name) throws Exception  {
		return getParameters(name).size();
	}

	
}
