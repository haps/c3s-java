package org.c3s.dispatcher;

import org.c3s.actions.ActionMapModuleInterface;
import org.c3s.annotations.Controller;
import org.c3s.annotations.CurrentUrl;
import org.c3s.annotations.Parameters;
import org.c3s.annotations.ActionUrl;
import org.c3s.core.ClassHolder;
import org.c3s.dispatcher.injectors.ClassInjectorInterface;
import org.c3s.dispatcher.injectors.InjectClassesNames;
import org.c3s.dispatcher.modules.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InjectiveDispatcher extends CommonDispatcher {

	private static Logger logger = LoggerFactory.getLogger(InjectiveDispatcher.class);
	
	protected ClassInjectorInterface injector;
	
	public InjectiveDispatcher(Object... objects) {
		super(objects);
		
		for (Object object: getInjectedObjects()) {
			if (ClassInjectorInterface.class.isAssignableFrom(object.getClass())) {
				injector = (ClassInjectorInterface)object;
				break;
			}
		}
		getInjectedObjects().add(this);
		
		for (Object object: getInjectedObjects()) {
			InjectClassesNames[] founds = InjectClassesNames.findClass(object.getClass());
			for (InjectClassesNames found : founds) {
				injector.setNamedData(found.name(), object);
			}
		}
	}

	public ClassInjectorInterface getInjector() {
		return injector;
	}
	
	@Override
	protected void processModule(ActionMapModuleInterface module) throws Exception {
		Class<?> clazz = Class.forName(module.getClassName());
		String action = module.getAction();
		if (action == null  || action.length() == 0) {
			super.processModule(module);
		} else {
			String instanceName = module.getInstanceName();
			boolean isSingleton = false;
			if (instanceName != null && instanceName.length() > 0) {
				isSingleton = true;
			} else {
				Controller contoller = clazz.getAnnotation(Controller.class);
				if (contoller != null) {
					isSingleton = true;
					instanceName = (contoller.value().length() > 0)?contoller.value():clazz.getName();
				} else if (Singleton.class.isAssignableFrom(clazz)) {
					isSingleton = true;
					instanceName = clazz.getName();
				}
			}
			
			logger.debug("Module is Singleton {" + isSingleton + "}");

			Object object = null;
			if (!isSingleton) {
				object = clazz.newInstance();
			} else {
				object = ClassHolder.getObject(instanceName);
				if (object == null) {
					object = ClassHolder.putObject(clazz.newInstance(), instanceName);
				}
			}
			/**
			 * Add additional
			 */
			injector.setNamedData(InjectClassesNames.URLPART.name(), this.getWorkUrl());
			injector.setNamedData(InjectClassesNames.URLPARTS.name(), this.getWorkedUrls());
			injector.setNamedData(InjectClassesNames.PARAMETERS.name(), new ActionParameters(module.getParameters()));
			
			injector.setNamedData(CurrentUrl.class.getName(), this.getCurrentUrl());
			injector.setNamedData(ActionUrl.class.getName(), this.getActionUrl());
			injector.setNamedData(Parameters.class.getName(), module.getParameters());
			/**
			 * 
			 */
			injector.invokeMethod(action, object);
		}
	}

	//protected from classHolder
}
