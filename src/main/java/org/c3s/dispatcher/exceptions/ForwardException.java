/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.c3s.dispatcher.exceptions;

import org.c3s.redirect.RedirectInterface;

/**
 *
 * @author admin
 */
public class ForwardException extends Exception{

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 2951933208906239781L;
	/**
	 * 
	 */
	private RedirectInterface forward;

	public ForwardException(RedirectInterface forward) {
		super("Forward to: " + forward.getUrl());
		this.forward = forward;
	}

	public RedirectInterface getForward() {
		return forward;
	}
}
