package org.c3s.dispatcher;

import java.util.List;

public interface PatternerInterface {
	
	public String getCurrentUrl();
	
	public String getActionUrl();
	
	public List<?> getWorkedUrls();
	
	public UrlPart getWorkUrl();
	
}
