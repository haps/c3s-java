package org.c3s.dispatcher;

import org.c3s.actions.ActionMapInterface;

public interface Dispatcher extends RedirectControlerInterface, PatternerInterface {
	
	public void dispatch(ActionMapInterface map) throws Exception;
	
	public void processUrl(String requestUri, String queryString);
	public void processUrl(String requestUri);
}
