/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.c3s.dispatcher.modules;

import java.util.List;
import java.util.Map;

import org.c3s.content.ContentObject;
import org.c3s.dispatcher.CommonDispatcher;
import org.c3s.web.context.PageRequestContext;
/**
 *
 * @author admin
 */
public abstract class BaseModule implements Parameterized, Dispatched {

	public BaseModule() {}
	/*
	 * Dispatcher
	 */
	private CommonDispatcher _disparcher = null;

	public void setDispatcher(CommonDispatcher dispatcher) {
		_disparcher = dispatcher;
	}

	protected CommonDispatcher getDispatcher() {
		return _disparcher;
	}

	/*
	 * ContentObject
	 */

	private ContentObject content = ContentObject.getInstance();

	protected ContentObject getContent() {
		return content;
	}

	/*
	 * Context
	 */

	private PageRequestContext context = PageRequestContext.getInstance();

	protected PageRequestContext getContext() {
		return context;
	}

	/*
	 * Parameters
	 */
	private Map<String, List<String>> _parameters = null;

	public void setParameters(Map<String, List<String>> parameters) {
		_parameters = parameters;
	}

	protected boolean hasParameter(String name) {
		return _parameters != null && _parameters.get(name) != null;
	}

	protected boolean isMultipleParameter(String name) throws Exception {
		return getParameters(name).size() > 1;
	}

	protected String getParameter(String name, int index) throws Exception  {
		return getParameters(name).get(index);
	}

	protected String getParameter(String name) throws Exception  {
		return getParameter(name,0);
	}


	protected List<String> getParameters(String name) throws Exception  {
		if (!hasParameter(name)) {
			throw new Exception("Parameter \""+name+"\" not set!");
		}
		return _parameters.get(name);
	}

	protected int getParameterLength(String name) throws Exception  {
		return getParameters(name).size();
	}

	
	/*
	 * 
	 */
	protected boolean isAjax() {
		return false;
	}
}
