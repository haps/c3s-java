/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.c3s.db.injectors;

import org.c3s.db.DBConnection;

/**
 *
 * @author azajac
 */
public class EmptySqlInjector implements SqlInjectorInterface {

	public String getOrderQuery() {
		return "";
	}

	public String getLimitQuery() {
		return "";
	}

	public String getWhereQuery() {
		return "";
	}

	public String getFromQuery() {
		return "";
	}

	public String getJoinQuery() {
		return "";
	}

	public String getRecordQuery() {
		return "";
	}

	public String getFullQuery() {
		return null;
	}
	
	
	protected DBConnection connection;
	
	public final void setConnection(DBConnection connection) {
		this.connection = connection;
	}
	
}
