/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.c3s.db.injectors;

import org.c3s.db.DBConnection;

/**
 *
 * @author azajac
 */
public interface SqlInjectorInterface {

	public String getOrderQuery();
	public String getLimitQuery();
	public String getWhereQuery();
	public String getFromQuery();
	public String getJoinQuery();
	public String getRecordQuery();
	public String getFullQuery();
	
	public void setConnection(DBConnection connection);
}
