package org.c3s.db;

import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author hapsys
 *
 */
public class DBManager {

	private static Map<String, DBConnection> connections = new ConcurrentHashMap<String, DBConnection>();

	/**
	 * @param name
	 * @param driver
	 * @param dsn
	 * @param props
	 * @return
	 * @throws SQLException
	 */
	public static DBConnection getConnection(String name, String driver,
			String dsn, Properties props) throws SQLException {

		if (name == null || name.length() == 0) {
			name = "default";
		}
		
		if (connections.get(name) == null) {
			connections.put(name, new DBConnection(driver, dsn, props));
		}
		return connections.get(name);
	}

	/**
	 * @param name
	 * @param driver
	 * @param dsn
	 * @return
	 * @throws SQLException
	 */
	public static DBConnection getConnection(String name, String driver,
			String dsn) throws SQLException {
		
		if (name == null || name.length() == 0) {
			name = "default";
		}
		
		if (connections.get(name) == null) {
			connections.put(name, new DBConnection(driver, dsn));
		}
		return connections.get(name);
	}
	
	/**
	 * @param name
	 * @param dsn
	 * @return
	 * @throws SQLException
	 */
	public static DBConnection getConnection(String name, String dsn)
			throws SQLException {
		
		if (name == null || name.length() == 0) {
			name = "default";
		}
		
		if (connections.get(name) == null) {
			connections.put(name, new DBConnection(dsn));
		}
		return connections.get(name);
	}

	/**
	 * @param name
	 * @return
	 * @throws SQLException
	 */
	public static DBConnection getConnection(String name) throws SQLException {
		
		if (name == null || name.length() == 0) {
			name = "default";
		}
		
		return connections.get(name);
	}

	/**
	 * @return
	 * @throws SQLException
	 */
	public static DBConnection getConnection() throws SQLException {
		return connections.get(null);
	}

}
