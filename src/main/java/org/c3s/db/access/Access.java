package org.c3s.db.access;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.c3s.data.mapers.GeneralDataMapper;
import org.c3s.db.DBConnection;
import org.c3s.db.DBManager;
import org.c3s.db.beans.Bean;
import org.c3s.db.injectors.EmptySqlInjector;
import org.c3s.db.injectors.SqlInjectorInterface;

@SuppressWarnings("unused")
public abstract class Access {

	protected String con_name = "default";
	protected String tablename = "";
	
	public DBConnection getCon() {
		DBConnection con = null;
		try {
			return DBManager.getConnection(con_name);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return con;
	}
	
	protected List<Map<String, Object>> _getTableRecords(SqlInjectorInterface intruder) throws SQLException  {
		
		if (intruder == null) {
			intruder = new EmptySqlInjector();
		}
		
		String query = intruder.getFullQuery();
		if (query == null) {
			query = "SELECT t.* " + intruder.getRecordQuery() + 
				" FROM " + tablename + " AS t " + intruder.getFromQuery() + " " +
				intruder.getJoinQuery() + " " + intruder.getWhereQuery() + " " 
				+ intruder.getOrderQuery() + " " + intruder.getLimitQuery() + " ";
		}
		
		List<Map<String, Object>> ret = getCon().fetchRows(query);
		
		return ret;
	}
	
	protected int _insert(Object bean/* , String auctoincfield_name */) throws SQLException, IllegalArgumentException, IllegalAccessException {
		int result = 0;
		
		/*
		String fields_str = ""; 
		String values_str = ""; 
		ArrayList<Object> values = new ArrayList<Object>();
		
		Set<String> names = bean.getOriginalNames();
		boolean flag = false;
		for (String name: names) {
			if (!name.equals(auctoincfield_name)) {
				fields_str += (flag?", ":"") + name;
				values_str += flag?", ?":"?";
				values.add(bean.get(name));
				flag = true;
			}
		}
		String query = "INSERT INTO " + tablename + " (" + fields_str + ") VALUES (" + values_str + " )"; 
		
		PreparedStatement stmp = getCon().getPreparedStatement(query);

		int i=1;
		for (Object obj: values) {
			stmp.setObject(i, obj);
			i++;
		}
		
		result = stmp.executeUpdate();
		*/
		
		if (bean instanceof Bean) {
			result = getCon().insertRow(tablename, ((Bean)bean).getOriginalKeyValue());
		} else {
			result = getCon().insertRow(tablename, new GeneralDataMapper().mapToRow(bean));
		}
		
		return result;
	}
	
}
