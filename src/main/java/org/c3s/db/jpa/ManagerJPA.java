/**
 * 
 */
package org.c3s.db.jpa;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * @author admin
 *
 */
public class ManagerJPA {
	private static Map<String, Map<Object,EntityManager>> managers = new ConcurrentHashMap<String, Map<Object,EntityManager>>(); 

	public static EntityManager get() {
		return get(null);
	}
	public static EntityManager get(String jpa) {
		return get(jpa, null);
	}
	public static EntityManager get(String jpa, Object name) {
		if (name == null) {
			name = "default";
		}
		EntityManager manager = null;
		synchronized (ManagerJPA.class) {
			Map<Object,EntityManager> internal = managers.get(jpa);
			if (internal == null) {
				internal = new ConcurrentHashMap<Object, EntityManager>();
				managers.put(jpa, internal);
			}
			manager = internal.get(name);
			if (manager == null || !manager.isOpen()) {
				EntityManagerFactory factory = Persistence.createEntityManagerFactory(jpa); 
				manager = factory.createEntityManager();
				internal.put(name, manager);
			}
		}
		return manager;
	}
}
