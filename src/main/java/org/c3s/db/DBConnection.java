package org.c3s.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.c3s.utils.Utils;
import org.postgresql.PGResultSetMetaData;

/**
 * 
 * @author azajac
 */
public class DBConnection {

	private Connection	connection	= null;
	
	private Map<String, Map<String, DBColumnMetadata>> _meta_data = new ConcurrentHashMap<String, Map<String, DBColumnMetadata>>();

	private Map<String, PreparedStatement> stmp_hash = new ConcurrentHashMap<String, PreparedStatement>();

	/**
	 * @param sql
	 * @return
	 * @throws SQLException
	 */
	public PreparedStatement getPreparedStatement(String sql) throws SQLException {
		PreparedStatement stmp = stmp_hash.get(sql);
		if (stmp == null || stmp.isClosed()) {
			stmp = connection.prepareStatement(sql, java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_UPDATABLE, java.sql.ResultSet.HOLD_CURSORS_OVER_COMMIT);
			//stmp = connection.prepareStatement(sql);
			stmp_hash.put(sql, stmp);
		}
		return stmp;
	}

	/**
	 * @param sql
	 * @return
	 * @throws SQLException
	 */
	public PreparedStatement getPreparedStatement1(String sql) throws SQLException {
		return connection.prepareStatement(sql, java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_UPDATABLE);
	}

	String			driver;
	String			dsn;
	Properties	properties;

	/**
	 * 
	 * @param driver
	 * @param dsn
	 * @throws Exception
	 */
	public DBConnection(String driver, String dsn, Properties properties) throws SQLException {
		// System.out.println(driver);
		this.driver = driver;
		this.dsn = dsn;
		this.properties = properties;
	}

	/**
	 * 
	 * @param driver
	 * @param dsn
	 * @throws SQLException
	 */

	public DBConnection(String driver, String dsn) throws SQLException {
		this(driver, dsn, null);
	}

	/**
	 * @param dsn
	 * @throws SQLException
	 */
	public DBConnection(String dsn) throws SQLException {
		this(null, dsn);
	}

	/**
	 * @param connection
	 * @throws SQLException
	 */
	public DBConnection(Connection connection) throws SQLException {
		this.connection = connection;
	}

	/**
	 * @throws SQLException
	 */
	public void connect() throws SQLException {
		int count = 2;
		while (count > 0) {
			count--;
			try {
				if (connection == null || !connection.isValid(1)) {
					if (driver != null) {
						try {
							Class.forName(driver);
						} catch (Exception e) {
							// TODO: разберемся
						}
					}
					if (properties != null) {
						connection = DriverManager.getConnection(dsn, properties);
					} else {
						connection = DriverManager.getConnection(dsn);
					}
				}
			} catch (SQLException e) {
				if (count == 0) {
					throw e;
				} else {
					if (connection != null) {
						try {
							close();
							connection = null;
							count = 1;
						} catch (Exception e1) {
							throw new SQLException(e1);
						}
					}
				}
			}
		}
	}

	public void close() throws Exception {
		for (String key: stmp_hash.keySet()) {
			stmp_hash.get(key).close();
		}
		if (connection != null) {
			connection.close();
		}
	}
	
	@Override
	protected void finalize() {
		try {
			close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				super.finalize();
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	/**
	 * 
	 * @return
	 * @throws SQLException
	 */
	public Connection getConnection() throws SQLException {
		connect();
		return connection;
	}

	/**
	 * @param name
	 * @param sql
	 * @return
	 * @throws SQLException
	 */
	public Map<String, DBColumnMetadata> getStatementMetaData(String name, String sql) throws SQLException {
		connect();
		Map<String, DBColumnMetadata> result = null;
		try {
			/*
			 * while(lock) { Thread.sleep(0); } lock = true;
			 */
			if (_meta_data.get(name) == null) {
				
				_meta_data.put(name, new ConcurrentHashMap<String, DBColumnMetadata>());

				PreparedStatement stmp = getPreparedStatement(sql);
				ResultSetMetaData md = stmp.getMetaData();

				if (md != null) {
					for (int i = 1; i <= md.getColumnCount(); i++) {
						
						DBColumnMetadata scm = getColumnMetadata(md, i);
						_meta_data.get(name).put(scm.getColumnLabel(), scm);
					}
				}
			}
			result = _meta_data.get(name);
			
		} catch (SQLException e) {
			e.printStackTrace(System.err);
			throw e;
		} finally {
		}
		return result;
	}

	/**
	 * @param md
	 * @param i
	 * @return
	 * @throws SQLException
	 */
	protected DBColumnMetadata getColumnMetadata(ResultSetMetaData md, int i) throws SQLException {
		DBColumnMetadata data = new DBColumnMetadata();
		boolean skip_pg = false;
		try {
			Class.forName("org.postgresql.PGResultSetMetaData");
		} catch (Exception e) {
			skip_pg = true;
		}
		if (!skip_pg && md instanceof PGResultSetMetaData) {
			PGResultSetMetaData pgm = (PGResultSetMetaData) md;
			// data.setColumnRealName(pgm.getBaseColumnName(i));
			data.setColumnTable(pgm.getBaseTableName(i));
		} else {
			data.setColumnTable(md.getTableName(i));
		}

		data.setColumnRealName(md.getColumnName(i));
		/* @todo !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
		//data.setColumnName(Utils.generateName(data.getColumnRealName()));
		data.setColumnName(Utils.generateName(md.getColumnLabel(i)));
		data.setColumnType(md.getColumnType(i));
		// data.setColumnTypeStr(md.getColumnTypeName(i));
		data.setColumnTypeStr(md.getColumnClassName(i));
		data.setIsAuctoincrement(md.isAutoIncrement(i));
		data.setColumnLabel(md.getColumnLabel(i));
		data.setColumnTable(md.getTableName(i));
		return data;
	}

	/**
	 * @param table_name
	 * @return
	 * @throws SQLException
	 */
	public Map<String, DBColumnMetadata> getTableMetaData(String table_name) throws SQLException {
		return getStatementMetaData(table_name, "SELECT * FROM " + table_name + " WHERE 1=0");
	}

	/*
	 * some wrapper methods
	 */
	/**
	 * @param table_name
	 * @param col_values
	 * @return
	 * @throws SQLException
	 */
	public int insertRow(String table_name, Map<String, Object> col_values) throws SQLException {
		connect();
		int result = -1;

		String query = "";
		
		ResultSet rs = null;
		try {
			
			Map<String, DBColumnMetadata> meta = getTableMetaData(table_name);
			String fields_str = "";
			String values_str = "";

			Set<String> names = meta.keySet();
			boolean flag = false;
			for (String name : names) {
				DBColumnMetadata col = meta.get(name);
				if (!col.isAuctoincrement() && col_values.containsKey(name)) {
					fields_str += (flag ? ", " : "") + name;
					values_str += flag ? ", ?" : "?";
					flag = true;
				}
			}
			query = "INSERT INTO " + table_name + " (" + fields_str + ") VALUES (" + values_str + " )";

			PreparedStatement stmp = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

			int i = 1;
			for (String name : names) {
				DBColumnMetadata col = meta.get(name);
				if (!col.isAuctoincrement() && col_values.containsKey(name)) {
					stmp.setObject(i, col_values.get(name), col.getColumnType());
					i++;
				}
			}
			stmp.executeUpdate();
			
			rs = stmp.getGeneratedKeys();
			
			if (rs.next()) {
				result = rs.getInt(1);
		    }
			
			rs.close();
			
			if (result == -1) {
				Statement res = connection.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_UPDATABLE);
				rs = res.executeQuery("SELECT LAST_INSERT_ID()");
				if (rs.next()) {
					result = rs.getInt(1);
			    }
				res.close();
				rs.close();
			}

		} catch (SQLException e) {
			throw e;
		} finally {
			if (rs != null) {
				rs.close();
			}
		}

		return result;
	}

	/**
	 * @param table_name
	 * @param col_values
	 * @param idexes_values
	 * @return
	 * @throws SQLException
	 */
	public int updateRow(String table_name, Map<String, Object> col_values, Map<String, Object> idexes_values) throws SQLException {
		connect();
		int result = 0;
		String query = "";
		try {
			Map<String, DBColumnMetadata> meta = getTableMetaData(table_name);
			String set_query = "";
			String where_query = "";

			Set<String> names = meta.keySet();
			for (String name : names) {
				if (col_values.containsKey(name)) {
					set_query += ((set_query.length() == 0) ? "" : ", ") + name + " = ? ";
				}
				if (idexes_values.containsKey(name)) {
					where_query += ((where_query.length() == 0) ? "" : "AND ") + name + " = ? ";
				}
			}
			query = "UPDATE " + table_name + " SET " + set_query + ((where_query.length() > 0) ? "WHERE " + where_query : "");

			PreparedStatement stmp = connection.prepareStatement(query);

			synchronized (stmp) {
				int i = 1;
				for (String name : names) {
					DBColumnMetadata col = meta.get(name);
					if (col_values.containsKey(name)) {
						stmp.setObject(i, col_values.get(name), col.getColumnType());
						i++;
					}
				}
				for (String name : names) {
					DBColumnMetadata col = meta.get(name);
					if (idexes_values.containsKey(name)) {
						stmp.setObject(i, idexes_values.get(name), col.getColumnType());
						i++;
					}
				}
				result = stmp.executeUpdate();
			}
		} catch (SQLException e) {
			// System.out.println(query);
			throw e;
		} finally {
			// lock = false;
		}

		return result;
	}

	/**
	 * @param query
	 * @param values
	 * @return
	 * @throws SQLException
	 */
	public List<Map<String, Object>> fetchRows(String query, Object... values) throws SQLException {
		return fetchRows(null, query, values);
	}

	/**
	 * @param qname
	 * @param query
	 * @param values
	 * @return
	 * @throws SQLException
	 */
	public List<Map<String, Object>> fetchRows(String qname, String query, Object... values) throws SQLException {
		connect();
		List<Map<String, Object>> result = null;
		ResultSet res = null;
		try {
			Map<String, DBColumnMetadata> meta = null;
			if (qname != null) {
				meta = getStatementMetaData(qname, query);
			} else {
				meta = getStatementMetaData(Utils.MD5(query), query);
			}
			Set<String> names = meta.keySet();
			PreparedStatement stmp = getPreparedStatement(query);
			synchronized (stmp) { 
				ParameterMetaData pmeta = stmp.getParameterMetaData();
				int cnt = pmeta.getParameterCount();
				for (int i = 0; i < cnt; i++) {
					// stmp.setObject(i+1, values[i],
					// is_mysql?java.sql.Types.VARCHAR:pmeta.getParameterType(i+1));
					Integer param_type = null;
					try {
						param_type = new Integer(pmeta.getParameterType(i + 1));
					} catch (Exception e) {
						// nothing todo
					}
					if (param_type == null) {
						// Oracle cann't getParameterType
						stmp.setObject(i + 1, values[i]);
					} else {
						stmp.setObject(i + 1, values[i], param_type.intValue());
					}
				}
				
				res = stmp.executeQuery();
	
				while (res.next()) {
					if (result == null) {
						result = new ArrayList<Map<String, Object>>();
					}
					Map<String, Object> fetch = new HashMap<String, Object>();
					for (String name : names) {
						fetch.put(name, res.getObject(name));
					}
					result.add(fetch);
				}
				res.close();
			}
			//stmp.close();
		} catch (SQLException e) {
			// System.out.println(query);
			throw e;
		} finally {
			if (res != null) {
				res.close();
			}
		}
		return result;
	}

	/**
	 * @param query
	 * @param values
	 * @return
	 * @throws SQLException
	 */
	/*
	public int query(String query, Object... values) throws SQLException {
		return query(null, query, values);
	}
	*/

	/**
	 * @param name
	 * @param query
	 * @param values
	 * @return
	 * @throws SQLException
	 */
	public int query(String query, Object... values) throws SQLException {
	//public int query(String name, String query, Object... values) throws SQLException {
		int result = 0;
		connect();
		//PreparedStatement stmp = connection.prepareStatement(query);
		PreparedStatement stmp = getPreparedStatement(query);
		synchronized (stmp) {
			ParameterMetaData pmeta = stmp.getParameterMetaData();
			int cnt = pmeta.getParameterCount();
			for (int i = 0; i < cnt; i++) {
				Integer param_type = null;
				try {
					param_type = new Integer(pmeta.getParameterType(i + 1));
				} catch (Exception e) {
					// nothing todo (Some Drivers not support getParameterType)
				}
				if (param_type == null) {
					// Oracle cann't getParameterType
					stmp.setObject(i + 1, values[i]);
				} else {
					stmp.setObject(i + 1, values[i], param_type.intValue());
				}
			}
			result = stmp.executeUpdate();
		}
		return result;
	}

}
