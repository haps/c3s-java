/**
 * 
 */
package org.c3s.db.beans;

import java.util.ArrayList;
import java.util.Collection;

import org.c3s.exceptions.XMLException;
import org.c3s.xml.utils.XMLUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * @author azajac
 *
 */
public class BeansList<T extends Bean> extends ArrayList<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3654550107967974961L;

	/**
	 * 
	 */
	public BeansList() {
	}

	/**
	 * @param initialCapacity
	 */
	public BeansList(int initialCapacity) {
		super(initialCapacity);
	}

	/**
	 * @param c
	 */
	public BeansList(Collection<T> c) {
		super(c);
	}

	public Document toXML() throws XMLException {
		return toXML("data");
	}
	
	public Document toXML(String root) throws XMLException {
		Document doc = XMLUtils.createXML(root);
		Element elm = doc.getDocumentElement();
		for (int i=0; i < size(); i++) {
			XMLUtils.appendClonedNode(elm, get(i).toXML().getDocumentElement());
		}
		return doc;
	}
}
