/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.c3s.db.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.c3s.exceptions.XMLException;
import org.c3s.xml.utils.XMLUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author azajac
 */
public abstract class Bean {
	
	/**
	 * Generated name ==> Original name 
	 */
	protected Map<String, String> map = new HashMap<String, String>();
	
	/**
	 * Original name ==> Value (Object)
	 */
	protected Map<String, Object> values = new HashMap<String, Object>();

	protected List<Bean> childs = new ArrayList<Bean>();

	
	public Set<String> getOriginalNames() {
		return values.keySet();
	}
	
	public Object get(String name) {
		return values.get(name);
	}

	public void set(String name, Object value) {
		values.put(name, value);
	}

	protected Object getByMap(String name) {
		Object ret = null;
		if (map.containsKey(name)) {
			ret = get(map.get(name));
		}
		return ret;
	}

	protected void setByMap(String name, Object value) {
		if (map.containsKey(name)) {
			values.put(map.get(name), value);
		}
	}

	public void add(List<Bean> beans) {
		childs.addAll(beans);
	}

	public void add(Bean child) {
		childs.add(child);
	}

	public List<Bean> getChilds() {
		return childs;
	}


	public void fillBean(Map<String, Object> values) {
		for (String key: values.keySet()) {
			set(key, values.get(key));
		}
	}

	public void setAutoincrementField(Object value) {
	}
	
	/*
	 * Some export methods
	 */
	public String toString() {
		return toString(true);
	}
	
	public String toString(boolean deep) {
		String str = "<row>\n";
		
		Set<String> keys = map.keySet();
		for (String key : keys) {
			str += "\t<col ";
			str += "name=\""+key+"\" value=\""+getByMap(key)+"\" original_name=\""+map.get(key)+"\"";
			str += "/>\n";
		}
		if (deep && childs.size() > 0) {
			str += "\t<childs>";
			for (Bean child : childs) {
				str += child.toString();
			}
			str += "\t</childs>";
		}
		str += "</row>\n";
		
		return str;
	}
	
	public Document toXML() throws XMLException {
		Document xml = XMLUtils.createXML("row");
		Element root = xml.getDocumentElement();
		Set<String> keys = map.keySet();
		for (String key : keys) {
			Element elm = xml.createElement("col");
			elm.setAttribute("name", key);
			if (getByMap(key) != null) {
				elm.setAttribute("value", getByMap(key).toString());
			}
			elm.setAttribute("original_name", map.get(key));
			root.appendChild(elm);
		}
		if (childs.size() > 0) {
			Element elm = xml.createElement("childs");
			root.appendChild(elm);
			for (Bean child : childs) {
				XMLUtils.appendClonedNode(elm, child.toXML().getDocumentElement());
			}
		}
		return xml;
	}
	
	public Map<String, Object> getOriginalKeyValue() {
		return values;
	}
}
