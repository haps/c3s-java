/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.c3s.db;

import org.c3s.exceptions.XMLException;
import org.c3s.xml.utils.XMLUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author azajac
 */
public class DBColumnMetadata {
	private String	columnRealName;
	private String	columnTable;
	private String	columnName;
	private String	columnLabel;
	private int		columnType = 1;
	private String	columnTypeStr = "java.lang.String";
	private boolean columnIsAuctoincrement = false;
	private boolean isPrimary = false;
	private String columnGeneratedName;
	private String fkSchema;
	private String fkTable;
	private String fkColumn;

	@Override
	public String toString() {
		return
			"\nDBColumnMetadata"
			+"\n\tName: "+columnRealName
			+"\n\tXMLName: "+columnName
			//+"\n\tXMLName: "+columnGeneratedName
			+"\n\tLabel: "+columnLabel
			+"\n\tType: "+columnTypeStr
			+"\n\tisAutoinc: "+columnIsAuctoincrement
			+"\n\tisPrimary: "+isPrimary
			+"\n";
	}
	/**
	 * @return the columnRealName
	 */
	public String getColumnRealName() {
		return columnRealName;
	}

	/**
	 * @param columnRealName the columnRealName to set
	 */
	public void setColumnRealName(String columnRealName) {
		this.columnRealName = columnRealName;
	}

	/**
	 * @return the columnTable
	 */
	public String getColumnTable() {
		return columnTable;
	}

	/**
	 * @param columnTable the columnTable to set
	 */
	public void setColumnTable(String columnTable) {
		this.columnTable = columnTable;
	}

	/**
	 * @return the columnName
	 */
	public String getColumnName() {
		return columnName;
	}

	/**
	 * @param columnName the columnName to set
	 */
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	/**
	 * @return the columnType
	 */
	public int getColumnType() {
		return columnType;
	}

	/**
	 * @param columnType the columnType to set
	 */
	public void setColumnType(int columnType) {
		this.columnType = columnType;
	}

	/**
	 * @return the columnTypeStr
	 */
	public String getColumnTypeStr() {
		return columnTypeStr;
	}

	/**
	 * @param columnTypeStr the columnTypeStr to set
	 */
	public void setColumnTypeStr(String columnTypeStr) {
		this.columnTypeStr = columnTypeStr;
	}

	/**
	 * @return the columnIsAuctoincrement
	 */
	public boolean isAuctoincrement() {
		return columnIsAuctoincrement;
	}

	/**
	 * @param columnIsAuctoincrement the columnIsAuctoincrement to set
	 */
	public void setIsAuctoincrement(boolean columnIsAuctoincrement) {
		this.columnIsAuctoincrement = columnIsAuctoincrement;
	}

	/**
	 * @return the isPrimary
	 */
	public boolean isPrimary() {
		return isPrimary;
	}

	/**
	 * @param isPrimary the isPrimary to set
	 */
	public void setIsPrimary(boolean isPrimary) {
		this.isPrimary = isPrimary;
	}

	/**
	 * @return the columnGeneratedName
	 */
	public String getColumnGeneratedName() {
		return columnGeneratedName;
	}

	/**
	 * @param columnGeneratedName the columnGeneratedName to set
	 */
	public void setColumnGeneratedName(String columnGeneratedName) {
		this.columnGeneratedName = columnGeneratedName;
	}

	/**
	 * @return the columnLabel
	 */
	public String getColumnLabel() {
		return columnLabel;
	}

	/**
	 * @param columnLabel the columnLabel to set
	 */
	public void setColumnLabel(String columnLabel) {
		this.columnLabel = columnLabel;
	}

	public Element toXML() throws XMLException {
		return toXML(XMLUtils.createXML("column", "utf-8"));
	}

	public Element toXML(Document doc) {
		/*
		Document xml = XMLUtils.createXML("column", "utf-8");

		Element root = xml.getDocumentElement();
		*/
		Element root = doc.createElement("metadata");

		root.setAttribute("base_name", columnRealName);
		root.setAttribute("name", columnName);
		root.setAttribute("label", columnLabel);
		root.setAttribute("type", columnType != -4?columnTypeStr:"java.lang.String");
		root.setAttribute("table", columnTable);
		root.setAttribute("autoincrement", columnIsAuctoincrement?"true":"false");

		return root;
	}

}
