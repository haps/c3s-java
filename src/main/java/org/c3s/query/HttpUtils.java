package org.c3s.query;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mozilla.universalchardet.UniversalDetector;

public class HttpUtils {

	public static ParametersHolder<String> parseQueryString(String query) throws UnsupportedEncodingException {
		return parseQueryString(query, null);
	}

	public static ParametersHolder<String> parseQueryString(String query, String codepage) throws UnsupportedEncodingException {
		return parseQueryString(query, codepage, "&");
	}

	public static ParametersHolder<String> parseQueryString2(String query, String codepage, String delimer) throws UnsupportedEncodingException {

		String cp = (codepage == null) ? "utf-8" : codepage;

		ParametersHolder<String> map = new ParametersHolder<String>();

		if (query != null && query.length() > 0) {
			
			String fromCharset = charDetect(query);
			
			//System.out.println(fromCharset);
			
			if (fromCharset == null) {
				fromCharset = cp;
			}
			
			//String[] pairs = (new String(new String(query.getBytes("ISO-8859-1")).getBytes(fromCharset)).split(delimer));
			//String[] pairs = (new String(query.getBytes("ISO-8859-1")).split(delimer));
			String[] pairs;
			/*
			if (fromCharset == null) {
				pairs = query.split(delimer);
			} else
			*/
			if (cp.equalsIgnoreCase(fromCharset)) {
				pairs = (new String(query.getBytes("ISO-8859-1"), "utf-8")).split(delimer);
			} else {
				pairs = (new String(new String(query.getBytes("ISO-8859-1")).getBytes(fromCharset)).split(delimer));
			}

			for (String pair : pairs) {
				if (pair.length() > 0) {
					String[] nv = pair.split("=", 2);
					String name = URLDecoder.decode(nv[0], cp);
					//String name = unescape(nv[0]);
					String value = "";
					if (nv.length > 1) {
						value = URLDecoder.decode(nv[1], cp);
						value = unescape(nv[1]);
						//value = nv[1];
					}
					map.put(name, value);
				}
			}
		}
		return map;
	}

	public static ParametersHolder<String> parseQueryString(String query, String codepage, String delimer) throws UnsupportedEncodingException {

		String cp = (codepage == null) ? "utf-8" : codepage;

		ParametersHolder<String> map = new ParametersHolder<String>();

		if (query != null && query.length() > 0) {
			
			String fromCharset = charDetect(query);
			
			//System.out.println(fromCharset);
			
			/*
			if (fromCharset == null) {
				fromCharset = cp;
			}
			*/
			
			//String[] pairs = (new String(new String(query.getBytes("ISO-8859-1")).getBytes(fromCharset)).split(delimer));
			//String[] pairs = (new String(query.getBytes("ISO-8859-1")).split(delimer));
			String[] pairs;
			if (fromCharset == null) {
				pairs = query.split(delimer);
			} else if (cp.equalsIgnoreCase(fromCharset)) {
				pairs = (new String(query.getBytes("ISO-8859-1"), "utf-8")).split(delimer);
			} else {
				pairs = (new String(new String(query.getBytes("ISO-8859-1")).getBytes(fromCharset)).split(delimer));
			}

			for (String pair : pairs) {
				if (pair.length() > 0) {
					String[] nv = pair.split("=", 2);
					String name = URLDecoder.decode(nv[0], cp);
					//String name = unescape(nv[0]);
					String value = "";
					if (nv.length > 1) {
						value = URLDecoder.decode(nv[1], cp);
						value = unescape(nv[1]);
						//value = nv[1];
					}
					map.put(name, value);
				}
			}
		}
		return map;
	}
	
	public static String unescape(String s) {
		StringBuffer sbuf = new StringBuffer();
		int l = s.length();
		int ch = -1;
		int b, sumb = 0;
		for (int i = 0, more = -1; i < l; i++) {
			/* Get next byte b from URL segment s */
			switch (ch = s.charAt(i)) {
			case '%':
				ch = s.charAt(++i);
				int hb = (Character.isDigit((char) ch) ? ch - '0' : 10 + Character.toLowerCase((char) ch) - 'a') & 0xF;
				ch = s.charAt(++i);
				int lb = (Character.isDigit((char) ch) ? ch - '0' : 10 + Character.toLowerCase((char) ch) - 'a') & 0xF;
				b = (hb << 4) | lb;
				break;
			case '+':
				b = ' ';
				break;
			default:
				b = ch;
			}
			/* Decode byte b as UTF-8, sumb collects incomplete chars */
			if ((b & 0xc0) == 0x80) { // 10xxxxxx (continuation byte)
				sumb = (sumb << 6) | (b & 0x3f); // Add 6 bits to sumb
				if (--more == 0)
					sbuf.append((char) sumb); // Add char to sbuf
			} else if ((b & 0x80) == 0x00) { // 0xxxxxxx (yields 7 bits)
				sbuf.append((char) b); // Store in sbuf
			} else if ((b & 0xe0) == 0xc0) { // 110xxxxx (yields 5 bits)
				sumb = b & 0x1f;
				more = 1; // Expect 1 more byte
			} else if ((b & 0xf0) == 0xe0) { // 1110xxxx (yields 4 bits)
				sumb = b & 0x0f;
				more = 2; // Expect 2 more bytes
			} else if ((b & 0xf8) == 0xf0) { // 11110xxx (yields 3 bits)
				sumb = b & 0x07;
				more = 3; // Expect 3 more bytes
			} else if ((b & 0xfc) == 0xf8) { // 111110xx (yields 2 bits)
				sumb = b & 0x03;
				more = 4; // Expect 4 more bytes
			} else /* if ((b & 0xfe) == 0xfc) */{ // 1111110x (yields 1 bit)
				sumb = b & 0x01;
				more = 5; // Expect 5 more bytes
			}
			/* We don't test if the UTF-8 encoding is well-formed */
		}
		return sbuf.toString();
	}

	private static String charDetect(String str) throws UnsupportedEncodingException {
		String ret = null;
		
		UniversalDetector detector = new UniversalDetector(null);
		detector.reset();
		detector.handleData(str.getBytes("ISO-8859-1"), 0, str.length());
		detector.dataEnd();
		
		ret = detector.getDetectedCharset();
		
		return ret;
	}

	private static Map<String, List<String>> patternsCache = new HashMap<String, List<String>>();
	/**
	 * 
	 * @param name
	 * @return
	 */
	public static List<String> parseParameterName(String name) {
		List<String> result = patternsCache.get(name);
		if (result == null) {
			result = new ArrayList<String>();
			int startIdx = name.indexOf('[');
			if (startIdx > -1) {
				String source = name;
				int lastIdx = 0;
				while ((lastIdx = source.lastIndexOf('[')) != -1) {
					//System.out.println(source);
					String part = source.substring(lastIdx + 1);
					//System.out.println(part);
					if (part.indexOf(']') == part.length() - 1) {
						result.add(0, part.substring(0, part.length() - 1));
						source = source.substring(0, lastIdx);
					} else {
						break;
					}
				}
				if (source.length() > 0) {
					result.add(0, source);
				}
			} else {
				result.add(name);
			}
			patternsCache.put(name, result);
		}
		return result;
	}
}
