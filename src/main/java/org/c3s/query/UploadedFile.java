package org.c3s.query;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.sax.BodyContentHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;


public class UploadedFile {

	private static Logger logger = LoggerFactory.getLogger(UploadedFile.class);
	
	private String uploadedName = "upload" + UUID.randomUUID() + ".tmp";
	
	private String fileName;
	private String fieldName;
	private String contentType;
	private long fileSize;
	
	private File uploadedFile;
	private File uploadPath;
	
	private InputStream inputStream;
	
	public UploadedFile(File uploadPath, FileItem item) {
		this.uploadPath = uploadPath;
		
		this.fileName = item.getName();
		this.fieldName = item.getFieldName();
		this.contentType = item.getContentType();
		this.fileSize = item.getSize();
		
		try {
			this.inputStream = item.getInputStream();
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
	}

	public UploadedFile(DiskFileItem item) {
		this.uploadedFile = item.getStoreLocation();
		this.uploadedName = this.uploadedFile.getName();
		
		this.fileName = item.getName();
		this.fieldName = item.getFieldName();
		this.contentType = item.getContentType();
		this.fileSize = item.getSize();
		
		try {
			this.inputStream = item.getInputStream();
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
	}
	/**
	 * @return
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @return
	 */
	public String getFieldName() {
		return fieldName;
	}

	/**
	 * @return
	 */
	public String getContentType() {
		return contentType;
	}

	/**
	 * @return
	 */
	public long getFileSize() {
		return fileSize;
	}
	
	/**
	 * @return
	 */
	public String getUploadedName() {
		return uploadedName;
	}
	
	/**
	 * @return
	 */
	public File getUploadedFile() {
		if (uploadedFile == null) {
			uploadedFile = new File(uploadPath, uploadedName);
		}
		return uploadedFile;
	}
	
	
	private Metadata metadata;
	/**
	 * Try to detect type of file and return it
	 * @return
	 */
	public String getRealContentType() {
		//return new MimetypesFileTypeMap().getContentType(getUploadedFile());
		return getMetadata().get("Content-Type");
	}
	
	public Metadata getMetadata() {
		if (metadata == null && inputStream != null) {
			metadata = new Metadata();
			ParseContext parseContext = new ParseContext();
			ContentHandler contentHandler = new BodyContentHandler(-1);
			AutoDetectParser parser = new AutoDetectParser();
			try {
				parser.parse(inputStream, contentHandler, metadata, parseContext);
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
			} catch (SAXException e) {
				logger.error(e.getMessage(), e);
			} catch (TikaException e) {
				logger.error(e.getMessage(), e);
			}
		}
		return metadata;
	}

	/**
	 * @return the inputStream
	 */
	public InputStream getInputStream() {
		return inputStream;
	}
}
