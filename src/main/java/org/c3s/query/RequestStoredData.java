/**
 * 
 */
package org.c3s.query;

import java.io.Serializable;
import java.util.Map;
import java.util.UUID;

import org.c3s.storage.StorageInterface;

/**
 * @author alexeyz
 *
 */
public class RequestStoredData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4837045205402565235L;

	private StorageInterface storage = null;
	
	private Object data = null;
	
	private Map<String, Object> requestData = null;
	
	private String guid = null;
	
	private String actionClass = null;
	private String actionMethod = null;
	
	private RequestType requestType = RequestType.NONE;
	/**
	 * 
	 */
	public RequestStoredData(StorageInterface storage) {
		this.storage = storage;
		this.guid = UUID.randomUUID().toString();
	}

	public void remove() {
		storage.remove(getGuid());
	}

	public void register(String clazz, String method) {
		registerAction(clazz, method);
		storage.put(getGuid(), this);
	}

	public void register(Object obj, String method) {
		register(obj.getClass().getName(), method);
	}
	
	
	public void registerAction() {
		StackTraceElement trace = Thread.currentThread().getStackTrace()[2];
		registerAction(trace.getClassName(), trace.getMethodName());
	}
		
	public void registerAction(Object obj, String method) {
		registerAction(obj.getClass().getName(), method);
	}

	public void registerAction(String clazz, String method) {
		actionClass = clazz;
		actionMethod = method;
	}
	
	
	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getGuid() {
		return guid;
	}

	
	public Map<String, Object> getRequestData() {
		return requestData;
	}

	public void setRequestData(Map<String, Object> requestData) {
		this.requestData = requestData;
	}

	public RequestType getRequestType() {
		return requestType;
	}

	public void setRequestType(RequestType requestType) {
		this.requestType = requestType;
	}

	/**
	 * @return the actionClass
	 */
	public String getActionClass() {
		return actionClass;
	}

	/**
	 * @return the actionMethod
	 */
	public String getActionMethod() {
		return actionMethod;
	}

	
}
