/**
 * 
 */
package org.c3s.query;

/**
 * @author alexeyz
 *
 */
public enum RequestType {
	NONE("___NONE___"), 
	GET("___GETDATA___"), 
	POST("____POSTDATA___"), 
	REQUEST("____REQUESTDATA__"), 
	FILE("___FILESDATA__");
	
	private String value;
	private RequestType(String value) {
		this.value = value;
	}
	
	public String toString() {
		return value;
	}
	
}
