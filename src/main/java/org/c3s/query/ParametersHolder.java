package org.c3s.query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class ParametersHolder<T> {
	
	private Map<String, List<T>> parameters = null;
	
	public Map<String, List<T>> getAllParameters() {
		return parameters;
	}
	
	public ParametersHolder() {
		this(new HashMap<String, List<T>>());
	}

	public ParametersHolder(Map<String, List<T>> parameters) {
		this.parameters = parameters;
		//System.out.println("ParametersHolder");
	}
	
	public void putAll(ParametersHolder<T> holder) {
		parameters.putAll(holder.getAllParameters());
	}
	
	public T getParameter(String name) {
		//System.out.println("getParameter");
		T value = null;
		T[] values = getParameterValues(name);
		if (values != null && values.length > 0) {
			value = values[0];
		}
		return value;
	}
	
	@SuppressWarnings("unchecked")
	public T[] getParameterValues(String name) {
		//System.out.println("getParameterValues");
		T[] values = null;
		if (parameters.containsKey(name)) {
			values = (T[]) parameters.get(name).toArray(new Object[parameters.get(name).size()]);
		}
		return values;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, T[]> getParameterMap() {
		//System.out.println("getParameterMap");
		Map<String, T[]> map = new HashMap<String, T[]>();
		
		Iterator<String> iterator = getParameterNames().iterator();
		while (iterator.hasNext()) {
			String name = iterator.next();
			T[] values = (T[]) parameters.get(name).toArray(new Object[parameters.get(name).size()]);
			map.put(name, values);
		}
		return map;
	}
	
	public Set<String> getParameterNames() {
		//System.out.println("getParameterNames");
		return parameters.keySet();
	}
	
	public void put(String name, T value) {
		if (!parameters.containsKey(name)) {
			//System.out.println("put");
			List<T> list = new ArrayList<T>();
			parameters.put(name, list);
		}
		//System.out.println(name + ">--------------" + value);
		parameters.get(name).add(value);
		putTreeParameter(name, value);
	}

	public void put(String name, List<T> value) {
		//System.out.println("put2");
		if (!parameters.containsKey(name)) {
			parameters.get(name).addAll(value);
		} else {
			parameters.put(name, value);
		}
		for (T val: value) {
			putTreeParameter(name + "[]", val);
		}
		// TODO ADDDDDD
	}
	
	public String toString() {
		String str = "";
		Map<String, T[]> map = getParameterMap();
		Iterator<String> iterator = map.keySet().iterator();
		while(iterator.hasNext()) {
			String key = iterator.next();
			str +=  "{" + key + "}";
		}
		//str = map.toString();
		return str;
	}
	
	public int size() {
		return parameters.size();
	}
	
	/**
	 * 
	 */
	private Map<String, Object> parametersTree = null;
	/**
	 * 
	 */
	/**
	 * @param name
	 * @param value
	 */
	private void  putTreeParameter(String name, T value) {
		List<String> names = HttpUtils.parseParameterName(name);
		if (names.size() > 0) {
			if (parametersTree == null) {
				parametersTree = new TreeMap<String, Object>();
			}
			putTreeParameter(names, value, parametersTree);
		}
	}
	
	/**
	 * @param names
	 * @param value
	 * @param holder
	 */
	@SuppressWarnings("unchecked")
	private void putTreeParameter(List<String> names, T value, Map<String, Object> holder) {
		Map<String, Object> map = holder;
		for (int i = 0; i < names.size(); i++) {
			String key = names.get(i);
			String realKey = key;
			if (key.length() == 0) {
				long lKey = 0L;
				for (String sKey: map.keySet()) {
					try {
						long tmp = Long.parseLong(sKey);
						if (tmp >= lKey) {
							lKey = tmp + 1;
						}
					} catch (NumberFormatException e) {}
				}
				realKey = String.valueOf(lKey);
			}
			
			if (!map.containsKey(realKey) && i < names.size() - 1) {
				Map<String, Object> submap = new TreeMap<String, Object>();
				map.put(realKey, submap);
				map = submap;
			} else if (i == names.size() - 1) {
				map.put(realKey, value);
			} else {
				map = (Map<String, Object>)map.get(realKey);
			}
		}
	}
	

	@SuppressWarnings("unchecked")
	public Object getObjectParameter(String name) {
		Object result = null;
		Map<String, Object> commonmap = parametersTree;
		if (name != null && name.length() > 0) {
			int i = 0;
			List<String> names = HttpUtils.parseParameterName(name);
			for (String key: names) {
				i++;
				Object map = commonmap.get(key);
				if (map == null) {
					result = null;
					break;
				} else if (i < names.size()) {
					commonmap = (Map<String, Object>)map;
					result = commonmap;
				} else {
					result = map;
				}
			}
		}
		return result;
	}

	public String getStringParameter(String name) {
		Object result = getObjectParameter(name);
		return result == null? null : result.toString();
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> getTreeParameter(String name) {
		return (Map<String, Object>) getObjectParameter(name);
	}
	
	public Map<String, Object> getTreeParameter() {
		Map<String, Object> result = parametersTree;
		return result;
	}
}
