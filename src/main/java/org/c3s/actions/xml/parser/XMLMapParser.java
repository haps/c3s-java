package org.c3s.actions.xml.parser;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.c3s.actions.ActionMapModuleInterface;
import org.c3s.actions.xml.parser.handlers.BaseHandler;
import org.c3s.actions.xml.parser.handlers.IncludeHandler;
import org.c3s.actions.xml.parser.handlers.ModuleHandler;
import org.c3s.actions.xml.parser.handlers.ParameterHandler;
import org.c3s.parsers.ElementHandler;
import org.c3s.parsers.FileParser;

public class XMLMapParser extends FileParser {

	private XMLMapContext context;
	
	private File file;
	
	public XMLMapParser(File file) {
		this(file, new XMLMapContext());
	}

	public XMLMapParser(File file, XMLMapContext context) {
		this.file = file;
		this.context = context;
		this.context.addDependence(file);
		this.context.setRootFile(file.getParentFile());
	}
	
	
	@Override
	public File getFile() {
		return file;
	}

	@Override
	public Map<String, ElementHandler> getHandlers() {
		Map<String, ElementHandler> map = new HashMap<String, ElementHandler>();

		map.put("config", new BaseHandler(context));
		map.put("modules", new BaseHandler(context));
		map.put("module", new ModuleHandler(context));
		map.put("param", new ParameterHandler(context));
		map.put("parameter", new ParameterHandler(context));
		map.put("include", new IncludeHandler(context));
		
		return map;
	}

	public List<ActionMapModuleInterface> getModules() {
		return context.getModules();
	}
	
	public List<File> getDependences() {
		return context.getDependences();
	}
}
