package org.c3s.actions.xml.parser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.c3s.actions.ActionMapModuleInterface;

public class XMLMapContext {
	
	private List<ActionMapModuleInterface> modules = new ArrayList<ActionMapModuleInterface>();
	
	private List<ActionMapModuleInterface> modulesStack = new ArrayList<ActionMapModuleInterface>();

	private List<File> dependences = new ArrayList<File>();
	/**
	 * @return the currentModule
	 */
	public ActionMapModuleInterface getCurrentModule() {
		return (modulesStack.size() > 0)?modulesStack.get(modulesStack.size() - 1):null;
	}

	/**
	 * @param currentModule the currentModule to set
	 */
	public void setCurrentModule(ActionMapModuleInterface currentModule) {
		if (modulesStack.size() == 0) {
			modules.add(currentModule);
		}
		modulesStack.add(currentModule);
	}

	public ActionMapModuleInterface getCurrentModuleLevelUp() {
		if (modulesStack.size() > 0) {
			modulesStack.remove(modulesStack.size() - 1);
		}
		return (modulesStack.size() > 0)?modulesStack.get(modulesStack.size() - 1):null;
	}
	
	/**
	 * @return the modules
	 */
	public List<ActionMapModuleInterface> getModules() {
		return modules;
	}
	
	public void addDependence(File dependence) {
		this.dependences.add(dependence);
	}
	
	public List<File> getDependences() {
		return this.dependences;
	}

	/**
	 * 
	 */
	private File rootFile;
	/**
	 * @return the rootFile
	 */
	public File getRootFile() {
		return rootFile;
	}

	/**
	 * @param rootFile the rootFile to set
	 */
	public void setRootFile(File rootFile) {
		this.rootFile = rootFile;
	}
}
