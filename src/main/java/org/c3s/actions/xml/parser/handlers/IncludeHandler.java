package org.c3s.actions.xml.parser.handlers;

import java.io.File;

import org.c3s.actions.xml.parser.XMLMapContext;
import org.c3s.actions.xml.parser.XMLMapParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

public class IncludeHandler extends BaseHandler {

	private static Logger logger = LoggerFactory.getLogger(IncludeHandler.class);	
	
	public IncludeHandler(XMLMapContext context) {
		super(context);
	}

	@Override
	public void performAction(Element element) throws Exception {
		String href = element.getAttribute("href");
		if (href.length() > 0) {
			try {
				
				//File file = FileSystem.newFile(href);
				File file = new File(getContext().getRootFile(), href);
				
				if (file.exists() && file.isFile()) {
					//getContext().addDependence(file);
					
					XMLMapParser parser = new XMLMapParser(file, getContext());
					parser.parse();
					/**
					for (ActionMapModuleInterface module: parser.getModules()) {
						((GeneralActionMapModule)getContext().getCurrentModule()).addModule(module);
					}
					*/
					
				} else {
					logger.error("Include file \""+href+"\" not exists! Ingnoring...");
				}
			} catch (Exception e) {
				logger.error("Error parse include file \""+href+"\"", e);
				//e.printStackTrace();
			}
		}
	}
}
