package org.c3s.actions.xml.parser.handlers;

import java.util.List;
import java.util.Map;

import org.c3s.actions.ActionMapModuleInterface;
import org.c3s.actions.GeneralActionMapModule;
import org.c3s.actions.ModuleParameter;
import org.c3s.actions.xml.parser.XMLMapContext;
import org.w3c.dom.Element;

public class ModuleHandler extends BaseHandler {

	public ModuleHandler(XMLMapContext context) {
		super(context);
	}

	@Override
	public void performAction(Element node) {
		
		ParsedModule module = new ParsedModule();
		
		module.setClass(node.getAttribute("class"));
		
		module.setAction(node.getAttribute("action"));
		
		module.setInstanceName(node.getAttribute("instance"));
		
		module.setIsLast("true".equals(node.getAttribute("is_last")));

		ActionMapModuleInterface parent = getContext().getCurrentModule();

		String pat = node.getAttribute("pattern");
		if (pat.length() == 0) {
			pat = node.getAttribute("regexp");
			if (pat.length() == 0) {
				if (parent != null) {
					pat = parent.getRegexp();
				} else {
					pat = "/";
				}
			}
		} else {
			//pat = pat.replace("/", "\\/");
			pat = pat.replace("*", ".*");
			pat = pat.replace(".*/", "[^/]+/");
			if (parent != null) {
				pat = parent.getRegexp() + pat;
			}
		}
		module.setRegexp(pat);
		
		if (parent != null) {
			module.setInherited(parent.getInherited());
			((ParsedModule)parent).addModule(module);
		}
		
		getContext().setCurrentModule(module);
	}
	
	@Override
	public void subTreeComplete() throws Exception {
		getContext().getCurrentModuleLevelUp();
	}
	
	private static class ParsedModule extends GeneralActionMapModule {
		
		public void setClass(String clazz) {
			_class =  clazz;
		}
		
		public void setAction(String action) {
			_action =  action;
		}
		
		public void setRegexp(String regexp) {
			_regexp = regexp;
		}
		
		public void setIsLast(boolean isLast) {
			_is_last = isLast;
		}
		
		public void setInstanceName(String instanceName) {
			_instance_name = instanceName;
		}
		
		public void setInherited(Map<String, List<ModuleParameter>> inherit) {
			if (inherit != null) {
				for (String key: inherit.keySet()) {
					for (ModuleParameter param: inherit.get(key)) {
						addParameter(param);
					}
				}
			}
		}
	}
}
