package org.c3s.actions.xml.parser.handlers;

import org.c3s.actions.xml.parser.XMLMapContext;

public class BaseHandler extends org.c3s.parsers.handlers.BaseHandler {

	private XMLMapContext context;
	
	public BaseHandler(XMLMapContext context) {
		this.context = context;
	}

	/**
	 * @return the context
	 */
	public XMLMapContext getContext() {
		return context;
	}

}
