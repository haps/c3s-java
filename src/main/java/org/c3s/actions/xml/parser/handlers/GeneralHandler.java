package org.c3s.actions.xml.parser.handlers;

import org.c3s.actions.xml.parser.XMLMapContext;

public class GeneralHandler extends BaseHandler {

	public GeneralHandler(XMLMapContext context) {
		super(context);
	}

	public boolean isHandlesSubTree() {
		return true;
	}
}
