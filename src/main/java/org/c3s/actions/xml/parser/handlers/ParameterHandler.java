package org.c3s.actions.xml.parser.handlers;

import org.c3s.actions.ModuleParameter;
import org.c3s.actions.xml.parser.XMLMapContext;
import org.w3c.dom.Element;

public class ParameterHandler extends GeneralHandler {

	public ParameterHandler(XMLMapContext context) {
		super(context);
	}

	@Override
	public void performAction(Element element) throws Exception {
		String name = element.getAttribute("name");
		String value = element.getAttribute("value");
		boolean inherit = "true".equals(element.getAttribute("inherit"));
		
		if (!name.isEmpty()) {
			getContext().getCurrentModule().addParameter(new ModuleParameter(name, value, inherit));
		}
	}
}
