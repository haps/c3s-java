/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.c3s.actions.xml;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.c3s.actions.ActionMapInterface;
import org.c3s.actions.ActionMapModuleInterface;
import org.c3s.actions.xml.parser.XMLMapParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author admin
 */
public class XMLActionMap implements ActionMapInterface {

	private static Logger logger = LoggerFactory.getLogger(XMLActionMap.class);
	
	private List<ActionMapModuleInterface> modules;
	
	private static Map<String, Cache> caches = new ConcurrentHashMap<String, XMLActionMap.Cache>();
	
	//private static Object lock = new Object();

	public XMLActionMap(File file) throws Exception {
		synchronized (caches) {
			if (!checkFiles(file.getAbsolutePath())) {
				logger.debug("Reload Action map file: " + file.getCanonicalPath());
				System.out.println("Reload Action map file: " + file.getCanonicalPath());
				XMLMapParser parser = new XMLMapParser(file);
				parser.parse();
				modules = parser.getModules();
				Cache cache = new Cache();
				cache.setFiles(parser.getDependences());
				cache.setModules(modules);
				caches.put(file.getAbsolutePath(), cache);
			} else {
				modules = caches.get(file.getAbsolutePath()).getModules();
			}
		}
	}
	
	public List<ActionMapModuleInterface> getModules() {
		return modules;
	}

	private boolean checkFiles(String filename) {
		boolean result = false;

		Cache cache = caches.get(filename);
		if (cache != null) {
			for (File file: cache.getFiles()) {
				result = checkFileChange(file);
				if (!result) {
					break;
				}
			}
		}
		return result;
	}
	
	private boolean checkFileChange(File file) {
		boolean result = false;
		
		File checkFile = new File(file.getAbsolutePath());
		
		if (checkFile.length() == file.length() && checkFile.lastModified() == file.lastModified()) {
			result = true;
		}
		
		return result;
	}
	
	private static class Cache {
		private List<ActionMapModuleInterface> modules;
		private List<File> files;
		/**
		 * @return the modules
		 */
		public List<ActionMapModuleInterface> getModules() {
			return modules;
		}
		/**
		 * @return the files
		 */
		public List<File> getFiles() {
			return files;
		}
		/**
		 * @param modules the modules to set
		 */
		public void setModules(List<ActionMapModuleInterface> modules) {
			this.modules = modules;
		}
		/**
		 * @param files the files to set
		 */
		public void setFiles(List<File> files) {
			this.files = files;
		}
	}
}
