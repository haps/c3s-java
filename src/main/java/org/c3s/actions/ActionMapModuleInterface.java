/**
 * 
 */
package org.c3s.actions;

import java.util.List;
import java.util.Map;

/**
 * @author azajac
 *
 */
public interface ActionMapModuleInterface {

	public String getRegexp();
	
	public void addParameter(ModuleParameter value);
	
	public Map<String, List<String>> getParameters();
	
	public Map<String, List<ModuleParameter>> getInherited();
	
	public List<ActionMapModuleInterface> getModules();
	
	public String getClassName();
	
	public String getAction();
	
	public boolean isLast();
	
	public String getInstanceName();
}
