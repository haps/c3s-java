/**
 * 
 */
package org.c3s.actions;

/**
 * @author azajac
 *
 */
public interface ControlledModuleInterface {

	public boolean isCanRun();
	
	public void setCanRun(boolean can_run);
	
	public void setChildsRun(boolean can_run);
}
