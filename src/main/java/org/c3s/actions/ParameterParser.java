package org.c3s.actions;

import org.c3s.lib.parsers.Callable;
import org.c3s.lib.parsers.StringParser;
import org.c3s.lib.parsers.interceptors.CharInterceptor;
import org.c3s.lib.parsers.interceptors.RegMatchInterceptor;
import org.c3s.web.context.ApplicationContext;

public class ParameterParser extends StringParser {

	public static final int STATE_NONE        = 0;
	public static final int STATE_START_BRACE = 1;
	public static final int STATE_CLOSE_BRACE = 2;
	
	
	private int state = STATE_NONE;
	private String result = "";
	private String current = "";
	
	
	public ParameterParser() {
		registerHandler(new CharInterceptor("{"), new Callable(this, "openBrace"));
		registerHandler(new CharInterceptor("}"), new Callable(this, "closeBrace"));
		registerHandler(new RegMatchInterceptor("~[^{}]~is"), new Callable(this, "otherChar"));
	}

	protected boolean openBrace(Character ch) {
		switch (state) {
			case STATE_NONE:
				state = STATE_START_BRACE;
				break;
			case STATE_START_BRACE:
				result += '{';
				state = STATE_NONE;
				break;
			case STATE_CLOSE_BRACE:
				result += getParsedValue();
				current = "";
				state = STATE_START_BRACE;
				break;
			default:
				error(ch);
		}
		return false;
	}

	protected boolean closeBrace(Character ch) {
		switch (state) {
			case STATE_NONE:
				result += ch;
				break;
			case STATE_START_BRACE:
				state = STATE_CLOSE_BRACE;
				break;
			case STATE_CLOSE_BRACE:
				otherChar(null);
				break;
			default:
				error(ch);
		}
		return false;
	}

	protected boolean otherChar(Character ch) {
		switch (state) {
			case STATE_NONE:
				result += ch;
				break;
			case STATE_START_BRACE:
				current += ch; 
				break;
			case STATE_CLOSE_BRACE:
				result += getParsedValue();
				current = "";
				result += (ch != null)?ch:"";
				state = STATE_NONE;
				break;
			default:
				error(ch);
		}
		return false;
	}
	
	protected void onStart(Object source) {
		state = STATE_NONE;
		result = "";
		current = "";
	}
	
	
	protected void onStop(Object source) {
		if (state == STATE_CLOSE_BRACE) {
			otherChar(null);
		} else if (state == STATE_START_BRACE) {
			result += '{' + current;
		}
	}
	
	protected String getParsedValue() {
		String ret = "{}";
		if (current.length() > 0) {
			if ((ret = ApplicationContext.getInstance().getProperty(current)) == null) {
				if ((ret = System.getProperty(current)) == null) {
					ret = '{' + current + '}';
				}
			}
		}
		return ret;
	}
	
	public String getResult() {
		//System.out.println("|" + result + "|:" + result.length());
		return result;
	}
	
}
