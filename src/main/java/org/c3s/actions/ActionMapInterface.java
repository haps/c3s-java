/**
 * 
 */
package org.c3s.actions;

import java.util.List;

/**
 * @author azajac
 *
 */
public interface ActionMapInterface {

	public List<ActionMapModuleInterface> getModules();
	
}
