/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.c3s.actions;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.c3s.actions.xml.XMLActionMap;
import org.c3s.utils.Utils;

/**
 *
 * @author admin
 */
public class ActionMapHolder {

	private static Map<String, ActionMapInterface>  map = new HashMap<String, ActionMapInterface>();

	public static ActionMapInterface getActionMap(File file) throws Exception {

		String path = file.getAbsolutePath();
		String size = Long.toString(file.length());
		String modify = Long.toString(file.lastModified());

		String hash = Utils.MD5(path + size + modify);

		ActionMapInterface am = null;
		if ((am = map.get(hash)) == null) {
			System.out.println("Reload Action map");
			if (file.getPath().toLowerCase().endsWith(".xml")) {
				am = new XMLActionMap(file) ;
			}
			map.put(hash, am);
		}
		return am;
	}

}
