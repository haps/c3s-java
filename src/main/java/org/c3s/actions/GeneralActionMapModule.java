/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.c3s.actions;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/**
 *
 * @author admin
 */
public class GeneralActionMapModule implements ActionMapModuleInterface {

	protected List<ActionMapModuleInterface> modules = new ArrayList<ActionMapModuleInterface>();
	//protected Map<String, List<String>> parameters;
	protected Map<String, List<ModuleParameter>> parameters;
	protected Map<String, List<ModuleParameter>> inherited;

	protected String _class;
	protected String _action;
	protected boolean _is_last = false;
	protected String _regexp;
	protected String _instance_name;

	protected static ParameterParser _parser = new ParameterParser();
	
	public String getRegexp() {
		return _regexp;
	}

	public void addParameter(ModuleParameter param) {
		
		String key = param.getName();
		
		if (parameters == null) {
			parameters = new LinkedHashMap<String, List<ModuleParameter>>();
		}
		
		if (!parameters.containsKey(key)) {
			parameters.put(key, new ArrayList<ModuleParameter>());
		}
		
		parameters.get(key).add(param);
		
		if (param.isInherit()) {
			if (inherited == null) {
				inherited = new LinkedHashMap<String, List<ModuleParameter>>();
			}
			if (!inherited.containsKey(key)) {
				inherited.put(key, new ArrayList<ModuleParameter>());
			}
			inherited.get(key).add(param);
		} else if (inherited != null) {
			inherited.remove(key);
		}
	}
	
	
	public void addParameter(String name, String value, boolean inherit) {
		this.addParameter(new ModuleParameter(name, value, inherit));
	}
	
	
	public void addParameter(String name, String value) {
		this.addParameter(new ModuleParameter(name, value, false));
	}

	public Map<String, List<String>> getParameters() {
		//return parameters;
		return processConstantsForParameters(parameters);
	}

	protected Map<String, List<String>> processConstantsForParameters(Map<String, List<ModuleParameter>> parameters) {
		Map<String, List<String>> result = new LinkedHashMap<String, List<String>>();
		if (parameters != null && parameters.size() > 0) {
			String[] keys = parameters.keySet().toArray(new String[]{});
			for (String key: keys) {
				List<String> line = new ArrayList<String>(); 
				result.put(key, line);
				for (ModuleParameter value: parameters.get(key)) {
					String transform;
					try {
						synchronized (_parser) {
							_parser.process(value.getValue());
							transform = _parser.getResult();
						}
					} catch (Exception e) {
						e.printStackTrace();
						transform = value.getValue();
					}
					line.add(transform);
				}
			}
		}
		return result;
	}
	
	public List<ActionMapModuleInterface> getModules() {
		return modules;
	}

	/*
	 * getters
	 */

	public String getClassName() {
		return _class;
	}

	public String getAction() {
		return _action;
	}
	
	public boolean isLast() {
		return _is_last;
	}

	public String getInstanceName() {
		return _instance_name;
	}
	
	public Map<String, List<ModuleParameter>> getInherited() {
		return inherited;
	}
	
	
	public void addModule(ActionMapModuleInterface module) {
		modules.add(module);
	}
}
