/**
 * 
 */
package org.c3s.actions;

/**
 * @author admin
 *
 */
public class ModuleParameter {

	/**
	 * 
	 */
	private String name;
	
	/**
	 * 
	 */
	private String value;
	/**
	 * 
	 */
	private boolean inherit = false;
	
	
	public ModuleParameter(String name, String value, boolean inherit) {
		super();
		this.name = name;
		this.value = value;
		this.inherit = inherit;
	}
	
	public ModuleParameter(String name, String value) {
		this(name, value, false);
	}
	
	/**
	 * @return
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return
	 */
	public String getValue() {
		return value;
	}
	/**
	 * @param value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	/**
	 * @return
	 */
	public boolean isInherit() {
		return inherit;
	}
	/**
	 * @param inherit
	 */
	public void setInherit(boolean inherit) {
		this.inherit = inherit;
	}
	
}
