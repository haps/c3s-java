/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.c3s.site;

/**
 *
 * @author admin
 */
public interface LanguageInterface {
	public String getId();
	public String getName();
	public String getPrefix();
	public String getSuffix();
	public String getEncoding();
}
