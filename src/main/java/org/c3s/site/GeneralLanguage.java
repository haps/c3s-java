package org.c3s.site;

import java.util.LinkedHashMap;
import java.util.Map;

public class GeneralLanguage implements LanguageInterface {

	private String id;
	private String name;
	private String prefix;
	private String suffix;
	private String encoding = "utf-8";
	
	public GeneralLanguage(String id, String name, String prefix, String suffix, String encoding) {
		setId(id);
		setName(name);
		setEncoding(encoding);
		setPrefix(prefix);
		setSuffix(suffix);
	}

	public GeneralLanguage() {
	}
	
	public Map<String, Object> __toArray() {
		Map<String, Object> result = new LinkedHashMap<String, Object>(5);
		result.put("language_id", getId());
		result.put("name", getName());
		result.put("suffix", getSuffix());
		result.put("prefix", getPrefix());
		result.put("encoding", getEncoding());
		return result;
	}
	
	@Override
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	@Override
	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	@Override
	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

}
