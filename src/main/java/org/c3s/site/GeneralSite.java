package org.c3s.site;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GeneralSite implements Cloneable {

	/**
	 * 
	 */
	//private static final long serialVersionUID = 5721279267633563932L;

	public enum Politics {
		LANGUAGE_NONE("none"),
		LANGUAGE_PREFIX("prefix"),
		LANGUAGE_SUFFIX("suffix");
		
		private String value;
		
		private Politics(String value) {
			this.value = value;
		}
		
		public String getValue() {
			return value;
		}
		
		public static Politics getPolitics(String politics) {
			Politics result = null;
			for (Politics p : values()) {
				if (p.getValue().equals(politics)) {
					result = p;
					break;
				}
			}
			return (result != null)? result : Politics.LANGUAGE_NONE;
		}
	}
	
	
	protected GeneralSiteGroup siteGroup = null;
	protected String name = null;
	protected String config = null;
	protected List<GeneralSiteAlias> aliases = new ArrayList<GeneralSiteAlias>();
	protected GeneralSiteAlias current_alias = null;
	protected Map<String,LanguageInterface> languages = new HashMap<String, LanguageInterface>();
	protected Politics language_politicts = Politics.LANGUAGE_NONE; 
	protected LanguageInterface default_language = null;
	protected LanguageInterface current_language = null;
	
	
	public GeneralSite() {
		
	}

	public GeneralSite(String name) {
		this.name = name;
	}

	public GeneralSite(String name, String config, String politic) {
		this.name = name;
		this.config = config;
		this.language_politicts = Politics.getPolitics(politic);
	}
	
	public GeneralSiteGroup getSiteGroup() {
		return siteGroup;
	}

	public void setSiteGroup(GeneralSiteGroup siteGroup) {
		this.siteGroup = siteGroup;
	}
	
	public String getName() {
		return name;
	}
	
	public boolean isMe(String _host) {
		String host = _host.toLowerCase();
		boolean result = false;
		if (hasAlias(host) != null) {
			setCurrentAlias(hasAlias(host));
			setCurrentLanguage(getDefaultLanguage());
			result = true;
		} else if (getLanguagePolitic() == Politics.LANGUAGE_PREFIX) {
			String[] info = host.split("\\.", 2);
			LanguageInterface lang = null;
			if (info.length > 1 && hasAlias(info[1]) != null && (lang = getLanguageByPrefix(info[0])) != null) {
				setCurrentAlias(hasAlias(info[1]));
				setCurrentLanguage(lang);
				result = true;
			}
		}
		return result;
	}

	/**
	 * @return
	 */
	public String getConfig() {
		return config;
	}

	/**
	 * @param config
	 */
	public void setConfig(String config) {
		this.config = config;
	}

	/**
	 * @return
	 */
	public List<GeneralSiteAlias> getAliases() {
		return aliases;
	}

	/**
	 * @param aliases
	 */
	public void addAlias(GeneralSiteAlias alias) {
		aliases.add(alias);
	}

	
	public GeneralSiteAlias hasAlias(String alias) {
		String checkAlias = alias.toLowerCase();
		GeneralSiteAlias result = null;
		for (GeneralSiteAlias siteAlias: getAliases()) {
			if (siteAlias.getAlias().equals(checkAlias)) {
				result = siteAlias;
				break;
			}
		}
		return result;
	}
	
	/**
	 * @return
	 */
	public GeneralSiteAlias getCurrentAlias() {
		return current_alias;
	}

	/**
	 * @param current_alias
	 */
	public void setCurrentAlias(GeneralSiteAlias current_alias) {
		this.current_alias = current_alias;
	}

	/**
	 * @param language_id
	 * @return
	 */
	public boolean hasLanguage(String language_id) {
		return languages.containsKey(language_id);
	}
	/**
	 * @return
	 */
	public LanguageInterface getLanguage() {
		return null;
	}

	/**
	 * @param language
	 */
	public void addLanguage(LanguageInterface language) {
		addLanguage(language, false);
	}

	public void addLanguage(LanguageInterface language, boolean isDefault) {
		languages.put(language.getId(), language);
		if (isDefault) {
			default_language = language;
		}
	}
	
	public LanguageInterface getLanguageByPrefix(String $language_prefix) {
		LanguageInterface result = null;
		for (LanguageInterface lang: languages.values()) {
			if (lang.getPrefix().equals($language_prefix)) {
				result = lang;
				break;
			}
		}
		return result;
	}
	
	public LanguageInterface getLanguageBySuffix(String $language_suffix) {
		LanguageInterface result = null;
		for (LanguageInterface lang: languages.values()) {
			if (lang.getSuffix().equals($language_suffix)) {
				result = lang;
				break;
			}
		}
		return result;
	}
	
	/**
	 * @return
	 */
	public Politics getLanguagePolitic() {
		return language_politicts;
	}

	/**
	 * @param language_politicts
	 */
	public void setLanguagePolitic(Politics language_politicts) {
		this.language_politicts = language_politicts;
	}

	/**
	 * @return
	 */
	public LanguageInterface getDefaultLanguage() {
		return default_language;
	}

	/**
	 * @param default_language
	 */
	public void setDefaultLanguage(LanguageInterface default_language) {
		this.default_language = default_language;
	}

	/**
	 * @return
	 */
	public LanguageInterface getCurrentLanguage() {
		return current_language;
	}

	/**
	 * @param current_language
	 */
	public void setCurrentLanguage(LanguageInterface current_language) {
		this.current_language = current_language;
	}

	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	public Map<String, Object> getLanguageInfo() {
		Map<String, Object> info = (Map<String, Object>)((GeneralLanguage)getCurrentLanguage()).__toArray();
		info.put("politic", getLanguagePolitic().getValue());
		info.put("default", getCurrentLanguage().equals(getDefaultLanguage()));
		return info;
	}
	
	public Map<String, Object> __toArray() {
		Map<String, Object> info = (Map<String, Object>)getLanguageInfo();
		List<Map<String, Object>> langs = new ArrayList<Map<String, Object>>();
		for(LanguageInterface lang: languages.values()) {
			Map<String, Object> linfo = ((GeneralLanguage)lang).__toArray();
			linfo.put("current", lang.equals(getCurrentLanguage()));
			linfo.put("default", lang.equals(getDefaultLanguage()));
			langs.add(linfo);
		}
		info.put("alias", getCurrentAlias().getAlias());
		info.put("languages", langs);
		info.put("aliases", getAliases());
		return info;
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
