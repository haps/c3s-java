/**
 * 
 */
package org.c3s.site;

import java.util.List;

import javax.persistence.EntityManager;

import org.c3s.db.jpa.ManagerJPA;
import org.c3s.site.entity.Site;
import org.c3s.site.entity.SiteAlias;
import org.c3s.site.entity.SiteLanguage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author admin
 *
 */
public class SiteLoaderJPA extends SiteLoader {

	private static Logger logger = LoggerFactory.getLogger(SiteLoader.class);
	/* (non-Javadoc)
	 * @see org.c3s.site.SiteLoader#load(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public SiteLoader load(Object parameters) {
		EntityManager manager = ManagerJPA.get();
		List<Site> sites = manager.createNamedQuery("Site.findAll").getResultList();
		for (Site site: sites) {
			GeneralSite gsite = new GeneralSite(site.getName(), site.getConfig(), site.getPolitics());
			for (SiteAlias alias: site.getSiteAliases()) {
				gsite.addAlias(new GeneralSiteAlias(alias.getHost(), alias.getPattern()));
				
			}
			for (SiteLanguage lang: site.getSiteLanguages()) {
				GeneralLanguage l = new GeneralLanguage(lang.getLanguage().getId(), 
						lang.getLanguage().getName(), lang.getLanguage().getPrefix(), 
						lang.getLanguage().getSuffix(), lang.getLanguage().getEncoding());
				gsite.addLanguage(l, lang.getIsDefault() > 0);
			}
			//this.sites.add(gsite);
			addSite(gsite);
		}
		logger.debug("count of loaded sites {}", this.sites.size());
		return this;
	}

}
