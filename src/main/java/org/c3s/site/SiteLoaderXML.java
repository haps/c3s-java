package org.c3s.site;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.xpath.XPathConstants;

import org.c3s.exceptions.XMLException;
import org.c3s.utils.FileSystem;
import org.c3s.xml.DecorXPath;
import org.c3s.xml.NodeSet;
import org.c3s.xml.XMLManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class SiteLoaderXML extends SiteLoader {

	private static String ELEMENT_SITE		= "site";
	private static String ELEMENT_CONFIG	= "config";
	private static String ELEMENT_ALIAS		= "alias";
	private static String ELEMENT_LANGUAGE	= "language";
	
 	private static String ELEMENT_ID		= "id";
 	private static String ELEMENT_NAME		= "name";
 	private static String ELEMENT_PREFIX	= "prefix";
 	private static String ELEMENT_SUFFIX	= "suffix";
 	private static String ELEMENT_ENCODING	= "encoding";
	
	
	
	private static Logger logger = LoggerFactory.getLogger(SiteLoader.class);
	
	@Override
	public SiteLoader load(Object parameters) {
		if (parameters instanceof Document) {
			return load((Document)parameters);
		} else if (parameters instanceof File) {
			return load((File)parameters);
		} else {
			return load(parameters.toString());
		}
		//return null;
	}

	public SiteLoader load(String filename) {
		return load(FileSystem.newFile(filename));
	}
	
	
	public SiteLoader load(File file) {
		SiteLoader result = null;
		try {
			Document xml = XMLManager.getDocument(file);
			result = load(xml);
		} catch (Exception e) {
			logger.error("Cann't load: " + file.getAbsolutePath());
		}
		return result;
	}
	
	public SiteLoader load(Document xml) {
		try {
			DecorXPath xpath = new DecorXPath();
			
			NodeSet sites = new NodeSet((NodeList)xpath.query(ELEMENT_SITE, xml.getDocumentElement(), XPathConstants.NODESET));
			NodeSet langs = new NodeSet((NodeList)xpath.query("languages/language", xml.getDocumentElement(), XPathConstants.NODESET));

			/*
			 * Make languages
			 */
			Map<String, LanguageInterface> l = new LinkedHashMap<String, LanguageInterface>();
			
			for (Node lang : langs) {
				String id = null;
				String name = null;
				String prefix = null;
				String suffix = null;
				String encoding = "utf-8";
				NodeSet nodes = new NodeSet((NodeList)xpath.query("*", lang, XPathConstants.NODESET));
				for (Node node: nodes) {
					if (ELEMENT_ID.equals(node.getNodeName())) {
						id = node.getTextContent();
					} else if (ELEMENT_NAME.equals(node.getNodeName())) {
						name = node.getTextContent();
					} else if (ELEMENT_PREFIX.equals(node.getNodeName())) {
						prefix = node.getTextContent();
					} else if (ELEMENT_SUFFIX.equals(node.getNodeName())) {
						suffix = node.getTextContent();
					} else if (ELEMENT_ENCODING.equals(node.getNodeName())) {
						encoding = node.getTextContent();
					}
				}
				l.put(id, new GeneralLanguage(id, name, prefix, suffix, encoding));
			}
			
			/*
			 * Make sites
			*/
			for (Node _site : sites) {
				Element site = (Element)_site;
				GeneralSite s = new GeneralSite(site.hasAttribute("name")?site.getAttribute("name"):"unknown");
				s.setLanguagePolitic(site.hasAttribute("language")?GeneralSite.Politics.getPolitics(site.getAttribute("language")):GeneralSite.Politics.LANGUAGE_NONE);
				NodeSet nodes = new NodeSet((NodeList)xpath.query("*", site, XPathConstants.NODESET));
				for (Node node: nodes) {
					if (ELEMENT_ALIAS.equals(node.getNodeName())) {
						s.addAlias(new GeneralSiteAlias(node.getTextContent()));
					} else if (ELEMENT_CONFIG.equals(node.getNodeName())) {
						s.setConfig(node.getTextContent());
					} else if (ELEMENT_LANGUAGE.equals(node.getNodeName())) {
						Element elm = (Element)node;
						s.addLanguage(l.get(elm.getAttribute("ref")), elm.hasAttribute("default") && elm.getAttribute("default").equals("true"));  
					}
				}
				if (this.sites == null) {
					this.sites = new ArrayList<GeneralSite>();
				}
				this.sites.add(s);
			}
			
		} catch (XMLException e) {
			logger.error(e.getMessage(), e);
		}
		// TODO Auto-generated method stub
		return this;
	}

}
