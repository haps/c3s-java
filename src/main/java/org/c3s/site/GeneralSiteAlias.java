package org.c3s.site;

public class GeneralSiteAlias {

	private String alias;
	private String workPattern;
	
	public GeneralSiteAlias(String alias, String workPattern) {
		this.alias = alias.toLowerCase().trim();
		this.workPattern = workPattern.trim();
	}

	public GeneralSiteAlias(String alias) {
		String[] parts = alias.split("/", 2);
		this.alias = parts[0].toLowerCase().trim();
		this.workPattern = (parts.length == 2)?parts[1].trim():"";
	}

	public String getAlias() {
		return alias;
	}

	public String getWorkPattern() {
		String pattern = (workPattern.endsWith("/")?workPattern.substring(0, workPattern.length()-1):workPattern);
		if (pattern.length() > 0 && !pattern.startsWith("/")) {
			pattern = "/" + pattern;
		}
		return pattern;
	}
	
}
