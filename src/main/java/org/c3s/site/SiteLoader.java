package org.c3s.site;

import java.util.ArrayList;
import java.util.List;

import org.c3s.storage.StorageFactory;
import org.c3s.storage.StorageInterface;
import org.c3s.storage.StorageType;

public abstract class SiteLoader {

	private static final String SITE_SAVE_NAME = "REQUEST_STORED_SITE";
	private static final String SITE_LOADER_SAVE_NAME = "REQUEST_STORED_SITE_LOADER";
	
	//private static SiteLoader current = null;
	private static SiteLoader instance = null;
	
	/**
	 * @param clazz
	 * @return
	 */
	public synchronized static SiteLoader getInstance(Class<? extends SiteLoader> clazz) {
		if (instance == null) {
			synchronized (SiteLoader.class) {
				try {
					instance = clazz.newInstance();
				} catch (InstantiationException e) {
					;
				} catch (IllegalAccessException e) {
					;
				}
			}
		}
		return instance;
	}
	
	/**
	 * 
	 */
	public SiteLoader() {
	}

	
	public void registerAtStorage(StorageInterface storage) {
		if (storage != null) {
			storage.put(SITE_LOADER_SAVE_NAME, this);
		}
	}
	/**
	 * @return
	 */
	public static SiteLoader get1() {
		return (SiteLoader)StorageFactory.getStorage(StorageType.REQUEST).get(SITE_LOADER_SAVE_NAME);
	}
	
	abstract public SiteLoader load(Object parameters);
	
	protected List<GeneralSite> sites;
	
	public List<GeneralSite> getSites() {
		return sites;
	}

	public void addSite(GeneralSite site) {
		if (sites == null) {
			sites = new ArrayList<GeneralSite>();
		}
		sites.add(site);
	}
	
	public static GeneralSite getSite() {
		StorageInterface storage = StorageFactory.getStorage(StorageType.REQUEST);
		return storage == null? null: (GeneralSite)storage.get(SITE_SAVE_NAME);
	}
	
	public GeneralSite getSite(String host) throws CloneNotSupportedException {
		GeneralSite result = null; 
		if (sites != null) {
			for (GeneralSite site : sites) {
				if (site.isMe(host)) {
					//this.site = site;
					GeneralSite clonedsite;
					synchronized (site) {
						clonedsite = (GeneralSite)site.clone();
						/*
						try {
							//
							ByteArrayOutputStream baos = new ByteArrayOutputStream();
					        ObjectOutputStream ous = new ObjectOutputStream(baos);
					        ous.writeObject(site);
					        ous.close();
					        ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
					        ObjectInputStream ois = new ObjectInputStream(bais);
					        clonedsite = (GeneralSite) ois.readObject();
					        ois.close();
						} catch (IOException e) {
							throw new RuntimeException(e);
						} catch (ClassNotFoundException e) {
							throw new RuntimeException(e);
						}
						*/
				        
					}
					result = clonedsite;
					StorageInterface storage = StorageFactory.getStorage(StorageType.REQUEST); 
					if (storage != null) {
						storage.put(SITE_SAVE_NAME, clonedsite);
					}
					break;
				}
			}
		}
		return result;
	}
	
}
