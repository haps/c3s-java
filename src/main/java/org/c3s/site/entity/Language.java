package org.c3s.site.entity;

import java.io.Serializable;

import javax.persistence.*;

import com.google.gson.annotations.Expose;

import java.util.List;


/**
 * The persistent class for the languages database table.
 * 
 */
@Entity
@Table(name="languages")
@NamedQueries({
	@NamedQuery(name="Language.findAll", query="SELECT l FROM Language l"),
	@NamedQuery(name="Language.findAllSorted", query="SELECT l FROM Language l ORDER BY l.id")
})
public class Language implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="language_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int languageId;

	private String encoding;

	private String id;

	private String name;

	private String prefix;

	private String suffix;

	//bi-directional many-to-one association to SiteLanguage
	@Expose(serialize = false, deserialize = false)
	@OneToMany(mappedBy="language")
	private List<SiteLanguage> siteLanguages;

	
	@Transient
	private boolean used = false;
	
	public void setUsed(boolean used) {
		this.used = used;
	}
	
	public Language() {
	}

	public int getLanguageId() {
		return this.languageId;
	}

	public void setLanguageId(int languageId) {
		this.languageId = languageId;
	}

	public String getEncoding() {
		return this.encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPrefix() {
		return this.prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getSuffix() {
		return this.suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public List<SiteLanguage> getSiteLanguages() {
		return this.siteLanguages;
	}

	public void setSiteLanguages(List<SiteLanguage> siteLanguages) {
		this.siteLanguages = siteLanguages;
	}

	public SiteLanguage addSiteLanguage(SiteLanguage siteLanguage) {
		getSiteLanguages().add(siteLanguage);
		siteLanguage.setLanguage(this);
		return siteLanguage;
	}

	public SiteLanguage removeSiteLanguage(SiteLanguage siteLanguage) {
		getSiteLanguages().remove(siteLanguage);
		siteLanguage.setLanguage(null);

		return siteLanguage;
	}

}