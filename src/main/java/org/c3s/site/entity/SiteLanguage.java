package org.c3s.site.entity;

import java.io.Serializable;

import javax.persistence.*;

import com.google.gson.annotations.Expose;


/**
 * The persistent class for the site_languages database table.
 * 
 */
@Entity
@Table(name="site_languages")
@NamedQuery(name="SiteLanguage.findAll", query="SELECT s FROM SiteLanguage s")
public class SiteLanguage implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="site_language_id")
	private int siteLanguageId;

	@Column(name="is_default")
	private byte isDefault;

	//bi-directional many-to-one association to Language
	@ManyToOne
	@JoinColumn(name="languages_id")
	private Language language;

	//bi-directional many-to-one association to Site
	@ManyToOne
	@JoinColumn(name="site_id")
	@Expose(serialize = false, deserialize = false)
	private Site site;

	public SiteLanguage() {
	}

	public int getSiteLanguageId() {
		return this.siteLanguageId;
	}

	public void setSiteLanguageId(int siteLanguageId) {
		this.siteLanguageId = siteLanguageId;
	}

	public byte getIsDefault() {
		return this.isDefault;
	}

	public void setIsDefault(byte isDefault) {
		this.isDefault = isDefault;
	}

	public Language getLanguage() {
		return this.language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public Site getSite() {
		return this.site;
	}

	public void setSite(Site site) {
		this.site = site;
	}

}