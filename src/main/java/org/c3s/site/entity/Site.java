package org.c3s.site.entity;

import java.io.Serializable;

import javax.persistence.*;

import com.google.gson.annotations.Expose;

import java.util.List;


/**
 * The persistent class for the sites database table.
 * 
 */
@Entity
@Table(name="sites")
@NamedQuery(name="Site.findAll", query="SELECT s FROM Site s")
public class Site implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="site_id")
	private int siteId;

	private String config;

	private String name;

	private String politics;

	//bi-directional many-to-one association to SiteAlias
	@OneToMany(mappedBy="site")
	private List<SiteAlias> siteAliases;

	//bi-directional many-to-one association to SiteLanguage
	@OneToMany(mappedBy="site")
	private List<SiteLanguage> siteLanguages;

	//bi-directional many-to-one association to SiteGroup
	@ManyToOne
	@JoinColumn(name="site_group_id")
	@Expose(serialize = false, deserialize = false)
	private SiteGroup siteGroup;

	public Site() {
	}

	public int getSiteId() {
		return this.siteId;
	}

	public void setSiteId(int siteId) {
		this.siteId = siteId;
	}

	public String getConfig() {
		return this.config;
	}

	public void setConfig(String config) {
		this.config = config;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPolitics() {
		return this.politics;
	}

	public void setPolitics(String politics) {
		this.politics = politics;
	}

	public List<SiteAlias> getSiteAliases() {
		return this.siteAliases;
	}

	public void setSiteAliases(List<SiteAlias> siteAliases) {
		this.siteAliases = siteAliases;
	}

	public SiteAlias addSiteAlias(SiteAlias siteAlias) {
		getSiteAliases().add(siteAlias);
		siteAlias.setSite(this);

		return siteAlias;
	}

	public SiteAlias removeSiteAlias(SiteAlias siteAlias) {
		getSiteAliases().remove(siteAlias);
		siteAlias.setSite(null);

		return siteAlias;
	}

	public List<SiteLanguage> getSiteLanguages() {
		return this.siteLanguages;
	}

	public void setSiteLanguages(List<SiteLanguage> siteLanguages) {
		this.siteLanguages = siteLanguages;
	}

	public SiteLanguage addSiteLanguage(SiteLanguage siteLanguage) {
		getSiteLanguages().add(siteLanguage);
		siteLanguage.setSite(this);

		return siteLanguage;
	}

	public SiteLanguage removeSiteLanguage(SiteLanguage siteLanguage) {
		getSiteLanguages().remove(siteLanguage);
		siteLanguage.setSite(null);

		return siteLanguage;
	}

	public SiteGroup getSiteGroup() {
		return this.siteGroup;
	}

	public void setSiteGroup(SiteGroup siteGroup) {
		this.siteGroup = siteGroup;
	}

}