package org.c3s.site.entity;

import java.io.Serializable;

import javax.persistence.*;

import com.google.gson.annotations.Expose;


/**
 * The persistent class for the site_aliases database table.
 * 
 */
@Entity
@Table(name="site_aliases")
@NamedQuery(name="SiteAlias.findAll", query="SELECT s FROM SiteAlias s")
public class SiteAlias implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="site_alias_id")
	private int siteAliasId;

	private String host;

	private String pattern;

	//bi-directional many-to-one association to Site
	@ManyToOne
	@JoinColumn(name="site_id")
	@Expose(serialize = false, deserialize = false)
	private Site site;

	public SiteAlias() {
	}

	public int getSiteAliasId() {
		return this.siteAliasId;
	}

	public void setSiteAliasId(int siteAliasId) {
		this.siteAliasId = siteAliasId;
	}

	public String getHost() {
		return this.host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPattern() {
		return this.pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public Site getSite() {
		return this.site;
	}

	public void setSite(Site site) {
		this.site = site;
	}

}