package org.c3s.site.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the site_groups database table.
 * 
 */
@Entity
@Table(name="site_groups")
@NamedQuery(name="SiteGroup.findAll", query="SELECT s FROM SiteGroup s")
public class SiteGroup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="site_group_id")
	private int siteGroupId;

	private String name;

	//bi-directional many-to-one association to Site
	@OneToMany(mappedBy="siteGroup")
	private List<Site> sites;

	public SiteGroup() {
	}

	public int getSiteGroupId() {
		return this.siteGroupId;
	}

	public void setSiteGroupId(int siteGroupId) {
		this.siteGroupId = siteGroupId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Site> getSites() {
		return this.sites;
	}

	public void setSites(List<Site> sites) {
		this.sites = sites;
	}

	public Site addSite(Site site) {
		getSites().add(site);
		site.setSiteGroup(this);

		return site;
	}

	public Site removeSite(Site site) {
		getSites().remove(site);
		site.setSiteGroup(null);

		return site;
	}

}