package org.c3s.utils;

import java.net.URI;
import java.net.URISyntaxException;

public class PathUtils {

	public static String realurl(String url) throws URISyntaxException {
		URI urls = new URI(url);
		return urls.getPath(); 
	}
	
}
