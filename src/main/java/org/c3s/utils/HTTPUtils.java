package org.c3s.utils;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

public class HTTPUtils {

	public static boolean isAjax(ServletRequest request) {
		
		boolean isAjax = false;

		/*
		for (Enumeration<String> key = ((HttpServletRequest)request).getHeaderNames(); key.hasMoreElements();) {
			String name = key.nextElement();
			for (Enumeration<String> val = ((HttpServletRequest)request).getHeaders(name); val.hasMoreElements();) {
				logger.debug("Header-value >>> {}: {}", name, val.nextElement());
			}
		}
		*/
		
		
		String header = ((HttpServletRequest)request).getHeader("x-requested-with");
		
		isAjax =  "XMLHttpRequest".equals(header); 
		
		//logger.debug("Is Ajax: {}", isAjax);
		
		return isAjax;
	}

	
}
