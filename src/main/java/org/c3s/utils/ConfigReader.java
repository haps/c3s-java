/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.c3s.utils;

/**
 *
 * @author admin
 */

import java.io.File;

import org.c3s.xml.utils.XMLUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ConfigReader {
	/*
	 * Variables
	 */
	protected Document _document = null;

	public Document getDocument() {
		return this._document;
	}
	/*
	 * Constructors
	 */



	protected ConfigReader(File file, boolean includes) throws Exception {
		String path = System.getProperties().getProperty("user.dir");
		this._document = XMLUtils.load(file);
		System.getProperties().setProperty("user.dir", FileSystem.getParentDirectory(file));
		if (includes) {
			this.processIncludes();
		}
		System.getProperties().setProperty("user.dir", path);
	}

	/*
	 * Static loaders
	 */

	public static ConfigReader load(File file, boolean includes) throws Exception {
		return (new ConfigReader(file, includes));
	}

	public static ConfigReader load(String filename, boolean includes) throws Exception {
		return load(FileSystem.newFile(filename), includes);
	}

	public static ConfigReader load(String filename) throws Exception {
		return load(FileSystem.newFile(filename), false);
	}


	public static ConfigReader load(File file) throws Exception {
		return load(file, false);
	}

	/*
	 * Methods
	 */

	protected void processIncludes() throws Exception {
		NodeList includes = this._document.getElementsByTagName("include");
		if (includes.getLength() != 0) {
			this.applyIncludes(includes.item(0));
			this.processIncludes();
		}
	}

	protected void applyIncludes(Node node) throws Exception {
		String filename = ((Element) node).getAttribute("href");
		Node parent = node.getParentNode();
		Document inc_xml = XMLUtils.load(filename);
		NodeList nodes = inc_xml.getDocumentElement().getChildNodes();
		for (int i=0; i < nodes.getLength(); i++) {
			parent.insertBefore(parent.getOwnerDocument().importNode(nodes.item(i), true), node);
		}
		parent.removeChild(node);
	}

}
