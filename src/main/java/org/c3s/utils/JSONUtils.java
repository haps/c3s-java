package org.c3s.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.google.gson.Gson;

public class JSONUtils {

	private static Map<String, Map<Class<?>, Object>> cache = new ConcurrentHashMap<String, Map<Class<?>, Object>>();
	private static Gson gson = new Gson();
	
	@SuppressWarnings("unchecked")
	public static <T> T fromJSON(String source, Class<T> clazz) {
		Map<Class<?>, Object> info = cache.get(source);
		if (info == null) {
			info = new HashMap<Class<?>, Object>();
			cache.put(source, info);
		}
		
		Object object = info.get(clazz);
		if (object == null) {
			object = gson.fromJson(source, clazz);
			info.put(clazz, object);
		}
		return (T)object;
	}
	
	public static String toJson(Object obj) {
		return gson.toJson(obj);
	}
}
