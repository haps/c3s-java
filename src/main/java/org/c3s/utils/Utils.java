/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.c3s.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.c3s.db.beans.Bean;

/**
 *
 * @author admin
 */
public class Utils {
	public static String MD5(String source) {
		StringBuffer md5 = new StringBuffer();

		try {
			MessageDigest digest = MessageDigest.getInstance("MD5");
			byte[] buff = digest.digest(source.getBytes());
			for (int i = 0; i < buff.length; i++) {
          md5.append(Integer.toHexString(0x0100 + (buff[i] & 0x00FF)).substring(1));
      }

		} catch (Exception e) {
			e.printStackTrace();
		}

		return md5.toString();
	}

	public static String generateName(String name, boolean isMethod) {
		String ret_name = "";
		boolean flag = !isMethod;
		for(int i=0; i < name.length(); i++) {
			if(name.charAt(i) == '_') {
				flag = true;
			} else {
				Character chr = name.charAt(i);
				if (flag) {
					ret_name += Character.toUpperCase(chr);
				} else {
					ret_name += Character.toLowerCase(chr);
				}
				flag = false;
			}
		}
		return ret_name;
	}

	public static String generateName(String name) {
		return generateName(name, false);
	}

	
	public static String replaceRecursive(String source, String find, String replace) {
		String str = source;
		if (replace.indexOf(find) == -1) {
			while (str.indexOf(find) != -1) {
				str = str.replace(find, replace);
			}
		}
		return str;
	}
	
	
	public static String generateString(int length, String chars) {
		Random rand = new Random(new Date().getTime());
		String ret = "";
		if (chars != null && chars.length() > 0) {
			int ch_len = chars.length();
			for (int i=0; i < length; i++) {
				char ch = chars.charAt(rand.nextInt(ch_len));
				ret += ch;
			}
		}
		return ret;
	}
	
	public static String readInputStream(InputStream io) throws IOException {
		StringBuilder sb = new StringBuilder();
		
		BufferedReader read = new BufferedReader(new InputStreamReader(io, "utf-8"));
		
		String line = "";
		while ((line = read.readLine()) != null) {
			sb.append(line);
			sb.append("\n");
		}
		return sb.toString();
	}
	
	public static String substr(String source, int beginIndex, int endIndex, String append) {
		boolean need = true;
		if (endIndex > source.length()) {
			endIndex = source.length();
			need = false;
		}
		return source.substring(beginIndex, endIndex) + (need?append:"");
	}
	
	public static Object getMapValue(Map<String, Object> map, String what) {
		Object result = null;

		if (what.indexOf('[') != 0) {
			if (!what.startsWith("[")) {
				what = what.substring(what.indexOf('['));
			}
		}
		List<List<String>> patterns = new ArrayList<List<String>>();
		
		if (RegexpUtils.preg_match_all("~\\[([^\\]]+)\\]~is", what, patterns)) {
			
		}
		
		
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public static String mapToString(Object map, String tabs) {
		StringBuffer sb = new StringBuffer();
		if (map instanceof Map<?, ?>) {
			//sb.append(tabs);
			sb.append("Map:\n");
			Map<String, Object> smap = (Map<String, Object>)map;
			for(String key : smap.keySet()) {
				sb.append(tabs);
				sb.append("\t");
				sb.append("{" + key + "} -> ");
				sb.append(mapToString(smap.get(key), tabs + "\t"));
				//sb.append("\n");
			}
		} else {
			sb.append((map != null)?map.toString():"null");
			sb.append("\n");
		}
		return sb.toString();
	}
	
	public static boolean nullOrEmpty(String source) {
		return source == null || source.isEmpty();
	}
	/**
	 * 
	 * @param params
	 * @return
	 */
	public static Map<String, Object> paramsArrayToMap(String[] params) {
		Map<String, Object> map = null;
		if (params != null && params.length > 0) {
			map = new HashMap<String, Object>();
			for(String param: params) {
				String[] parts = param.split(":", 2);
				map.put(parts[0], parts.length == 2?parts[1]:"");
			}
		}
		return map;
	}
	
	
	public static <T> Map<Object, List<T>> getArrayGrouped(List<T> items, String index) {
		Map<Object, List<T>> result = null; 
		
		try {
			if (items != null) {
				result = new LinkedHashMap<Object, List<T>>();
				if (items.size() > 0) {
					Class<?> clazz = items.get(0).getClass();
					Field field = clazz.getDeclaredField(index);
					field.setAccessible(true);
					for(T item: items) {
						List<T> list = result.get(field.get(item));
						if (list == null) {
							list = new ArrayList<T>();
							result.put(field.get(item), list);
						}
						//result.put(field.get(item), item);
						list.add(item);
					}
				}
			}
		} catch (NoSuchFieldException e) {
		} catch (SecurityException e) {
		} catch (IllegalArgumentException e) {
		} catch (IllegalAccessException e) {
		}
		return result;
	}
}
