/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.c3s.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import javax.servlet.ServletContext;

import org.c3s.exceptions.FileSystemException;
/**
 *
 * @author admin
 */
public class FileSystem {
	public static String getParentDirectory(File file) {
		String ret_path = "";

		String path = file.getAbsolutePath();
		path = Utils.replaceRecursive(path, "\\", "/");

		String[] path_arr = path.split("/");

		for(int i=0; i < path_arr.length-1; i++) {
			ret_path += path_arr[i] + "/";
		}

		return ret_path;
	}
	
	
	public static String fileGetContents(String filename) throws FileSystemException {
		return fileGetContents(filename, "utf-8");
	}
	
	public static String fileGetContents(String filename, String encoding) throws FileSystemException {
		File file = FileSystem.newFile(filename);
		return fileGetContents(file, encoding);
	}
	
	public static String fileGetContents(File file, String encoding) throws FileSystemException {
		String out = null;
		Long size = file.length();
		try {
			InputStreamReader reader = new InputStreamReader(new FileInputStream(file), encoding);
			char[] cbuf = new char[size.intValue()];
			//System.out.println(">>>>>>>>>>>>FileSize: " + size.intValue());
			int realsize = reader.read(cbuf);
			//System.out.println(">>>>>>>>>>>>ReadSize: " + realsize);
			char[] outbuf =  new char[realsize];
			System.arraycopy(cbuf, 0, outbuf, 0, realsize);
			out = new String(outbuf);
			reader.close();
		} catch (Exception e) {
			throw new FileSystemException(e);
		}
		return out;
	}
	
	public static String fileGetContents(File file) throws FileSystemException {
		return fileGetContents(file, "utf-8");
	}
	
	/**
	 * 
	 */
	public static void filePutContents(File file, String content, String encoding) throws FileSystemException {
		try {
			OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(file), encoding);
			writer.write(content);
			writer.close();
		} catch (Exception e) {
			throw new FileSystemException(e);
		}
	}
	
	
	private static ServletContext __servletContext = null;
	public static void setServletContext(ServletContext servletContext) {
		__servletContext = servletContext;
	}
	/**
	 * 
	 * @param path
	 * @return
	 */
	public static File newFile(String path) {
		File file = new File((path.indexOf('/') != 0 && __servletContext != null)?__servletContext.getRealPath(path):path);
		return file;
	}
}
