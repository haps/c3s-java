/**
 * 
 */
package org.c3s.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.c3s.query.RequestType;
import org.c3s.storage.StorageType;

/**
 * @author user6182
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface StoredObject {
	StorageType storage() default StorageType.SESSION;
	String name()  default "";
	RequestType request() default RequestType.NONE; 
	boolean remove()  default true;
}
