package org.c3s.data.cast;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class GeneralCastType implements CastType {

	private Map<Class<?>, Primitive> primitives = new HashMap<Class<?>, GeneralCastType.Primitive>() {/**
		 * 
		 */
		private static final long serialVersionUID = 3084224752418321959L;

	{
		put(boolean.class, new Primitive(Boolean.class, "booleanValue"));
		put(byte.class, new Primitive(Byte.class, "byteValue"));
		put(short.class, new Primitive(Short.class, "shortValue"));
		put(int.class, new Primitive(Integer.class, "intValue"));
		put(long.class, new Primitive(Long.class, "longValue"));
		put(float.class, new Primitive(Float.class, "floatValue"));
		put(double.class, new Primitive(Double.class, "doubleValue"));
		put(char.class, new Primitive(Character.class, "charValue"));
	}};
	/* (non-Javadoc)
	 * @see org.c3s.data.cast.CastType#cast(java.lang.Class, java.lang.Class, java.lang.Object)
	 */
	@Override
	public final Object cast(Class<?> targetClass, Class<?> sourceClass, Object value) {
		
		if (value != null) {
			Class<?> clazz = this.getClass();
			String fromClass = sourceClass.getName().substring(sourceClass.getName().lastIndexOf('.') + 1);
			String toClass = targetClass.getName().substring(targetClass.getName().lastIndexOf('.') + 1);
			String methodName = "cast" + fromClass + "To" + toClass;
			try {
				Method method = clazz.getMethod(methodName, Object.class);
				method.setAccessible(true);
				value = method.invoke(this, value);
			} catch (NoSuchMethodException e) {
				if (!sourceClass.isAssignableFrom(targetClass)) {
					try {
						Primitive primitive = primitives.get(targetClass);
						if (primitive == null) {
							throw new RuntimeException(e);
						} else if (primitive.getWrapper() == sourceClass) {
							value = primitive.getValue(value);
						} else {
							value = cast(primitive.getWrapper(), sourceClass, value);
							value = cast(targetClass, primitive.getWrapper(), value); 
						}
					} catch (RuntimeException e1) {
						throw e1;
					} catch (Exception e1) {
						throw new RuntimeException(e1);
					}
				}
			} catch (RuntimeException e) {
				throw e;
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		return value;
	}
	
	
	private static class Primitive {
		
		private Class<?> wrapper; 
		String method;
		public Primitive(Class<?> wrapper, String method) {
			super();
			this.wrapper = wrapper;
			this.method = method;
		}
		
		public Object getValue(Object value) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
			Method method = null;
			method = wrapper.getMethod(this.method);
			return method.invoke(value);
		}
		
		public Class<?> getWrapper() {
			return this.wrapper;
		}
	}
}
