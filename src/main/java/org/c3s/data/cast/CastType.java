package org.c3s.data.cast;

public interface CastType {
	
	public Object cast(Class<?> targetClass, Class<?> sourceClass, Object value);
	
}
