/**
 * 
 */
package org.c3s.data.cast;

/**
 * @author azajac
 *
 */
public class SimpleCastType implements CastType {

	/* (non-Javadoc)
	 * @see org.c3s.data.cast.CastType#cast(java.lang.Class, java.lang.Class, java.lang.Object)
	 */
	@Override
	public Object cast(Class<?> targetClass, Class<?> sourceClass, Object value) {
		Object result = null;
		if (Long.class.equals(targetClass) && Integer.class.equals(sourceClass)) {
			result = new Long((Integer)value);
			
		} else if (Integer.class.equals(targetClass) && Long.class.equals(sourceClass)) {
			result = new Integer(((Long)value).intValue());
			
		} else	if (Long.class.equals(targetClass) && Boolean.class.equals(sourceClass)) {
			result = new Long(((Boolean)value)?1:0);
			
		} else if (Boolean.class.equals(targetClass) && Long.class.equals(sourceClass)) {
			result = new Boolean(((Long)value) != 0);
			
		} else	if (Integer.class.equals(targetClass) && Boolean.class.equals(sourceClass)) {
			result = new Integer(((Boolean)value)?1:0);
			
		} else if (Boolean.class.equals(targetClass) && Integer.class.equals(sourceClass)) {
			result = new Boolean(((Integer)value) != 0);
			
		} else {
			result = value;
		}
		return result;
	}

}
