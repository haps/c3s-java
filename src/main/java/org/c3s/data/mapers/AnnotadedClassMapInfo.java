package org.c3s.data.mapers;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.c3s.data.annotations.DataSource;
import org.c3s.data.annotations.DataTarget;

public class AnnotadedClassMapInfo extends GeneralClassMapInfo {
	
	public void prepareClassInfo(Class<?> clazz) {
		for (Field field: clazz.getDeclaredFields()) {
			DataSource sources = field.getAnnotation(DataSource.class);
			DataTarget target = field.getAnnotation(DataTarget.class);
			
			if (sources != null) {
				List<String> srclist = new ArrayList<String>();
				String[] srcs = sources.value();
				for (String src: srcs) {
					src = src.toLowerCase().trim();
					sourceFields.put(src, field);
					srclist.add(src);
				}
				fieldsSources.put(field, srclist);
			}
			
			if (target != null) {
				fieldTargets.put(field, target.value().toLowerCase().trim());
			}
			findGetterSetter(field);
		}
	}

}
