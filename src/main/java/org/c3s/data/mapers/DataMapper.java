package org.c3s.data.mapers;

import java.util.Map;

public interface DataMapper {

	/**
	 * @param source
	 * @param target
	 * @return
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	public abstract <T> T map(Object source, Class<T> target)
			throws IllegalArgumentException, IllegalAccessException,
			InstantiationException;

	/**
	 * @param source
	 * @param target
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public abstract void map(Object source, Object target)
			throws IllegalArgumentException, IllegalAccessException;

	/**
	 * @param source
	 * @return
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public abstract Map<String, Object> mapToRow(Object source)
			throws IllegalArgumentException, IllegalAccessException;

	/**
	 * @param row
	 * @param target
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public abstract void mapFromRow(Map<String, Object> row, Object target)
			throws IllegalArgumentException, IllegalAccessException;

	/**
	 * @param row
	 * @param target
	 * @return
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	public abstract <T> T mapFromRow(Map<String, Object> row, Class<T> target)
			throws IllegalArgumentException, IllegalAccessException,
			InstantiationException;

}