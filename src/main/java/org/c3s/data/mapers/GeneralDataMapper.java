package org.c3s.data.mapers;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.c3s.data.cast.CastType;
import org.c3s.data.cast.EmptyCastType;

public class GeneralDataMapper implements DataMapper {
	
	private CastType castType = new EmptyCastType();
	
	private Class<? extends MappedClassInfo> classInfo = AnnotadedClassMapInfo.class; 
	
	/**
	public void setCast(CastType caster) {
		castType = caster;
	}
	*/
	public GeneralDataMapper(CastType caster) {
		this(caster, null);
	}

	public GeneralDataMapper(Class<? extends MappedClassInfo> clazz) {
		this(null, clazz);
	}
	
	public GeneralDataMapper(CastType caster, Class<? extends MappedClassInfo> clazz) {
		castType = caster;
		this.classInfo = clazz;
	}
	
	
	public GeneralDataMapper() {
		this(null, null);
	}
	
	private static Map<Class<?>, MappedClassInfo> classMap = new ConcurrentHashMap<Class<?>, MappedClassInfo>();  

	/* (non-Javadoc)
	 * @see org.c3s.data.mapers.DataMapper#map(java.lang.Object, java.lang.Class)
	 */
	@Override
	public <T> T map(Object source, Class<T> target) throws IllegalArgumentException, IllegalAccessException, InstantiationException {
		T object = target.newInstance();
		map(source, object);
		return object;
	}
	
	/* (non-Javadoc)
	 * @see org.c3s.data.mapers.DataMapper#map(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void map(Object source, Object target) throws IllegalArgumentException, IllegalAccessException {
		MappedClassInfo sclass =  getClassInfo(source.getClass());
		MappedClassInfo tclass =  getClassInfo(target.getClass());

		for (Field sfield: sclass.getFieldTargets().keySet()) {
			String totarget = sclass.getFieldTargets().get(sfield);
			Field tfield = tclass.getSourceFields().get(totarget);
			if (tfield != null) {
				Object value =  null;
				if (sclass.getters().containsKey(sfield)) {
					Method method = sclass.getters().get(sfield);
					try {
						value = method.invoke(source);
					} catch (InvocationTargetException e) {
						throw new RuntimeException(e.getMessage(), e);
					}
				} else {
					boolean as = sfield.isAccessible();
					sfield.setAccessible(true);
					value = sfield.get(source);
					sfield.setAccessible(as);
				}
				
				value = castType.cast(tfield.getType(), sfield.getType(), value);
				
				if (tclass.setters().containsKey(tfield)) {
					Method method = tclass.setters().get(tfield);
					try {
						method.invoke(target, value);
					} catch (InvocationTargetException e) {
						throw new RuntimeException(e.getMessage(), e);
					}
				} else {
					boolean at = tfield.isAccessible();
					tfield.setAccessible(true);
					tfield.set(target, value);
					tfield.setAccessible(at);
				}
				
				/*
				//synchronized (sfield) {
					boolean as = sfield.isAccessible();
					sfield.setAccessible(true);
					value = sfield.get(source);
					//sfield.setAccessible(as);
				//}
				//synchronized (tfield) {
					boolean at = tfield.isAccessible();
					tfield.setAccessible(true);
					tfield.set(target, castType.cast(tfield.getType(), sfield.getType(), value));
					//tfield.setAccessible(at);
				//}
				 * 
				 */
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see org.c3s.data.mapers.DataMapper#mapToRow(java.lang.Object)
	 */
	@Override
	public Map<String, Object> mapToRow(Object source) throws IllegalArgumentException, IllegalAccessException {
		Map<String, Object> result = null;
		
		MappedClassInfo sclass =  getClassInfo(source.getClass());
		if (!sclass.getFieldTargets().isEmpty()) {
			result = new HashMap<String, Object>();
			for (Field sfield: sclass.getFieldTargets().keySet()) {
				String totarget = sclass.getFieldTargets().get(sfield);
				boolean as = sfield.isAccessible();
				sfield.setAccessible(true);
				result.put(totarget, sfield.get(source));
				sfield.setAccessible(as);
			}
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see org.c3s.data.mapers.DataMapper#mapFromRow(java.util.Map, java.lang.Object)
	 */
	@Override
	public void mapFromRow(Map<String, Object> row, Object target) throws IllegalArgumentException, IllegalAccessException {
		
		MappedClassInfo tclass =  getClassInfo(target.getClass());

		for (String key: row.keySet()) {
			Field tfield = tclass.getSourceFields().get(key);
			if (tfield == null) {
				tfield = tclass.getSourceFields().get(key.toLowerCase());
			}
			if (tfield != null) {
				
				Object value = row.get(key);
				// setters before
				if (tclass.setters().containsKey(tfield)) {
					Method method = tclass.setters().get(tfield);
					try {
						method.invoke(target, (value == null)?null:castType.cast(tfield.getType(), value.getClass(), value));
					} catch (InvocationTargetException e) {
						throw new RuntimeException(e.getMessage(), e);
					}
				} else {
					boolean at = tfield.isAccessible();
					tfield.setAccessible(true);
					try {
						if (value == null) {
							tfield.set(target, null);
						} else {
							tfield.set(target, castType.cast(tfield.getType(), value.getClass(), value));
						}
					} catch (IllegalAccessException e) {
						throw new IllegalAccessException(e.getMessage() + " field \"" + tfield.getName()  + "\"");
					}
					tfield.setAccessible(at);
				}
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see org.c3s.data.mapers.DataMapper#mapFromRow(java.util.Map, java.lang.Class)
	 */
	@Override
	public <T> T mapFromRow(Map<String, Object> row, Class<T> target) throws IllegalArgumentException, IllegalAccessException, InstantiationException {
		T object = target.newInstance();
		mapFromRow(row, object);
		return object;
	}
	
	/**
	 * @param clazz
	 * @return
	 */
	private MappedClassInfo getClassInfo(Class<?> clazz) {
		MappedClassInfo result = null;
		if (classMap.containsKey(clazz)) {
			result = classMap.get(clazz);
		} else {
			try {
				result = classInfo.newInstance();
				result.prepareClassInfo(clazz);
			} catch (Exception e) {
				result = new AnnotadedClassMapInfo();
				result.prepareClassInfo(clazz);
			} 
			//
			classMap.put(clazz, result);
		}
		return result;
	}
}
