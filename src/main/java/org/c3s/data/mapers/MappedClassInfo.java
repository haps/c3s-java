package org.c3s.data.mapers;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

public interface MappedClassInfo {

	/**
	 * @return the fieldsSources
	 */
	public abstract Map<Field, List<String>> getFieldsSources();

	/**
	 * @return the sourceFields
	 */
	public abstract Map<String, Field> getSourceFields();

	/**
	 * @return the fieldTragets
	 */
	public abstract Map<Field, String> getFieldTargets();


	/**
	 * @param clazz
	 */
	public abstract void prepareClassInfo(Class<?> clazz);
	
	
	/**
	 * @return
	 */
	public abstract Map<Field, Method> getters();
	
	/**
	 * @return
	 */
	public abstract Map<Field, Method> setters();
	
}