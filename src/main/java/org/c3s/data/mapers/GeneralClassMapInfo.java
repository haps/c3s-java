package org.c3s.data.mapers;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GeneralClassMapInfo implements MappedClassInfo {
	
	protected Map<Field, List<String>> fieldsSources = new HashMap<Field, List<String>>();
	protected Map<String, Field> sourceFields = new HashMap<String, Field>();
	protected Map<Field, String> fieldTargets = new HashMap<Field, String>();
	protected Map<Field, Method> _setters = new HashMap<Field, Method>();
	protected Map<Field, Method> _getters = new HashMap<Field, Method>();
	
	
	public GeneralClassMapInfo(Class<?> clazz) {
		prepareClassInfo(clazz);
	}
	
	public GeneralClassMapInfo() {
	}
	
	protected void findGetterSetter(Field field) {
		String name = field.getName();
		name = name.substring(0, 1).toUpperCase() + (name.length() > 1?name.substring(1):"");
		// Try setter
		try {
			Method setter = field.getDeclaringClass().getMethod("set" + name, field.getType());
			if ((setter.getModifiers() & Modifier.PUBLIC) != 0) {
				_setters.put(field, setter);
			}
		} catch (NoSuchMethodException e) {
		} catch (SecurityException e) {
		} 
		// Try getter
		try {
			Method getter = field.getDeclaringClass().getMethod("get" + name);
			if ((getter.getModifiers() & Modifier.PUBLIC) != 0) {
				_getters.put(field, getter);
			}
		} catch (NoSuchMethodException e) {
		} catch (SecurityException e) {
		} 
	}
	
	public void prepareClassInfo(Class<?> clazz) {
		for (Field field: clazz.getDeclaredFields()) {
			List<String> srclist = new ArrayList<String>();
			String src = field.getName();
			sourceFields.put(src, field);
			srclist.add(src);
			fieldsSources.put(field, srclist);
			fieldTargets.put(field, src);
			findGetterSetter(field);
		}
	}


	/* (non-Javadoc)
	 * @see org.c3s.data.mapers.MappedClassInfo#getFieldsSources()
	 */
	@Override
	public Map<Field, List<String>> getFieldsSources() {
		return fieldsSources;
	}


	/* (non-Javadoc)
	 * @see org.c3s.data.mapers.MappedClassInfo#getSourceFields()
	 */
	@Override
	public Map<String, Field> getSourceFields() {
		return sourceFields;
	}


	/* (non-Javadoc)
	 * @see org.c3s.data.mapers.MappedClassInfo#getFieldTargets()
	 */
	@Override
	public Map<Field, String> getFieldTargets() {
		return fieldTargets;
	}

	@Override
	public Map<Field, Method> getters() {
		return _getters;
	}

	@Override
	public Map<Field, Method> setters() {
		return _setters;
	}

	
}
