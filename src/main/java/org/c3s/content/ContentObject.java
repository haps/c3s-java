/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.c3s.content;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.c3s.content.transformers.NotApplicableException;
import org.c3s.content.transformers.Transformer;
import org.c3s.exceptions.FileSystemException;
import org.c3s.exceptions.XMLException;
import org.c3s.storage.StorageFactory;
import org.c3s.storage.StorageType;
import org.c3s.utils.FileSystem;
import org.c3s.utils.Utils;
import org.c3s.xml.DocumentInjector;
import org.c3s.xml.utils.XMLUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
//import javax.xml.transform.Transformer;

/**
 *
 * @author admin
 */
public class ContentObject {
	
	private static Logger logger = LoggerFactory.getLogger(ContentObject.class);

	public static final int CONTENT_LAST = 0;
	public static final int CONTENT_ARRAY = 1;
	public static final int CONTENT_RANDOM = 2;
	
	private static String default_html_id = "HTML";

	public static ContentObject getInstance() {
		
		Throwable t = new Throwable();
		StackTraceElement[] st = t.getStackTrace();
		String className = st[0].getClassName();

		ContentObject instance = (ContentObject) StorageFactory.getStorage(StorageType.REQUEST).get(className);
		
		if (instance == null) {
			instance = new ContentObject();
			StorageFactory.getStorage(StorageType.REQUEST).put(className, instance);
		}
		return instance;
	}
	
	
	Map<String, Object> fixedParameters = new HashMap<String, Object>();
	
	public void setFixedParameters(String key, Object value) {
		fixedParameters.put(key, value);
	}

	public void setFixedParameters(Map<String, Object> value) {
		fixedParameters.putAll(value);
	}
	
	
	public Object getFixedParameter(String key) {
		return fixedParameters.get(key);
	}
	
	String title = "";
	
	
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}


	List<PathElement> path = new ArrayList<PathElement>();
	
	public void addPath(PathElement element) {
		path.add(element);
	}
	public void addPath(String href, String title) {
		addPath(new PathElement(href, title));
	}

	public List<PathElement> getPath() {
		return path;
	}
	/*
	 * worked template
	 */
	private String template;

	/**
	 * @return the template
	 */
	public String getTemplate() {
		return template;
	}

	/**
	 * @param template the template to set
	 */
	public void setTemplate(String template) {
		this.template = template;
	}

	/*
	 * Data
	 */

	private List<DataHolder> _data = new ArrayList<DataHolder>();

	/*
	 * Put data methods
	 */
	public void setData(DataHolder dataHolder) {
		_data.add(dataHolder);
	}
	
	public void setData(String tag, Object data, Object template, Map<String, Object> parameters,
					boolean cacheble ) {
		_data.add(new DataHolder(tag, data, template, parameters, cacheble));
	}

	public void setData(String tag, Object data, Object template, String[] parameters) {
		_data.add(new DataHolder(tag, data, template, parameters, true));
	}

	public void setData(String tag, Object data, Object template, String[] parameters,
			boolean cacheble ) {
		_data.add(new DataHolder(tag, data, template, parameters, cacheble));
	}
	
	public void setData(String tag, Object data, Object template, Map<String, Object> parameters) {
		_data.add(new DataHolder(tag, data, template, parameters, true));
	}
	
	public void setData(String tag, Object data, String template_file_name, Map<String, Object> parameters) {
		_data.add(new DataHolder(tag, data, template_file_name, parameters, true));
	}

	public void setData(String tag, Object data, Object template) {
		_data.add(new DataHolder(tag, data, template, (String[])null, true));
	}

	public void setData(String tag, Object data, String template_file_name) {
		_data.add(new DataHolder(tag, data, template_file_name, (String[])null, true));
	}

	public void setData(String tag, Object data) {
		_data.add(new DataHolder(tag, data, null, (String[])null, true));
	}

	public void setData(String tag, String data) {
		_data.add(new DataHolder(tag, data, null, (String[])null, true));
	}
	
	
	/*
	 * Get data methods
	 */

	public String getData(String tag, String[] parameters, int content_type, String html_id) throws Exception {
		return getData(tag, Utils.paramsArrayToMap(parameters), content_type, html_id);
	}
	
	public String getData(String tag, Map<String, Object> parameters, int content_type, String html_id) throws Exception {

		String _html_id = (html_id == null)?default_html_id:html_id;
		//logger.info("Tag {" + tag + "} HtmlId {" + _html_id + "}");

		List<String> res = new ArrayList<String>();

		for (int i=0; i < _data.size(); i++) {
			if (_data.get(i).tag.equals(tag)) {
				DataHolder data = _data.get(i);
				String str;
				if (data.html.get(_html_id) != null) {
					res.add((String)data.html.get(_html_id));
				} else if (data.data != null) {
					if (data.template == null) {
						/*
						 * Simple copy data to OUT
						 */
						if (data.data instanceof Document) {
							str = XMLUtils.saveXML((Document)data.data);
						} else {
							str = data.data.toString();
						}
					} else {
						/*
						 * Make Transformation
						 */
						if (parameters != null) {
							data.params.putAll(parameters);
						}
						
						/**
						Document xml;
						if (data.data instanceof Document) {
							xml = (Document)data.data;
						} else {
							xml =  XMLUtils.loadXML((String)data.data);
						}
						applyInjectors(xml);
						**/
						/*
						 * 
						 */
						Map<String, Object> transformParameters = new HashMap<String, Object>(fixedParameters);
						transformParameters.putAll(data.params);
						/*
						 * 
						 */
						Transformer transformer = getTransformer(data.template);
						if (transformer != null) {
							str = transformer.transform(data.data, data.template, transformParameters, this); 
						} else {
							throw new NotApplicableException(data.template.toString());
						}

					}

					res.add(str);
					if (data.cacheble) {
						data.html.put(_html_id, str);
					}
				}
			}
		}

		String res_str = "";

		if (content_type == CONTENT_RANDOM) {
			int size = res.size();
			if (size > 0) {
				int index = (int)Math.floor(Math.random()*size);
				res_str = res.get(index);
			}
		} else if (content_type == CONTENT_ARRAY) {
			for (int i=0; i < res.size(); i++) {
				res_str += res.get(i);
			}
		} else {
			int size = res.size();
			if (size > 0) {
				res_str = res.get(size - 1);
			}
		}
		return res_str;
	}

	public String getData(String tag, Map<String, Object> parameters, int content_type) throws Exception {
		return getData(tag, parameters, content_type, null);
	}

	public String getData(String tag, Map<String, Object> parameters) throws Exception {
		return getData(tag, parameters, CONTENT_ARRAY, null);
	}

	public String getData(String tag, String[] parameters, int content_type) throws Exception {
		return getData(tag, parameters, content_type, null);
	}

	public String getData(String tag, String[] parameters) throws Exception {
		return getData(tag, parameters, CONTENT_ARRAY, null);
	}
	
	public String getData(String tag) throws Exception {
		return getData(tag, (String[])null, CONTENT_ARRAY, null);
	}

	public Object getObject(String tag) {
		Object result = null;
		for (DataHolder data: _data) {
			if (tag.equals(data.tag)) {
				result = data.data;
				//break;
			}
		}
		return result;
	}
	/*
	 * Private class for holding data
	 */

	/*
	 * 
	 * Path
	 * 
	 */
	
	List<DocumentInjector> injectors = new ArrayList<DocumentInjector>();
	
	public void addInjector(DocumentInjector injector) {
		injectors.add(injector);
	}

	protected void applyInjectors(Document doc) throws XMLException {
		for (DocumentInjector inj : injectors) {
			inj.inject(doc);
		}
	}
	
	/**
	 * 
	 */
	private static List<Transformer> transformers = new ArrayList<Transformer>();
	
	public static void registerTransformer(Transformer transformer) {
		logger.debug("Add template tranformer: " + transformer.getClass().getName());
		transformers.add(transformer);
	}

	protected Transformer getTransformer(Object template) {
		
		logger.debug("Find template tranformer for class: " + template.getClass().getName());
		
		Transformer transformer = null;
		for (Transformer tr: transformers) {
			logger.debug("Transformer class: " + tr.getClass().getName());
			if (tr.isApplicable(template)) {
				transformer = tr;
				break;
			}
		}
		
		logger.debug("Found template transformer: " + ((transformer != null)?transformer.getClass().getName():"null"));
		
		return transformer;
	}
	
	/**
	 * 
	 */
	private Map<String, List<String>> includes = new HashMap<String, List<String>>(); 
	
	public void setInclude(String tag, String include) {
		if (!includes.containsKey(tag)) {
			includes.put(tag, new ArrayList<String>());
		}
		includes.get(tag).add(include);
	}
	
	public String getInclude(String tag) {
		return getInclude(tag, CONTENT_LAST);
	}
	
	public String getInclude(String tag, int get_type) {
		String result = null;
		get_type = get_type == CONTENT_ARRAY?CONTENT_RANDOM:get_type;
		
		if (includes.get(tag) != null && includes.get(tag).size() > 0) {
			List<String> stored = prepareInclude(tag, get_type);
			StringBuffer sb = new StringBuffer();
			for (String filename : stored) {
				sb.append(filename);
			}
			result = sb.toString();
		}
		return result;
	}

	public String getIncludeContent(String tag) throws FileSystemException {
		return getIncludeContent(tag, CONTENT_LAST);
	}
	
	public String getIncludeContent(String tag, int get_type) throws FileSystemException {
		String result = "";
		if (includes.get(tag) != null && includes.get(tag).size() > 0) {
			List<String> stored = prepareInclude(tag, get_type);
			StringBuffer sb = new StringBuffer();
			for (String filename : stored) {
				sb.append(FileSystem.fileGetContents(filename));
			}
			result = sb.toString();
		}
		return result;
	}
	
	private List<String> prepareInclude(String tag, int get_type) {
		List<String> result = new ArrayList<String>();
		List<String> stored = includes.get(tag);
		if (get_type == CONTENT_LAST) {
			result.add(stored.get(stored.size() - 1));
		} else 	if (get_type == CONTENT_RANDOM) {
			int index = (new Float(Math.random() * stored.size())).intValue();
			result.add(stored.get(index));
		} else {
			result = stored; 
		}
		return result;
	}
}
