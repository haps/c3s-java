/**
 * 
 */
package org.c3s.content;

/**
 * @author azajac
 *
 */
public class PathElement {

	private String href = null;
	private String title = null;
	
	public PathElement(String href, String title) {
		this.href = href;
		this.title = title;
	}

	/**
	 * @return the href
	 */
	public String getHref() {
		return href;
	}

	/**
	 * @param href the href to set
	 */
	public void setHref(String href) {
		this.href = href;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
}
