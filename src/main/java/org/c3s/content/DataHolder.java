package org.c3s.content;

import java.util.HashMap;
import java.util.Map;

import org.c3s.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DataHolder {

	private static Logger logger = LoggerFactory.getLogger(DataHolder.class);

	String tag;
	Object data;
	Object template;
	Map<String, Object> params = new HashMap<String, Object>();
	boolean cacheble = true;
	Map<String, Object> html = new HashMap<String, Object>();

	public DataHolder(String tag, Object data, Object template, String[] params, boolean cacheble) {
		this(tag, data, template, Utils.paramsArrayToMap(params), cacheble);
	}
	
	public DataHolder(String tag, Object data, Object template, Map<String, Object> params, boolean cacheble) {
		this.data = data;
		this.tag = tag;
		if (params != null) {
			this.params = params;
		}
		this.cacheble = cacheble;
		this.template = template;
		
		StringBuffer sb = new StringBuffer();
		sb.append(tag);
		sb.append(", ");
		sb.append(data instanceof String?Utils.substr(data.toString(),0, 100, "..."):data.getClass().getName());
		if (template != null) {
			sb.append(", ");
			sb.append(template instanceof String?Utils.substr(template.toString(),0, 100,"..."):template.getClass().getName());
		}
		sb.append(", {");
		if (params != null) {
			for(String name: params.keySet()) {
				sb.append(name);
				sb.append(":");
				sb.append(params.get(name));
				sb.append(", ");
			}
		}
		sb.append("} ,");
		sb.append(cacheble);
		logger.debug("Add content data: " + sb.toString());
	}

}
