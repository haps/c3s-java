package org.c3s.content.transformers;

import java.io.File;

public class NotApplicableException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NotApplicableException(File template, Transformer transformer) {
		this(template.getPath(), transformer);
	}
	
	public NotApplicableException(String template, Transformer transformer) {
		this("Template \""  + template  + "\" not applicable for transformer \"" + transformer.getClass().getName() + "\"");
	}

	public NotApplicableException(File template, Transformer transformer, Throwable exception) {
		this(template.getPath(), transformer, exception);
	}
	
	public NotApplicableException(String template, Transformer transformer, Throwable exception) {
		this("Template \""  + template  + "\" not applicable for transformer \"" + transformer.getClass().getName() + "\"", exception);
	}
	
	public NotApplicableException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public NotApplicableException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public NotApplicableException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	/*
	public NotApplicableException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}
	*/

}
