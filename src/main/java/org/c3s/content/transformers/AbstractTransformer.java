/**
 * 
 */
package org.c3s.content.transformers;

import java.util.Set;
import java.util.TreeSet;

import org.c3s.utils.RegexpUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author azajac
 *
 */
abstract public class AbstractTransformer {
	
	private static Logger logger = LoggerFactory.getLogger(AbstractTransformer.class);

	protected Set<String> extensions = new TreeSet<String>();
	protected Set<String> seen = new TreeSet<String>();
	
	/**
	 * @param filename
	 * @return
	 */
	protected boolean checkExtension(String filename) {
		
		logger.debug("Template file: " + filename);
		logger.debug("Extensions: " + extensions);
		
		boolean result = true;
		if (!seen.contains(filename)) {
			result = false;
			String extension = RegexpUtils.preg_replace("~^.+\\.([^\\.]+)$~is", filename, "$1").toLowerCase();
			logger.debug("Extension: " + extension);
			if (extensions.contains(extension)) {
				seen.add(extension);
				result = true;
			}		
		}
		return result;
	}
	
}
