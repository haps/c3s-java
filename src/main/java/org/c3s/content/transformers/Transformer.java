package org.c3s.content.transformers;

import org.c3s.content.ContentObject;

public interface Transformer {
	
	/**
	 * @param object
	 * @return
	 */
	public boolean isApplicable(Object object);
	
	/**
	 * @param data
	 * @param template
	 * @param params
	 * @param content
	 * @return
	 * @throws Exception 
	 */
	public String transform(Object data, Object template, Object params, ContentObject content) throws Exception;
	
	/**
	 * @param data
	 * @param template
	 * @param params
	 * @return
	 */
	public String transform(Object data, Object template, Object params) throws Exception;	
}
