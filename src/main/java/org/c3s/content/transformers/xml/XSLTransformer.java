/**
 * 
 */
package org.c3s.content.transformers.xml;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;

import org.c3s.content.ContentObject;
import org.c3s.content.transformers.AbstractTransformer;
import org.c3s.content.transformers.NotApplicableException;
import org.c3s.content.transformers.Transformer;
import org.c3s.exceptions.XMLException;
import org.c3s.utils.FileSystem;
import org.c3s.xml.XMLManager;
import org.c3s.xml.utils.XMLUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

/**
 * @author azajac
 *
 */
public class XSLTransformer extends AbstractTransformer implements Transformer {

	private static Logger	 logger = LoggerFactory.getLogger(XSLTransformer.class); 
	/**
	 * 
	 */
	public XSLTransformer() {
		extensions.add("xsl");
		extensions.add("xslt");
	}

	/* (non-Javadoc)
	 * @see org.c3s.content.transformers.Transformer#isApplicable(java.lang.Object)
	 */
	@Override
	public boolean isApplicable(Object object) {
		boolean ret = object instanceof javax.xml.transform.Transformer 
				|| object instanceof String && checkExtension((String)object);
		return ret;
	}

	/* (non-Javadoc)
	 * @see org.c3s.content.transformers.Transformer#transform(java.lang.Object, java.lang.Object, java.lang.Object, org.c3s.content.ContentObject)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String transform(Object data, Object template, Object params, ContentObject content) throws Exception  {
		String result = "";
		try {
			javax.xml.transform.Transformer transformer = null;
			if (template instanceof String) {
				//File file = new File((String)template).getAbsoluteFile();
				File file = FileSystem.newFile((String)template);
				
				if (file.exists() && file.isFile()) {
					try {
						transformer = XMLManager.getTransformer(file);
					} catch (XMLException e) {
						throw new NotApplicableException(file, this, e);
					}
				} else {
					throw new FileNotFoundException("File \"" + template.toString()  +"\" not found!");
				}
			} else if (template instanceof javax.xml.transform.Transformer)  {
				transformer = (javax.xml.transform.Transformer)template;
			} else {
				throw new NotApplicableException(template.toString(), this);
			}
			synchronized (transformer) {
				result = XMLUtils.transformXML((Document)data, transformer, (Map<String, Object>)params);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see org.c3s.content.transformers.Transformer#transform(java.lang.Object, java.lang.Object, java.lang.Object)
	 */
	@Override
	public String transform(Object data, Object template, Object params) throws Exception {
		return transform(data, template, params, null);
	}

}
