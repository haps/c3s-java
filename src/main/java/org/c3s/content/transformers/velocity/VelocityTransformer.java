package org.c3s.content.transformers.velocity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Properties;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeServices;
import org.apache.velocity.runtime.log.LogChute;
import org.c3s.content.ContentObject;
import org.c3s.content.transformers.AbstractTransformer;
import org.c3s.content.transformers.NotApplicableException;
import org.c3s.content.transformers.Transformer;
import org.c3s.utils.FileSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VelocityTransformer extends AbstractTransformer implements Transformer, LogChute {

	private static Logger	logger = LoggerFactory.getLogger(VelocityTransformer.class);
	
	HashMap<String, VelocityEngine> velocity = new HashMap<String, VelocityEngine>();
	/**
	 * @throws Exception 
	 * 
	 */
	public VelocityTransformer() throws Exception {
		/**
		 * 
		 */
		extensions.add("vm");
		extensions.add("vlc");
		extensions.add("htm");
		extensions.add("html");
	}

	protected VelocityEngine getEngineForFile(File file) {
		VelocityEngine ve = velocity.get(file.getParent());
		if (ve == null) {
			Properties props = new Properties();
			props.put("resource.loader", "file");
			props.put("file.resource.loader.class", "org.apache.velocity.runtime.resource.loader.FileResourceLoader");
			props.put("file.resource.loader.cache", "true");
			props.put("file.resource.loader.modificationCheckInterval", "2");
			props.put("file.resource.loader.path", file.getParent());
			ve = new VelocityEngine();
			ve.init(props);
			velocity.put(file.getParent(), ve);
		}
		return ve;
	}
	/* (non-Javadoc)
	 * @see org.c3s.content.transformers.Transformer#isApplicable(java.lang.Object)
	 */
	@Override
	public boolean isApplicable(Object object) {
		boolean ret = object instanceof org.apache.velocity.Template 
				|| object instanceof String && checkExtension((String)object);
		return ret;
	}

	@Override
	public String transform(Object data, Object template, Object params, ContentObject content) throws Exception {
		String result = "";
		try {
			VelocityContext context;
			if (data instanceof VelocityContext) {
				context = (VelocityContext)data; 
			} else {
				context = new VelocityContext();
				context.put("data", data);
			}
			if (params != null) {
				context.put("parameters", params);
			} else {
				context.put("parameters", new HashMap<String, String>());
			}
			Template transformer = null;
			if (template instanceof String) {
				//File file = new File((String)template).getAbsoluteFile();
				File file = FileSystem.newFile((String)template);
				
				if (file.exists() && file.isFile()) {
					try {
						//logger.debug("Velocity Directory: " + file.getParent());
						//velocity.setProperty("file.resource.loader.path", file.getParent());
						//transformer = velocity.getTemplate(file.getName(), "UTF-8");
						transformer = getEngineForFile(file).getTemplate(file.getName(), "UTF-8");
					} catch (Exception e) {
						logger.debug(e.getMessage());
						throw new NotApplicableException(file, this);
					}
				} else {
					throw new FileNotFoundException("File \"" + template.toString()  +"\" not found!");
				}
			} else if (template instanceof Template)  {
				transformer = (Template)template;
			} else {
				throw new NotApplicableException(template.toString(), this);
			}
			StringWriter writer = new StringWriter();
			transformer.merge(context, writer);
			result = writer.getBuffer().toString();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		return result;
	}

	@Override
	public String transform(Object data, Object template, Object params) throws Exception {
		return transform(data, template, params, null);
	}

	@Override
	public void init(RuntimeServices arg0) throws Exception {
		// DO Nothing
	}

	@Override
	public boolean isLevelEnabled(int arg0) {
		return true;
	}

	@Override
	public void log(int arg0, String arg1) {
		logger.debug(arg1);
	}

	@Override
	public void log(int arg0, String arg1, Throwable arg2) {
		logger.debug(arg1, arg2);
	}

}
