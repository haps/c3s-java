// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst ansi nonlb 
// Source File Name:   XMLManager.java

package org.c3s.parsers;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.c3s.utils.FileSystem;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

// Referenced classes of package com.pozitive.framework.xml:
//			ElementHandler

public class XMLManager {

	// private static final String DEFAULT_XML_ENCODING = "UTF-8";

	protected static XPath xpath = XPathFactory.newInstance().newXPath();
	protected static Map<String, XPathExpression> exps = new HashMap<String, XPathExpression>();

	protected static String MD5(String source) {
		StringBuffer md5 = new StringBuffer();

		try {
			MessageDigest digest = MessageDigest.getInstance("MD5");
			byte[] buff = digest.digest(source.getBytes());
			for (int i = 0; i < buff.length; i++) {
				md5.append(Integer.toHexString(0x0100 + (buff[i] & 0x00FF)).substring(1));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return md5.toString();
	}

	public static Object query(String expr, Node input, QName type) throws XPathExpressionException {
		Object ret = null;
		String hash = MD5(expr);
		XPathExpression exp;
		if (!exps.containsKey(hash)) {
			exp = xpath.compile(expr);
			exps.put(hash, exp);
		} else {
			exp = exps.get(hash);
		}
		ret = exp.evaluate(input, type);

		return ret;
	}

	public XMLManager() {
	}

	public static void parseXML(String xmlFilename, Map<String, ElementHandler> handlers) throws Exception {
		//File xmlFile = new File(xmlFilename);
		File xmlFile = FileSystem.newFile(xmlFilename);
		parseXML(xmlFile, handlers);
	}
	
	public static void parseXML(File xmlFile, Map<String, ElementHandler> handlers) throws Exception {
		FileInputStream stream = null;
		try {
			stream = new FileInputStream(xmlFile);
			Document doc = getWellFormedDocument(stream);
			Element root = doc.getDocumentElement();
			ElementHandler handler = (ElementHandler) handlers.get(root.getTagName());
			if (handler != null) {
				handler.performAction(root);
				if (!handler.isHandlesSubTree())
					processNode(root, handlers);
				handler.subTreeComplete();
			} else {
				processNode(root, handlers);
			}
		} finally {
			if (stream != null) stream.close();
		}
	}

	private static void processNode(Element node, Map<String, ElementHandler> handlers) throws Exception {
		if (node == null)
			return;

		NodeList children = node.getChildNodes();
		int i = 0;
		for (int n = children.getLength(); i < n; i++) {
			if (children.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element current = (Element) children.item(i);
				ElementHandler handler = (ElementHandler) handlers.get(current.getTagName());
				if (handler == null)
					throw new Exception("XMLManager: Failed to find handler for XML node ".concat(String.valueOf(String.valueOf(current.getTagName()))));
				handler.performAction(current);
				if (!handler.isHandlesSubTree())
					processNode(current, handlers);
				handler.subTreeComplete();
			}
		}

	}

	private static int posIn(byte src[], int pos, byte expr) {
		for (int i = pos; i < src.length; i++)
			if (src[i] == expr)
				return i;

		return -1;
	}

	private static int posIn(byte src[], int pos, byte expr[]) {
		for (int si = pos; (si = posIn(src, si, expr[0])) >= 0 && si + expr.length < src.length; si++) {
			boolean found = true;
			int k = 1;
			do {
				if (k >= expr.length)
					break;
				if (src[si + k] != expr[k]) {
					found = false;
					break;
				}
				k++;
			} while (true);
			if (found)
				return si;
		}

		return -1;
	}

	public static Document getWellFormedDocument(InputStream rawXmlCode) throws Exception {
		return getWellFormedDoc(inputStreamToByteArray(rawXmlCode));
	}

	private static Document getWellFormedDoc(byte rawXmlCode[]) throws Exception {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		DocumentBuilder bld = factory.newDocumentBuilder();
		InputStream is = new ByteArrayInputStream(rawXmlCode);
		Document doc = null;
		String msg = null;
		try {
			doc = bld.parse(is);
		} catch (Exception ex) {
			msg = ex.getCause().getMessage();
			is = null;
			int s = 0;
			do {
				if ((s = posIn(rawXmlCode, s, "<!DOCTYPE ".getBytes())) < 0)
					break;
				int e = posIn(rawXmlCode, s + 10, (byte) 62);
				if (e < 0)
					break;
				String internals = (new String(rawXmlCode, s + 10, e - (s + 10))).toLowerCase();
				boolean fnd = internals.indexOf(" system ") >= 0 || internals.indexOf(" public ") >= 0;
				if (!fnd)
					continue;
				int n = (e - s) + 1;
				byte tmp[] = new byte[rawXmlCode.length - n];
				int i;
				for (i = 0; i < s; i++)
					tmp[i] = rawXmlCode[i];

				for (; i < tmp.length; i++)
					tmp[i] = rawXmlCode[i + n];

				is = new ByteArrayInputStream(tmp);
				break;
			} while (true);
			if (is != null)
				try {
					doc = bld.parse(is);
				} catch (Exception ex2) {
					msg = ex2.getCause().getMessage();
				}
		}
		if (doc == null)
			throw new Exception(msg);
		else
			return doc;
	}

	private static byte[] inputStreamToByteArray(InputStream is) throws Exception {
		byte b[] = new byte[2048];
		ByteArrayOutputStream outp = new ByteArrayOutputStream();
		int k;
		while ((k = is.read(b)) > 0)
			outp.write(b, 0, k);
		return outp.toByteArray();
	}

	public static String getWellFormed(InputStream rawXmlCode) throws Exception {
		return docToString(getWellFormedDoc(inputStreamToByteArray(rawXmlCode)));
	}

	public static InputStream getWellFormed2(InputStream rawXmlCode) throws Exception {
		return docToIS(getWellFormedDoc(inputStreamToByteArray(rawXmlCode)));
	}

	/**
	 * @param xml
	 * @param buffer
	 * @param props
	 * @throws TransformerException
	 * @throws Exception
	 */
	protected static void saveXMLIntoSream(Element elm, OutputStream buffer, Properties props) throws TransformerException {

		DOMSource domSource = new DOMSource(elm);
		TransformerFactory factory = TransformerFactory.newInstance();
		Transformer transformer = factory.newTransformer();

		String defencoding = transformer.getOutputProperty("encoding");
		transformer.setOutputProperty("encoding", elm.getOwnerDocument().getXmlEncoding());

		StreamResult result = new StreamResult(buffer);

		if (props != null) {
			transformer.setOutputProperties(props);
		}

		transformer.transform(domSource, result);
		transformer.setOutputProperty("encoding", defencoding);
	}

	private static InputStream docToIS(Document doc) throws IOException {

		Properties props = new Properties();
		props.put(OutputKeys.OMIT_XML_DECLARATION, "no");

		ByteArrayOutputStream bos = new ByteArrayOutputStream();

		try {
			saveXMLIntoSream(doc.getDocumentElement(), bos, props);
		} catch (TransformerException e) {
			throw new IOException(e);
		}
		bos.close();
		return new ByteArrayInputStream(bos.toByteArray());
	}

	public static String docToString(Document doc, boolean omitEncoding, boolean omitDecl) throws IOException {

		/*
		 * Properties props = new Properties();
		 * props.put(OutputKeys.OMIT_XML_DECLARATION, omitDecl?"yes":"no"); if
		 * (omitEncoding) { props.put(OutputKeys.OMIT_XML_DECLARATION, null); }
		 * 
		 * ByteArrayOutputStream bos = new ByteArrayOutputStream();
		 * 
		 * try { saveXMLIntoSream(doc, bos, props); } catch (TransformerException e)
		 * { throw new IOException(e); }
		 * 
		 * bos.close(); return bos.toString();
		 */
		return elmToString(doc.getDocumentElement(), omitEncoding, omitDecl);
	}

	public static String elmToString(Element elm, boolean omitEncoding, boolean omitDecl) throws IOException {

		Properties props = new Properties();
		props.put(OutputKeys.OMIT_XML_DECLARATION, omitDecl ? "yes" : "no");
		if (omitEncoding) {
			//props.put(OutputKeys.OMIT_XML_DECLARATION, null);
			props.remove(OutputKeys.OMIT_XML_DECLARATION);
		}

		ByteArrayOutputStream bos = new ByteArrayOutputStream();

		try {
			saveXMLIntoSream(elm, bos, props);
		} catch (TransformerException e) {
			throw new IOException(e);
		}

		bos.close();
		return bos.toString();

	}

	public static String elmToString(Element elm) throws IOException {
		return elmToString(elm, true, false);
	}

	public static String docToString(Document doc) throws IOException {
		return docToString(doc, true, false);
	}

	public static String docToString2(Document doc) throws IOException {
		return docToString(doc, false, false);
	}

	public static NodeList findItems(Document doc, String what) throws XPathExpressionException {
		NodeList list = (NodeList) query(what, doc.getDocumentElement(), XPathConstants.NODESET);
		return list;
	}

	public static Object findItem(Document doc, String what) throws XPathExpressionException {

		Object elm = null;
		NodeList list = findItems(doc, what);
		if (list.getLength() > 0) {
			elm = list.item(0);
		}
		/*
		 * XPath xpath = new XPath(what); Object elm = xpath.selectSingleNode(doc);
		 */
		return elm;
	}

	public static int setValues(Document doc, String what, String itemName, String itemValue) throws XPathExpressionException {
		// XPath xpath = new XPath(what);
		// List list = xpath.selectNodes(doc);
		NodeList list = findItems(doc, what);
		int n = list.getLength();
		for (int i = 0; i < n; i++) {
			Node item = list.item(i);
			if (item.getNodeType() == Node.ATTRIBUTE_NODE) {
				// Attribute attr = (Attribute)item;
				if (itemValue != null) {
					item.setNodeValue(itemValue);
					// attr.setValue(itemValue);
				} else {
					// Element parent = attr.getParent();
					Element parent = (Element) item.getParentNode();
					// parent.removeAttribute(attr.getName());
					parent.removeAttributeNode((Attr) item);
				}
				continue;
			}
			if (item.getNodeType() == Node.ELEMENT_NODE) {
				Element elem = (Element) item;
				if (itemValue == null) {
					if (itemName == null) {
						elem.getParentNode().removeChild(elem);
					} else {
						elem.removeAttribute(itemName);
					}
					continue;
				}
				if (itemName == null)
					elem.setTextContent(itemValue);
				else
					elem.setAttribute(itemName, itemValue);
			} else {
				throw new XPathExpressionException(String.valueOf(String.valueOf((new StringBuffer("Objects of the '")).append(item.getClass().toString()).append(
						"' are under development"))));
			}
		}
		return n;
	}

}
