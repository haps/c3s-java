package org.c3s.parsers;

import org.w3c.dom.Element;

public interface ElementHandler {

	public abstract void performAction(Element element) throws Exception;

	public abstract boolean isHandlesSubTree();

	public abstract void subTreeComplete() throws Exception;
}
