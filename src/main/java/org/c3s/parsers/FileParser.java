// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst ansi nonlb 
// Source File Name:   FileParser.java

package org.c3s.parsers;

import java.io.File;
import java.util.Map;

// Referenced classes of package com.pozitive.framework.xml:
//			XMLManager

public abstract class FileParser {

	public void parse() throws Exception {
		XMLManager.parseXML(getFile(), getHandlers());
	}

	public abstract File getFile();

	public abstract Map<String, ElementHandler> getHandlers();
}
