// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst ansi nonlb 
// Source File Name:   XMLVector.java

package org.c3s.parsers;

import java.util.ArrayList;

public class XMLVector extends ArrayList<Object> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1950653889755710235L;
	
	String _name = null;
	String _listAttr = null;

	public XMLVector() {
		_name = "list";
		_listAttr = "";
	}

	public void setName(String v) {
		_name = v;
	}

	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("<");
		sb.append(_name);
		if(_listAttr.length() != 0)
			sb.append(' ').append(_listAttr);
		sb.append(">\n");
		for(int i = 0; i < size(); i++) {
			sb.append(get(i).toString());
			sb.append("\n");
		}

		sb.append("</");
		sb.append(_name);
		sb.append(">\n");
		return sb.toString();
	}

	public void setAttrib(String attr) {
		if(attr != null)
			_listAttr = attr;
	}

	public void addAttrib(String attr) {
		if(attr != null)
			_listAttr = String.valueOf(String.valueOf((new StringBuffer(String.valueOf(String.valueOf(attr)))).append(" ").append(_listAttr))).trim();
	}
}
