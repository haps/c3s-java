// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst ansi nonlb 
// Source File Name:   BaseHandler.java

package org.c3s.parsers.handlers;

import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.c3s.parsers.ElementHandler;

public class BaseHandler implements ElementHandler {

	public BaseHandler() {
	}

	public boolean isHandlesSubTree() {
		return false;
	}

	public void performAction(Element element) throws Exception {
	}

	public void subTreeComplete() throws Exception {
	}

	public Map<String, String> getParameters(Element node) {
		NodeList children = node.getChildNodes();
		Map<String, String> params = new HashMap<String, String>();
		int i = 0;
		for(int n = children.getLength(); i < n; i++) {
			if (children.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element child = (Element)children.item(i);
				if(!child.getTagName().equals("parameter") && !child.getTagName().equals("property"))
					continue;
				String name = child.getAttribute("name");
				String value = child.getAttribute("value");
				if(name != null && value != null) {
					params.put(name, value);
				}
			}
		}
		return params;
	}
}
