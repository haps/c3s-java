/**
 * 
 */
package org.c3s.storage;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author azajac
 *
 */
public abstract class AbstractStorage implements StorageInterface {

	abstract protected Map<Object, Object> getMap();
	abstract protected Map<Object, Long> getTimes();
	
	public AbstractStorage() {
	}
	
	/* (non-Javadoc)
	 * @see java.util.Map#size()
	 */
	@Override
	public int size() {
		return getMap().size();
	}

	/* (non-Javadoc)
	 * @see java.util.Map#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return getMap().isEmpty();
	}

	/* (non-Javadoc)
	 * @see java.util.Map#containsKey(java.lang.Object)
	 */
	@Override
	public boolean containsKey(Object key) {
		//return getMap().containsKey(key);
		return get(key) != null;
	}

	/* (non-Javadoc)
	 * @see java.util.Map#containsValue(java.lang.Object)
	 */
	@Override
	public boolean containsValue(Object value) {
		return getMap().containsValue(value);
	}

	/* (non-Javadoc)
	 * @see java.util.Map#get(java.lang.Object)
	 */
	@Override
	public Object get(Object key) {
		Object result = null;
		if (getTimes() != null &&  getMap() != null) {
			if (getTimes().containsKey(key)) {
				Long expiries = getTimes().get(key); 
				if (expiries == 0 || System.nanoTime() < expiries) {
					result = getMap().get(key);
				} else {
					getMap().remove(key);
					getTimes().remove(key);
				}
			}
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see java.util.Map#put(java.lang.Object, java.lang.Object)
	 */
	@Override
	public Object put(Object key, Object value) {
		synchronized (this) {
			return put(key, value, 0L);
		}
	}

	/* (non-Javadoc)
	 * @see java.util.Map#remove(java.lang.Object)
	 */
	@Override
	public Object remove(Object key) {
		Object result = get(key);
		synchronized (this) {
			getMap().remove(key);
			getTimes().remove(key);
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see java.util.Map#putAll(java.util.Map)
	 */
	@Override
	public void putAll(Map<? extends Object, ? extends Object> m) {
		synchronized (this) {
			putAll(m, 0L);
		}
	}

	@Override
	public void clear() {
		synchronized (this) {
			getMap().clear();
			getTimes().clear();
		}
	}

	/* (non-Javadoc)
	 * @see java.util.Map#keySet()
	 */
	@Override
	public Set<Object> keySet() {
		return getMap().keySet();
	}

	@Override
	public Collection<Object> values() {
		return getMap().values();
	}

	/* (non-Javadoc)
	 * @see java.util.Map#entrySet()
	 */
	@Override
	public Set<java.util.Map.Entry<Object, Object>> entrySet() {
		return getMap().entrySet();
	}

	/* (non-Javadoc)
	 * @see org.c3s.storage.StorageInterface#put(java.lang.Object, java.lang.Object, java.lang.Long)
	 */
	@Override
	public Object put(Object key, Object value, Long mills) {
		synchronized (this) {
			if (getTimes() != null &&  getMap() != null) {
				getTimes().put(key, (mills > 0)?(System.nanoTime() + mills * 1000000):0);
				return getMap().put(key, value);
			} else {
				return null;
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.c3s.storage.StorageInterface#putAll(java.util.Map, java.lang.Long)
	 */
	@Override
	public void putAll(Map<? extends Object, ? extends Object> m, Long mills) {
		synchronized (this) {
			getMap().putAll(m);
			for (Object key: m.keySet()) {
				getTimes().put(key, (mills > 0)?(System.nanoTime() + mills * 1000000):0);
			}
		}
	}
	
	@Override
	public void clearExpired() {
		synchronized (this) {
			Set<Object> keys =  new HashSet<Object>(getTimes().keySet());
			for (Object key: keys) {
				Long time = getTimes().get(key);
				if (time > 0 && time < System.nanoTime()) {
					getMap().remove(key);
					getTimes().remove(key);
				}
			}
		}
	}
	
	@Override
	public void updateExpire(Object key, Long mills) {
		if (getMap().get(key) != null) {
			getTimes().put(key, (mills > 0)?(System.nanoTime() + mills * 1000000):0);
		}
	}
	
}
