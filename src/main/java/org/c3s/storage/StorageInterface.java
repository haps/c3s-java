/**
 * 
 */
package org.c3s.storage;

import java.util.Map;

/**
 * @author azajac
 *
 */
public interface StorageInterface extends Map<Object, Object> {
	
	public Object put(Object key, Object value, Long mills);
	public void putAll(Map<? extends Object,? extends Object> m, Long mills);
	public void clearExpired();
	public void updateExpire(Object key, Long mills);
}
