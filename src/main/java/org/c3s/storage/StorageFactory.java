package org.c3s.storage;

import java.util.HashMap;
import java.util.Map;

public class StorageFactory {

	private static Map<String, StorageInterface> elements = new HashMap<String, StorageInterface>();
	
	public static void register(String name, StorageInterface storage) {
		synchronized (StorageFactory.class) {
			if (elements.size() == 0) {
				elements.put("default", storage);
			}
			elements.put(name, storage);
		}
	}
	
	public static void register(StorageType type, StorageInterface storage) {
		register(type.getName(), storage);
	}
	
	public static StorageInterface getStorage(StorageType type) {
		return getStorage(type.getName());
	}
	public static StorageInterface getStorage(String name) {
		return elements.get(name);
	}
}
