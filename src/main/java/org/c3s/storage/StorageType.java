package org.c3s.storage;

public enum StorageType {
	PERSISTENT("persistent"),
	APPLICATION("application"),
	SESSION("session"),
	REQUEST("request");

	private String name;
	
	private StorageType(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}
