package org.c3s.storage;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ApplicationStorage extends AbstractStorage {

	Map<Object, Object> map = new ConcurrentHashMap<Object, Object>();
	Map<Object, Long> times = new ConcurrentHashMap<Object, Long>();
	
	@Override
	protected Map<Object, Object> getMap() {
		return map;
	}

	@Override
	protected Map<Object, Long> getTimes() {
		return times;
	}

}
