/**
 * 
 */
package org.c3s.storage;

import java.util.HashMap;
import java.util.Map;

/**
 * @author AZayac
 *
 */
public class ThreadStorage extends AbstractStorage {

	private ThreadLocal<Map<Object, Object>> map = new ThreadLocal<Map<Object,Object>>();
	private ThreadLocal<Map<Object, Long>> times = new ThreadLocal<Map<Object,Long>>();
	
	
	/* (non-Javadoc)
	 * @see org.c3s.storage.AbstractStorage#getMap()
	 */
	@Override
	protected Map<Object, Object> getMap() {
		if (map.get() == null) {
			map.set(new HashMap<Object, Object>());
		}
		// TODO Auto-generated method stub
		return map.get();
	}

	/* (non-Javadoc)
	 * @see org.c3s.storage.AbstractStorage#getTimes()
	 */
	@Override
	protected Map<Object, Long> getTimes() {
		if (times.get() == null) {
			times.set(new HashMap<Object, Long>());
		}
		// TODO Auto-generated method stub
		return times.get();
	}

}
