package org.c3s.web.nodes.path;

import java.util.List;

import org.c3s.annotations.ActionUrl;
import org.c3s.annotations.Controller;
import org.c3s.annotations.Parameter;
import org.c3s.content.ContentObject;
import org.c3s.content.PathElement;
import org.c3s.dispatcher.RedirectControlerInterface;
import org.c3s.exceptions.XMLException;
import org.c3s.site.LanguageInterface;
import org.c3s.site.SiteLoader;
import org.c3s.web.nodes.AbstractNode;
import org.c3s.web.nodes.NodeAdditionalInfo;
import org.c3s.web.nodes.SiteNodes;
import org.c3s.xml.utils.XMLUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

@Controller
public class PathController {

	private static final Logger logger = LoggerFactory.getLogger(PathController.class);
	
	private SiteNodes nodes = null;
	private LanguageInterface language = null;
	//private List<PathElement> path = new ArrayList<PathElement>();
	String parentTitle = null;
	
	protected List<PathElement> getPath() {
		//return path;
		return ContentObject.getInstance().getPath();
	}
	
	public void setPath(RedirectControlerInterface redirect, @ActionUrl String url) {
		if (!redirect.hasRedirect()) {
			if (nodes == null) {
				//language = SiteLoader.get().getSite().getCurrentLanguage();
				language = SiteLoader.getSite().getCurrentLanguage();
				nodes = SiteNodes.getInstance();
			}
			if (language != null) {
				AbstractNode node = nodes.getCurrentNode();
				String isPath = node.getAttribute("is_path");
				if (!"false".equals(isPath)) {
					String title = node.getAttribute("title");
					List<NodeAdditionalInfo> menus = node.getAdditional("title"); 
					for (NodeAdditionalInfo menu: menus) {
						if (language.getId().equals(menu.getAttribute("lang"))) {
							title = menu.getAttribute("@text");
							if (title == null || title.length() == 0) {
								title = menu.getAttribute("value");
							}
							break;
						}
					}
					if (title == null) {
						title = parentTitle;
					} else {
						ContentObject.getInstance().setTitle(title);
						parentTitle = title;
					}
					if (title != null) {
						//path.add(new PathElement(url, title));
						ContentObject.getInstance().addPath(new PathElement(url, title));
					}
				}
			}
		}
	}

	public void getPath(@Parameter("tag") String tag, @Parameter("template") String template, 
		@Parameter("type") String type, RedirectControlerInterface redirect) throws XMLException {

		logger.debug("Path XML: debug");
		if (!redirect.hasRedirect()) {
			Document xml = XMLUtils.createXML("data");
			for (PathElement path: getPath()) {
				Element element = xml.createElement("item");
				element.setAttribute("href", path.getHref());
				element.setAttribute("title", path.getTitle());
				xml.getDocumentElement().appendChild(element);
			}
			ContentObject.getInstance().setData(tag, xml, template);
			logger.debug("Path XML:\n" + XMLUtils.xml2out(xml));
		}
	}
}
