package org.c3s.web.nodes.content;

import java.util.List;

import org.c3s.annotations.Controller;
import org.c3s.annotations.Parameter;
import org.c3s.content.ContentObject;
import org.c3s.site.GeneralSite;
import org.c3s.site.SiteLoader;
import org.c3s.web.nodes.AbstractNode;
import org.c3s.web.nodes.NodeAdditionalInfo;
import org.c3s.web.nodes.SiteNodes;

@Controller
public class ContentModule {
	
	private String language = "";
	
	public ContentModule() {
		GeneralSite site = SiteLoader.getSite();
		if (site.getCurrentLanguage() != null) {
			language = site.getCurrentLanguage().getId();
		}
	}
	
	public void process(@Parameter("nodes_instance") String nodes_instance) {
		AbstractNode node = SiteNodes.getInstance(nodes_instance).getCurrentNode();
		List<NodeAdditionalInfo> includes = node.getAdditional("include");
		processInclude(includes);
	}
	
	protected void processInclude(List<NodeAdditionalInfo> includes) {
		for (NodeAdditionalInfo include : includes) {
			String tag = include.getAttribute("tag");
			String filename = (include.getAttribute("value") != null)?include.getAttribute("value"):((include.getAttribute("@text") != null)?include.getAttribute("@text"):null);
			String lang = include.getAttribute("lang");
			if (tag != null && filename != null && (lang == null || lang.equals(language))) {
				if (processAdditionalAttributes(include)) {
					ContentObject.getInstance().setInclude(tag, filename);
				}
			}
		}
	}
	
	protected boolean processAdditionalAttributes(NodeAdditionalInfo include) {
		return true;
	}
	
}
