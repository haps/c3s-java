package org.c3s.web.nodes;

import java.util.HashMap;
import java.util.Map;

public class NodeAdditionalInfo {
	private String name;
	private Map<String, String> attributes;
	
	public NodeAdditionalInfo(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public Map<String, String> getAttributes() {
		return attributes;
	}
	
	public void setAttributes(Map<String, String> attributes) {
		if (this.attributes == null) {
			this.attributes = new HashMap<String, String>();
		}
		this.attributes.putAll(attributes);
	}

	public String getAttribute(String name) {
		if (this.attributes == null) {
			return null;
		}
		return this.attributes.get(name);
	}
}