package org.c3s.web.nodes.xml;

import java.util.HashMap;
import java.util.Map;

import org.c3s.web.nodes.AbstractNode;
import org.c3s.web.nodes.NodeAdditionalInfo;
import org.c3s.web.nodes.SiteNode;
import org.c3s.xml.NodeSet;
import org.c3s.xml.utils.XMLUtils;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public class SiteNodeXML extends SiteNode {

	/* (non-Javadoc)
	 * @see org.c3s.web.nodes.AbstractNode#load(java.lang.Object)
	 */
	@Override
	public void load(Object nodeInfo) {
		synchronized (nodeInfo) {
			Element elm = (Element)nodeInfo;
			if (elm.hasAttributes()) {
				NamedNodeMap attributes = elm.getAttributes();
				for (int i = 0; i < attributes.getLength(); i++) {
					Attr attribute = (Attr)attributes.item(i);
					addAttribute(attribute.getNodeName(), attribute.getNodeValue());
				}
			}
			
			NodeSet nodes = new NodeSet(elm.getChildNodes());
			
			for (Node node: nodes) {
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					if (node.getNodeName().equals("node")) {
						AbstractNode newNode = new SiteNodeXML();
						newNode.load(node);
						addNode(newNode);
					} else {
						Map<String, String> nodeAttr = new HashMap<String, String>();
						if (node.hasAttributes()) {
							NamedNodeMap attributes = node.getAttributes();
							for (int i = 0; i < attributes.getLength(); i++) {
								Attr attribute = (Attr)attributes.item(i);
								nodeAttr.put(attribute.getNodeName(), attribute.getNodeValue());
							}
						}
						if (node.getTextContent().trim().length() > 0) {
							nodeAttr.put("@text", node.getTextContent().trim());
						}
						this.addAdditional(node.getNodeName(), nodeAttr);
					}
				}
			}
		}
	}

	
	/**
	 * @return
	 */
	public Document exportToXML() {
		Document xml = null;
		try {
			xml = XMLUtils.createXML("node");
			Element root = xml.getDocumentElement();
			if (attributes != null) {
				for (String name: attributes.keySet()) {
					root.setAttribute(name, attributes.get(name));
				}
			}
			if (additional != null) {
				for (NodeAdditionalInfo node: additional) {
					Element elm = xml.createElement(node.getName());
					Map<String, String> attrs = node.getAttributes();
					for (String name: attrs.keySet()) {
						if (name.equals("@text")) {
							Node text = xml.createTextNode(attrs.get(name));
							elm.appendChild(text);
						} else {
							elm.setAttribute(name, attrs.get(name));
						}
					}
					root.appendChild(elm);
				}
			}
			
			if (nodes != null) {
				for (AbstractNode node: nodes) {
					Document elm = (Document)node.export("XML");
					XMLUtils.appendClonedNode(xml, elm);
				}
			}
			return xml;
		} catch (Exception e) {
			;
		}
		
		return xml;
	}
}
