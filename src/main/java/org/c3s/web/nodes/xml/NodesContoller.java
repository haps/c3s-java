package org.c3s.web.nodes.xml;

import org.c3s.annotations.Parameter;
import org.c3s.dispatcher.PatternerInterface;
import org.c3s.dispatcher.RedirectControlerInterface;
import org.c3s.dispatcher.UrlPart;
import org.c3s.site.GeneralSite;
import org.c3s.site.GeneralSite.Politics;
import org.c3s.site.SiteLoader;
import org.c3s.web.nodes.AbstractNode;
import org.c3s.web.nodes.SiteNode;
import org.c3s.web.nodes.SiteNodes;
import org.c3s.web.redirect.DirectRedirect;
import org.c3s.web.redirect.RelativeRedirect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class NodesContoller {
	
	private static final Logger logger = LoggerFactory.getLogger(NodesContoller.class);
	
	private SiteNodes nodes = null;
	
	//private boolean isRootNode = true;
	
	//private Site site = SiteLoader.get().getSite();
	private GeneralSite site;
	{
		synchronized (NodesContoller.class) {
			site = SiteLoader.getSite();
		}
	}
	
	
	protected abstract AbstractNode loadNodes(Object parameters) throws Exception;
	
	public void processNodes(@Parameter("nodes") String parameterNodes,
			@Parameter("nodes_instance") String nodes_instance,
			RedirectControlerInterface dispatcher, UrlPart workUrl, PatternerInterface patterner) throws Exception {
		AbstractNode node = null;
		if (nodes == null) {
			node = this.loadNodes(parameterNodes);
			nodes = SiteNodes.getInstance(nodes_instance);
			nodes.setCurrentNode(node);
		} 
		
		if (!dispatcher.hasRedirect()) {
			String pattern = workUrl.getPattern();
			if (pattern.endsWith("/")) {
				pattern = pattern.substring(0, pattern.length() - 1);
			}
			logger.debug("Current pattern:" + pattern);
			if (!pattern.isEmpty()) {
				//if (!isRootNode) {
					AbstractNode current = nodes.getCurrentNode();
					//logger.debug("Current node: {}", current.toString());
					node = ((SiteNode)current).getNodeByPattern(pattern);
					//logger.debug("Result node: {}", node == null?"null":node.toString());
				//} else {
					//node = nodes.getCurrentNode();
					//isRootNode = false;
				//}
			} else {
				SiteNodes.getInstance(nodes_instance).getCurrentNode().removeAttribute("self", true);
			}
			
			String lang = "";
			//if (site.getCurrentLanguage() != site.getDefaultLanguage() && site.getLanguagePolitic() == Politics.LANGUAGE_SUFFIX) {
			if (site.getCurrentLanguage() != site.getDefaultLanguage() && site.getLanguagePolitic() == Politics.LANGUAGE_SUFFIX) {
				//logger.info("Current language is: {}");
				lang = "/" + site.getCurrentLanguage().getSuffix();
			}
			//if ()
			if (node == null) {
				logger.debug("Set redirect new node == null");
				dispatcher.setRedirect(new DirectRedirect(lang + SiteNodes.getInstance(nodes_instance).getCurrentUrl()));
			} else {
				nodes.setCurrentNode(node);
				if (workUrl.getUrlReminder().length() == 0 && node.getAttribute("redirect") != null && node.getAttribute("redirect").length() > 0) {
					logger.debug("Set redirect new node by redirect");
					String redirect = node.getAttribute("redirect");
					dispatcher.setRedirect(redirect.startsWith("/")?new DirectRedirect(lang + redirect): new RelativeRedirect(redirect, patterner, lang));
				} 
			}
			
			
			if (!dispatcher.hasRedirect()) {
				prepareAdditionalInfo(SiteNodes.getInstance(nodes_instance).getCurrentNode());
			}
		}
	}

	protected void prepareAdditionalInfo(AbstractNode node) {
		/*
		String title = node.getAttribute("title");
		if (title != null) {
			ContentObject.getInstance().setTitle(title);
			if (node.getAttribute("path") != null) {
				ContentObject.getInstance().addPath(new PathElement(SiteNodes.getInstance().getCurrentUrl(), title));
			}
		}
		*/
	}
}

