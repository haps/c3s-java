package org.c3s.web.nodes.xml;

import org.c3s.annotations.Controller;
import org.c3s.web.nodes.AbstractNode;
import org.c3s.xml.XMLManager;
import org.w3c.dom.Document;

@Controller
public class NodesModuleXML extends NodesContoller {
	
	protected AbstractNode loadNodes(Object parameters) throws Exception {
		Document xml = XMLManager.getDocument(parameters.toString());
		AbstractNode node = new SiteNodeXML();
		synchronized (xml) {
			node.load(xml.getDocumentElement());
		}
		return node;
	}
	
}

