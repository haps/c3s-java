package org.c3s.web.nodes;

import java.io.File;

import org.c3s.content.ContentObject;
import org.c3s.exceptions.FileSystemException;
import org.c3s.exceptions.XMLException;
import org.c3s.utils.FileSystem;
import org.c3s.web.interfaces.Context;
import org.c3s.xml.utils.XMLUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class FileInclude {

	private String tag;
	private String xsl;
	private String file;
	private boolean passthrough;
	private Context context;

	public FileInclude(Context context) {
		this.context = context;
	}

	public void read(Element element) {
		tag = element.getAttribute("tag");
		xsl = element.getAttribute("xsl");
		file = element.getAttribute("file");
		passthrough = "true".equals(element.getAttribute("pass"));
	}

	public void apply(boolean url_reminder) throws XMLException, FileSystemException {
		
		if (!url_reminder || passthrough) {
			if (getTag().length() > 0 && getXsl().length() > 0 && getFile().length() > 0) {
				Document file = XMLUtils.load(context.getApplicationPath() + File.separator + context.getProperty("directory.include") + File.separator
						+ getFile());
				Document xsl  = XMLUtils.load(context.getApplicationPath() + File.separator + context.getProperty("directory.stylesheet") + File.separator + getXsl());
				ContentObject.getInstance().setData(getTag(), file, xsl);

			} else if (getTag().length() > 0 && getFile().length() > 0) {
				String content = FileSystem.fileGetContents(context.getApplicationPath() + File.separator + context.getProperty("directory.include") + File.separator
						+ getFile());
				ContentObject.getInstance().setData(getTag(), content);
			}
		}
	}

	/**
	 * @return the tag
	 */
	public String getTag() {
		return (tag == null) ? "" : tag;
	}

	/**
	 * @param tag
	 *          the tag to set
	 */
	public void setTag(String tag) {
		this.tag = tag;
	}

	/**
	 * @return the xsl
	 */
	public String getXsl() {
		return (xsl == null) ? "" : xsl;
	}

	/**
	 * @param xsl
	 *          the xsl to set
	 */
	public void setXsl(String xsl) {
		this.xsl = xsl;
	}

	/**
	 * @return the file
	 */
	public String getFile() {
		return (file == null) ? "" : file;
	}

	/**
	 * @param file
	 *          the file to set
	 */
	public void setFile(String file) {
		this.file = file;
	}

	/**
	 * @return the passthrough
	 */
	public boolean isPassthrough() {
		return passthrough;
	}

	/**
	 * @param passthrough
	 *          the passthrough to set
	 */
	public void setPassthrough(boolean passthrough) {
		this.passthrough = passthrough;
	}
}
