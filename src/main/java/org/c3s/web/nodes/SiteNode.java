package org.c3s.web.nodes;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public abstract class SiteNode extends AbstractNode {

	private static Logger logger = LoggerFactory.getLogger(SiteNode.class);
	/**
	 * @param pattern
	 * @return
	 */
	public AbstractNode getNodeByPattern(String pattern) {
		AbstractNode result = null;
		AbstractNode probably = null;
		if (getNodes() != null) {
			for (AbstractNode node: getNodes()) {
				String nodePattern = node.getAttribute("pattern");
				
				if (pattern.equals(nodePattern)) {
					result = node;
					break;
				} else if ("*".equals(nodePattern)) {
					//node.addAttribute("pattern", pattern);
					//result = node;
					//break;
					probably = node;
				}
			}
		}
		
		if (result == null && probably != null) {
			result = probably;
			result.addAttribute("pattern", pattern);
		}
		
		return result;
	}
	
	
	/* (non-Javadoc)
	 * @see org.c3s.web.nodes.AbstractNode#export()
	 */
	@Override
	public Object export() {
		return export(null);
	}
	
	/* (non-Javadoc)
	 * @see org.c3s.web.nodes.AbstractNode#export(java.lang.String)
	 */
	@Override
	public Object export(String type) {
		Object result = null;
		
		String methodName = "exportTo" + type;
		
		try {
			Method method = this.getClass().getDeclaredMethod(methodName);
			method.setAccessible(true);
			result = method.invoke(this);
		} catch (Exception e) {
			logger.error("Can't export " + this.getClass().getName() + " to " + type);
		}
		
		return result;
	}
	
}
