package org.c3s.web.nodes;

import org.c3s.storage.StorageFactory;
import org.c3s.storage.StorageType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SiteNodes {

	private static final Logger logger = LoggerFactory.getLogger(SiteNodes.class);
	
	public static SiteNodes getInstance() {
		return getInstance(null);
	}
	public static SiteNodes getInstance(String instance_name) {
		instance_name = SiteNodes.class + "_" + instance_name != null?instance_name:"";
		SiteNodes result = (SiteNodes)StorageFactory.getStorage(StorageType.REQUEST).get(instance_name);
		if (result == null) {
			result = new SiteNodes();
			StorageFactory.getStorage(StorageType.REQUEST).put(instance_name, result);
		}
		return result;
	}

	private AbstractNode currentNode = null;
	
	private String currentUrl = null;
	
	private AbstractNode rootNode = null;
	
	public AbstractNode getRootNode() {
		return rootNode;
	}
	
	public AbstractNode getCurrentNode() {
		return currentNode;
	}
	
	public String getCurrentUrl() {
		return currentUrl;
	}
	
	public void setCurrentUrl(String currentUrl) {
		this.currentUrl = currentUrl;
	}
	
	public void setCurrentNode(AbstractNode currentNode) {
		if (currentNode != getCurrentNode()) {
			currentNode.addAttribute("self", "1");
			this.currentNode = currentNode;
			if (rootNode == null) {
				rootNode = currentNode;
				currentUrl = "";
			} else {
				currentUrl += "/" + currentNode.getAttribute("pattern");
			}
			logger.debug("Set current node '{}' on URL '{}'", currentNode.getAttribute("pattern"), currentUrl);
		}
	}
}
