package org.c3s.web.nodes.menu;

import org.c3s.annotations.Parameter;
import org.c3s.content.ContentObject;
import org.c3s.dispatcher.UrlPart;
import org.c3s.dispatcher.modules.Singleton;
import org.c3s.web.nodes.AbstractNode;
import org.c3s.web.nodes.SiteNodes;
import org.c3s.xml.utils.XMLUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

public class MenuModule implements Singleton {
	
	private static Logger logger = LoggerFactory.getLogger(MenuModule.class);
	
	private AbstractNode startNode = null;
	private String currentUrl = null;
	
	public void getMenu(UrlPart url, @Parameter("tag") String tag, @Parameter("template") String template, 
			@Parameter("type") String type, @Parameter("nodes_instance") String nodes_instance) throws Exception {
		
		if (startNode == null) {
			startNode = SiteNodes.getInstance(nodes_instance).getCurrentNode();
			currentUrl = SiteNodes.getInstance(nodes_instance).getCurrentUrl();
		}
		
		if (url.getUrlReminder().length() == 0) {
			Document xml = XMLUtils.createXML("menu");
			xml.getDocumentElement().setAttribute("url", currentUrl);
			logger.debug("" + startNode);
			
			Document nodeXml = (Document)startNode.export("XML");
			logger.debug(XMLUtils.xml2out(nodeXml));
			XMLUtils.appendClonedNode(xml, nodeXml);
			this.presaveXML(xml);
			ContentObject.getInstance().setData(tag, xml, template);
		}
	}
	
	protected void presaveXML(Document doc) {
		
	}
}
