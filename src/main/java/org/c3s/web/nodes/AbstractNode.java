package org.c3s.web.nodes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractNode {
	
	protected List<AbstractNode> nodes;
	
	protected Map<String, String> attributes;
	
	protected List<NodeAdditionalInfo> additional;
	
	/**
	 * @param node
	 */
	public void addNode(AbstractNode node) {
		if (nodes == null) {
			nodes = new ArrayList<AbstractNode>();
		}
		nodes.add(node);
	}
	
	/**
	 * @return
	 */
	public List<AbstractNode> getNodes() {
		return nodes;
	}
	
	
	/**
	 * @param name
	 * @param value
	 */
	public void addAttribute(String name, String value) {
		if (attributes == null) {
			attributes = new HashMap<String, String>();
		}
		attributes.put(name, value);
	}
	
	/**
	 * @param name
	 * @return
	 */
	public String getAttribute(String name) {
		String result = null;
		
		if (attributes != null) {
			result = attributes.get(name);
		}
		return result;
	}
	
	/**
	 * @param name
	 * @return
	 */
	public void removeAttribute(String name, boolean depth) {
		if (attributes != null) {
			attributes.remove(name);
		}
		if (depth && getNodes() != null) {
			for (AbstractNode child: getNodes()) {
				child.removeAttribute(name, depth);
			}
		}
	}
	
	/**
	 * @param name
	 * @return
	 */
	public void removeAttribute(String name) {
		removeAttribute(name, false);
	}
	
	/**
	 * @param name
	 * @param info
	 */
	public void addAdditional(String name, Map<String, String> info) {
		if (additional == null) {
			additional = new ArrayList<NodeAdditionalInfo>();
		}
		
		NodeAdditionalInfo node = new NodeAdditionalInfo(name);
		node.setAttributes(info);
		
		additional.add(node);
	}
	
	public List<NodeAdditionalInfo> getAdditional(String tagName) {
		List<NodeAdditionalInfo> result = new ArrayList<NodeAdditionalInfo>();
		if (getInfo() != null) {
			for (NodeAdditionalInfo info: getInfo()) {
				if (info.getName().equals(tagName)) {
					result.add(info);
				}
			}
		}
		return result;
	}
	
	public List<NodeAdditionalInfo> getInfo() {
		return additional;
	}
	
	/**
	 * @param nodeInfo
	 */
	abstract public void load(Object nodeInfo);
	
	/**
	 * 
	 */
	abstract public Object export();
	
	/**
	 * @param type
	 */
	abstract public Object export(String type);
	
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Pattern: ");
		sb.append(this.getAttribute("pattern"));
		sb.append("\n");
		if (nodes != null) {
			sb.append("Childs: [ ");
			for (AbstractNode node : getNodes()) {
				sb.append(node.getAttribute("pattern"));
				sb.append(", ");
			}
			sb.append("]\n");
		}
		return sb.toString();
	}
}
