package org.c3s.web.db;

import java.sql.SQLException;
import java.util.Properties;

import org.c3s.annotations.Parameter;
import org.c3s.db.DBManager;

public class DbInitializer {

	public void init(@Parameter("name") String name, @Parameter("driver") String driver, @Parameter("dsn") String dsn, 
			@Parameter("user") String user, @Parameter("password") String password) throws SQLException {
		Properties prop = new Properties();
		prop.put("user", user);
		prop.put("password", password);
		DBManager.getConnection(name, driver, dsn, prop);
	}
	
}
