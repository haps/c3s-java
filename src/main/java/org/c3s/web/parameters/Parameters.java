/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.c3s.web.parameters;

import java.util.Arrays;
import java.util.List;

import org.c3s.dispatcher.modules.Singleton;
import org.c3s.web.context.PageRequestContext;


/**
 *
 * @author admin
 */
public class Parameters implements Singleton {

	/**
	 * get String
	 */
	public static String getString(String name, String default_value, String[] avaiable_values) {
		String ret_val = null; 
		String str = PageRequestContext.getInstance()._get(name);
		if (str == null) {
			str = default_value;
		} else if (avaiable_values != null) {
			List<String> values = Arrays.asList(avaiable_values);
			if (!values.contains(str)) {
				str = default_value;
			}
		}
		ret_val = str;
		return ret_val;
	}
	
	public static String getString(String name, String default_value) {
		return getString(name, default_value, null);
	}
	
	public static String getString(String name) {
		return getString(name, null);
	}
	
	/**
	 * get Long|Int
	 */
	public static Long getInt(String name, Long default_value, Long[] avaiable_values) {
		Long ret_val = null; 
		String str = PageRequestContext.getInstance()._get(name);
		if (str == null) {
			ret_val = default_value;
		} else {
			try {
				ret_val = Long.valueOf(str);
				if (avaiable_values != null) {
					List<Long> values = Arrays.asList(avaiable_values);
					if (!values.contains(ret_val)) {
						ret_val = default_value;
					}
				}
			} catch (Exception e) {
				ret_val = default_value;
			}
		}
		return ret_val;
	}
	
	public static Long getInt(String name, Long default_value) {
		return 	getInt(name, default_value, null);
	}
	
	public static Long getInt(String name) {
		return 	getInt(name, null);
	}
	
	/**
	 * get Unsigned Long|Int
	 */
	public static Long getUnsignedInt(String name, Long default_value, Long[] avaiable_values) {
		Long ret_val = null; 
		String str = PageRequestContext.getInstance()._get(name);
		if (str == null) {
			ret_val = default_value;
		} else {
			try {
				ret_val = Math.abs(Long.valueOf(str));
				if (avaiable_values != null) {
					List<Long> values = Arrays.asList(avaiable_values);
					if (!values.contains(ret_val)) {
						ret_val = default_value;
					}
				}
			} catch (Exception e) {
				ret_val = default_value;
			}
		}
		return ret_val;
	}
	
	public static Long getUnsignedInt(String name, Long default_value) {
		return getUnsignedInt(name, default_value, null);
	}
	
	public static Long getUnsignedInt(String name) {
		return getUnsignedInt(name, null);
	}
}
