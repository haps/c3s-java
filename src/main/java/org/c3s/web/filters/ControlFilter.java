/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.c3s.web.filters;

import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.c3s.utils.Utils;
import org.c3s.web.context.ApplicationContext;
import org.c3s.web.servlets.constants.ServletConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author admin
 */
public class ControlFilter implements Filter {

	private static Logger logger = LoggerFactory.getLogger(ControlFilter.class);
	
	// The filter configuration object we are associated with. If
	// this value is null, this filter instance is not currently
	// configured.
	private FilterConfig filterConfig = null;

	public ControlFilter() {
	}

	/**
	 * 
	 * @param request
	 *          The servlet request we are processing
	 * @param response
	 *          The servlet response we are creating
	 * @param chain
	 *          The filter chain we are processing
	 * 
	 * @exception IOException
	 *              if an input/output error occurs
	 * @exception ServletException
	 *              if a servlet error occurs
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		
		String start_pattern = ServletConstants.WORK_PATTERN.get(ApplicationContext.getInstance());
		start_pattern = ((HttpServletRequest)request).getContextPath() + start_pattern; 
		start_pattern = Utils.replaceRecursive(start_pattern, "//", "/");

		if (((HttpServletRequest)request).getRequestURI().startsWith(start_pattern)) {

			String worked_uri = ((HttpServletRequest)request).getRequestURI().substring(start_pattern.length());
			if (!worked_uri.startsWith("/")) {
				worked_uri = "/" + worked_uri;
			}
			
			logger.debug("ControlFilter:doFilter()");
	
			//HttpServletRequest req = (HttpServletRequest) request;
			//String uri = req.getRequestURI();
	
			this.getFilterConfig().getServletContext().getNamedDispatcher("ControlServlet").forward(request, response);
			
		} else {
			chain.doFilter(request, response);
		}

	}

	/**
	 * Return the filter configuration object for this filter.
	 */
	public FilterConfig getFilterConfig() {
		return (this.filterConfig);
	}

	/**
	 * Set the filter configuration object for this filter.
	 * 
	 * @param filterConfig
	 *          The filter configuration object
	 */
	public void setFilterConfig(FilterConfig filterConfig) {
		this.filterConfig = filterConfig;
	}

	/**
	 * Destroy method for this filter
	 */
	public void destroy() {
	}

	/**
	 * Init method for this filter
	 */
	public void init(FilterConfig filterConfig) {
		this.filterConfig = filterConfig;
		if (filterConfig != null) {
			log("ControlFilter:Initializing filter");
		}
	}

	/**
	 * Return a String representation of this object.
	 */
	@Override
	public String toString() {
		if (filterConfig == null) {
			return ("ControlFilter()");
		}
		StringBuffer sb = new StringBuffer("ControlFilter(");
		sb.append(filterConfig);
		sb.append(")");
		return (sb.toString());
	}

	@SuppressWarnings("unused")
	private void sendProcessingError(Throwable t, ServletResponse response) {
		String stackTrace = getStackTrace(t);

		if (stackTrace != null && !stackTrace.equals("")) {
			try {
				response.setContentType("text/html");
				PrintStream ps = new PrintStream(response.getOutputStream());
				PrintWriter pw = new PrintWriter(ps);
				pw.print("<html>\n<head>\n<title>Error</title>\n</head>\n<body>\n"); // NOI18N

				// PENDING! Localize this for next official release
				pw.print("<h1>The resource did not process correctly</h1>\n<pre>\n");
				pw.print(stackTrace);
				pw.print("</pre></body>\n</html>"); // NOI18N
				pw.close();
				ps.close();
				response.getOutputStream().close();
			} catch (Exception ex) {
			}
		} else {
			try {
				PrintStream ps = new PrintStream(response.getOutputStream());
				t.printStackTrace(ps);
				ps.close();
				response.getOutputStream().close();
			} catch (Exception ex) {
			}
		}
	}

	public static String getStackTrace(Throwable t) {
		String stackTrace = null;
		try {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			t.printStackTrace(pw);
			pw.close();
			sw.close();
			stackTrace = sw.getBuffer().toString();
		} catch (Exception ex) {
		}
		return stackTrace;
	}

	public void log(String msg) {
		filterConfig.getServletContext().log(msg);
	}
}
