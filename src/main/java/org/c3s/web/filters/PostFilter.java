/**
 * 
 */
package org.c3s.web.filters;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.c3s.query.HttpUtils;
import org.c3s.query.ParametersHolder;
import org.c3s.query.RequestType;
import org.c3s.query.UploadedFile;
import org.c3s.storage.StorageFactory;
import org.c3s.storage.StorageType;
import org.c3s.utils.FileSystem;
import org.c3s.web.context.ApplicationContext;
import org.c3s.web.interfaces.Context;
import org.c3s.web.servlets.constants.ServletConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author azajac
 *
 */
public class PostFilter implements Filter {

	private static Logger logger = LoggerFactory.getLogger(PostFilter.class);
	
	@SuppressWarnings("unused")
	private FilterConfig filterConfig = null;
	/**
	 * 
	 */
	protected String file_size;
	protected String upload_path;
	protected Context ctx;
	
	public PostFilter() {
		ctx = ApplicationContext.getInstance();
		file_size = ServletConstants.UPLOAD_MAX_FILESIZE.get(ctx);
		upload_path = ServletConstants.UPLOAD_DIRECTORY.get(ctx);
	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}
	
	/* (non-Javadoc)
	 * @see javax.servlet.Filter#destroy()
	 */
	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		String method = req.getMethod();

		//PrintWriter out = ((HttpServletResponse)response).getWriter();
		
		if (method.equals("POST")) {
			if (!ServletFileUpload.isMultipartContent(req)) {
				
				String query = req.getReader().readLine();
				/*
				out.println(">>>>>");
				out.println(query);
				out.println(">>>>>");
				ctx.set_Post(HttpUtils.parseQueryString(query, req.getCharacterEncoding()));
				*/
				ParametersHolder<String> post = HttpUtils.parseQueryString(query, req.getCharacterEncoding());
				StorageFactory.getStorage(StorageType.REQUEST).put(RequestType.POST, post);
				
				StorageFactory.getStorage(StorageType.REQUEST).put(RequestType.REQUEST, post);
				//request.setAttribute("$_POST", post);
				
			} else {
				File dir = FileSystem.newFile(upload_path);
				dir.mkdirs();
				
				DiskFileItemFactory factory = new DiskFileItemFactory();
				factory.setRepository(dir);
				
				ServletFileUpload upload = new ServletFileUpload(factory);
				
				try {
				
					List<FileItem> list = upload.parseRequest(req);
					
					ParametersHolder<String> post = new ParametersHolder<String>();
					ParametersHolder<UploadedFile> files = new ParametersHolder<UploadedFile>();
					for (FileItem item : list) {
						if (item.isFormField()) {
							post.put(item.getFieldName(), item.getString());
						} else {
							UploadedFile file;
							if (item.isInMemory()) {
								file = new UploadedFile(dir, item);
								item.write(file.getUploadedFile());
							} else {
								file = new UploadedFile((DiskFileItem)item);
							}
							files.put(file.getFieldName(), file);
						}
					}
					
					if (post.size() > 0) {
						StorageFactory.getStorage(StorageType.REQUEST).put(RequestType.POST, post);
						StorageFactory.getStorage(StorageType.REQUEST).put(RequestType.REQUEST, post);						
					}
					
					if (files.size() > 0) {
						StorageFactory.getStorage(StorageType.REQUEST).put(RequestType.FILE, files);
					}
					
				} catch (Exception e) {
					logger.error("Upload error", e);
				}
			}
		}
		
		if (method.equals("POST") || method.equals("GET")) {
			String query = req.getQueryString();
			ParametersHolder<String> get = HttpUtils.parseQueryString(query, req.getCharacterEncoding());
			StorageFactory.getStorage(StorageType.REQUEST).put(RequestType.GET, get);
			if (StorageFactory.getStorage(StorageType.REQUEST).get(RequestType.REQUEST) == null) {
				StorageFactory.getStorage(StorageType.REQUEST).put(RequestType.REQUEST, get);
			} else {
				((ParametersHolder<String>)StorageFactory.getStorage(StorageType.REQUEST).get(RequestType.REQUEST)).putAll(get);
			}
		}
		/*
		 * 
		 */
		chain.doFilter(request, response);
	}


}
