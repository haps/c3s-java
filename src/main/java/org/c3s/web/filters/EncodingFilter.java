package org.c3s.web.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.c3s.web.context.PageRequestContext;
import org.c3s.web.servlets.constants.ServletConstants;

public class EncodingFilter implements Filter {

	@SuppressWarnings("unused")
	private FilterConfig filterConfig = null;
	
	private String in_encoding;
	private String out_encoding;

	public EncodingFilter() {
	}

	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
		
		in_encoding = ServletConstants.ENCODING_INPUT.get(PageRequestContext.getInstance());
		out_encoding = ServletConstants.ENCODING_OUTPUT.get(PageRequestContext.getInstance());
		
		if (in_encoding == null) {
			in_encoding = out_encoding;
		}
		
		req.setCharacterEncoding(in_encoding);
		resp.setCharacterEncoding(out_encoding);
		//System.out.println("Before");
		chain.doFilter(req, resp);
		//System.out.println("After");
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}

}
