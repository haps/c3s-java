/**
 * 
 */
package org.c3s.web.security;


/**
 * @author admin
 *
 */
public interface SecurityProvider<T> {
	
	public T autificate();
	
	public boolean isAutificated();  
}
