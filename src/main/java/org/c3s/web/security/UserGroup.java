package org.c3s.web.security;

public abstract class UserGroup {
	
	private String name;
	private String uniq;

	
	public String getName() {
		return name;
	}

	public UserGroup setName(String name) {
		this.name = name;
		return this;
	} 

	public String getUniq() {
		return uniq;
	}

	public UserGroup setUniq(String uniq) {
		this.uniq = uniq;
		return this;
	} 
	
}
