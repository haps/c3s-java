package org.c3s.web.interfaces;

import java.util.Map;

import org.c3s.query.UploadedFile;

public interface Context {
	
	public String getApplicationRoot();	
	public String getApplicationPath();
	public String getProperty(String name, String default_value);
	public String getProperty(String name);
	public void setProperty(String name, String value);
	public Map<String, Object> getProperties();
	
	public String _get(String name);
	public String _post(String name);
	public UploadedFile _files(String name);
	
}
