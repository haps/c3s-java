package org.c3s.web.modules.security;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.c3s.annotations.Parameter;
import org.c3s.annotations.ParameterPost;
import org.c3s.db.jpa.ManagerJPA;
import org.c3s.dispatcher.Dispatcher;
import org.c3s.dispatcher.InjectiveDispatcher;
import org.c3s.dispatcher.RedirectControlerInterface;
import org.c3s.dispatcher.exceptions.SkipSubLevelsExeption;
import org.c3s.storage.StorageFactory;
import org.c3s.storage.StorageType;
import org.c3s.utils.Utils;
import org.c3s.web.redirect.DropRedirect;
import org.c3s.web.security.entity.Role;
import org.c3s.web.security.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Autificate {

	private static Logger logger = LoggerFactory.getLogger(Autificate.class);	
	
	private static final String SAVED_USER = "session_saved_user"; 
	
	private EntityManager em = ManagerJPA.get();  
	
	public void getAutification(@ParameterPost("login") String login, 
			@ParameterPost("password") String password, 
			@ParameterPost("save") Boolean isSaved,
			Dispatcher dispatcher) {
		
		logger.debug(login);
		logger.debug(password);
		
		User user = null;
		if (login != null && password != null) {
			//user = (User)em.createNamedQuery("User.autificate").setParameter("login", login).setParameter("password", password).getSingleResult();
			user = (User)em.createNamedQuery("User.autificate").setParameter("login", login).getSingleResult();
			if (user != null && !user.getPassword().equals(Utils.MD5(user.getSalt() + password))) {
				user = null; 
			}
			//logger.debug("User name: {}", user.getLogin());
		} else {
			user = (User) StorageFactory.getStorage(StorageType.SESSION).get(SAVED_USER);
		}
		
		if (user != null) {
			StorageFactory.getStorage(StorageType.SESSION).put(SAVED_USER, user);
			if (dispatcher instanceof InjectiveDispatcher) {
				((InjectiveDispatcher)dispatcher).getInjector().setNamedData(User.class.getName(), user);
			}
		}
	}

	public void checkAutification(User user, @Parameter("role") String[] roles, RedirectControlerInterface redirect) throws SkipSubLevelsExeption {
		
		boolean pass = false;
		
		//User user =	(User) StorageFactory.getStorage(StorageType.SESSION).get(SAVED_USER);
		
		logger.debug("Saved user: {}", user);
		
		if (user != null && roles != null) {
			List<Role> userRoles = user.getRoles();
			List<String> rolesNames = new ArrayList<String>();
			for(Role role: userRoles) {
				rolesNames.add(role.getName());
			}
			for(String role: roles) {
				logger.debug("User {} check role {}", user.getLogin(), role); 
				if (rolesNames.contains(role)) {
					pass = true;
					break;
				}
			}
		}
		
		/**
		if (!pass) {
			redirect.setRedirect(new DropRedirect());
			throw new SkipSubLevelsExeption();
		}
		**/
	}
				
}
