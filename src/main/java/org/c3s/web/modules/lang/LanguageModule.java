package org.c3s.web.modules.lang;

import java.util.List;
import java.util.Map;

import org.c3s.annotations.CurrentUrl;
import org.c3s.annotations.Parameter;
import org.c3s.content.ContentObject;
import org.c3s.dispatcher.UrlPart;
import org.c3s.dispatcher.exceptions.ForwardException;
import org.c3s.dispatcher.modules.Singleton;
import org.c3s.exceptions.XMLException;
import org.c3s.site.LanguageInterface;
import org.c3s.site.GeneralSite;
import org.c3s.site.GeneralSite.Politics;
import org.c3s.site.SiteLoader;
import org.c3s.web.redirect.DirectRedirect;
import org.c3s.xml.utils.XMLUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

public class LanguageModule implements Singleton {

	private static Logger logger = LoggerFactory.getLogger(LanguageModule.class);
	
	private GeneralSite site = null;
	
	public void processSuffix(UrlPart workUrl) throws ForwardException {
		logger.debug("Process suffix...");
		if (site == null) {
			//site = SiteLoader.get().getSite();
			site = SiteLoader.getSite();
			if (site.getLanguagePolitic() == GeneralSite.Politics.LANGUAGE_SUFFIX) {
				logger.debug("Site politics: " + site.getLanguagePolitic());
				//String reminder = getDispatcher().getWorkUrl().getUrlReminder();
				String reminder = workUrl.getUrlReminder();
				String[] last = reminder.split("/");
				if (last.length > 0 && last[0].length() > 0) {
					LanguageInterface lang = site.getLanguageBySuffix(last[0]);
					if (lang != null) {
						site.setCurrentLanguage(lang);
						String forward = reminder.substring(last[0].length());
						throw new ForwardException(new DirectRedirect(forward));
					}
				}
			}
		}
		//ContentObject.getInstance().setFixedParameters(((Language)site.getCurrentLanguage()).__toArray());
		ContentObject.getInstance().setFixedParameters(site.getLanguageInfo());
	}
	
	/**
	 * @throws XMLException 
	 * 
	 */
	@SuppressWarnings("unchecked")
	public void getLanguageSwitch(@CurrentUrl String url, @Parameter("tag") String tag,  
			@Parameter("template") String template) throws XMLException {
		
		//long start = System.currentTimeMillis();
		
		Map<String, Object> info = site.__toArray();
		
		//logger.debug("1: {}", Math.floor(((System.currentTimeMillis() - start)/1000f)*10000)/10000);
		
		for (Map<String, Object> lang: (List<Map<String, Object>>)info.get("languages")) {
			if (site.getLanguagePolitic() != Politics.LANGUAGE_NONE) {
				StringBuffer href = new StringBuffer();
				if (site.getLanguagePolitic() == Politics.LANGUAGE_PREFIX) {
					href.append("http://");
					if (!(Boolean)lang.get("default")) {
						href.append((String)lang.get("prefix"));
					}
					href.append(site.getCurrentAlias().getAlias());
					href.append(site.getCurrentAlias().getWorkPattern());
					href.append(url);
				} else if (site.getLanguagePolitic() == Politics.LANGUAGE_SUFFIX) {
					//href.append(site.getCurrentAlias().getAlias());
					href.append(site.getCurrentAlias().getWorkPattern());
					if (!(Boolean)lang.get("default")) {
						href.append("/");
						href.append((String)lang.get("suffix"));
					}
					href.append(url);
				}
				lang.put("href", href.toString());
			}
		}
		
		//logger.debug("2: {}", Math.floor(((System.currentTimeMillis() - start)/1000f)*10000)/10000);
		
		Document dom = XMLUtils.arrayToXml(info, false);
		
		//logger.debug("\n" + XMLUtils.xml2out(dom));
		//logger.debug("3: {}", Math.floor(((System.currentTimeMillis() - start)/1000f)*10000)/10000);
		
		
		ContentObject.getInstance().setData(tag, dom, template);
	}
}
