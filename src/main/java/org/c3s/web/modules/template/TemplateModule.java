/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.c3s.web.modules.template;

import java.io.File;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.c3s.annotations.Parameter;
import org.c3s.dispatcher.Dispatcher;
import org.c3s.dispatcher.modules.Singleton;
import org.c3s.utils.FileSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author admin
 */
public class TemplateModule implements Singleton {

	private static final Logger logger = LoggerFactory.getLogger(TemplateModule.class);
	
	protected String template;

	public void setTemplate(@Parameter("template") String template) throws Exception {
		//if (hasParameter("tpl")) {
			//template = getParameter("tpl");
			//Dbg.prn(template);
		this.template = template;
			logger.debug("Set template: " + template);
		//}
	}

	public void showTemplate(ServletRequest req, ServletResponse res, Dispatcher dispatcher) throws Exception  {
		//Dbg.prn("Show Template!!!");
		logger.debug("Is redirected: " + dispatcher.hasRedirect());
		if (!dispatcher.hasRedirect()) {
			logger.debug("Set template");
			//Debug.getInstance().out("Set template", Debug.E_INFO);
			if (template != null) {
				try {
					File tpl = FileSystem.newFile(template);
					if (tpl.exists() && tpl.isFile()) {
						logger.debug("Show template: " + template);
						logger.debug(tpl.getCanonicalPath());
						req.getServletContext().getRequestDispatcher("/" + template).forward(req, res);
					} else {
						logger.error("Template not found: " + tpl.getAbsolutePath());
						res.getWriter().println("file not found");
					}
				} catch (Exception e) {
					Throwable err = e;
					while(err.getCause() != null) {
						err = err.getCause();
					}
					logger.error(err.getMessage(), err);
				}
			} else {
				logger.error("Template not Set!!!");
			}
		}
	}
}
