package org.c3s.web.modules.db;

import java.util.Properties;

import org.c3s.db.DBManager;
import org.c3s.dispatcher.modules.BaseModule;

public class DBInitializer extends BaseModule {
	public void createConnection() {
		try {
			String name = getParameter("name");
			String driver = getParameter("driver");
			String dsn = getParameter("dsn");
			String user = getParameter("user");
			String password = getParameter("password");
			
			Properties props = new Properties();
			
			props.put("user", user);
			props.put("password", password);
			
			DBManager.getConnection(name, driver, dsn, props);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
