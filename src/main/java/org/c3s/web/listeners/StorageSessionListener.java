/**
 * 
 */
package org.c3s.web.listeners;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.c3s.storage.StorageFactory;
import org.c3s.storage.StorageType;
import org.c3s.web.storage.SessionStorage;

/**
 * @author azajac
 *
 */
public class StorageSessionListener implements HttpSessionListener {

	@Override
	public void sessionCreated(HttpSessionEvent se) {
		((SessionStorage)StorageFactory.getStorage(StorageType.SESSION)).registerSession(se.getSession());
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		((SessionStorage)StorageFactory.getStorage(StorageType.SESSION.getName())).destroySession(se.getSession());
	}

}
