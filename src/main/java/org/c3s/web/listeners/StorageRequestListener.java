package org.c3s.web.listeners;

import java.io.IOException;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;

import org.c3s.query.ParametersHolder;
import org.c3s.query.RequestType;
import org.c3s.query.UploadedFile;
import org.c3s.storage.StorageFactory;
import org.c3s.storage.StorageType;
import org.c3s.web.storage.RequestStorage;
import org.c3s.web.storage.SessionStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StorageRequestListener implements ServletRequestListener {
	
	private static Logger logger = LoggerFactory.getLogger(StorageRequestListener.class);
	
	@Override
	public void requestDestroyed(ServletRequestEvent sre) {
		@SuppressWarnings("unchecked")
		ParametersHolder<UploadedFile> uploaded = (ParametersHolder<UploadedFile>) StorageFactory.getStorage(StorageType.REQUEST).get(RequestType.FILE);
		if (uploaded != null) {
			
			for (String key: uploaded.getParameterMap().keySet()) {
				Object[] files = uploaded.getParameterMap().get(key);
				for (Object object: files) {
					UploadedFile file = (UploadedFile)object;
					if (file.getInputStream() != null) {
						try {
							file.getInputStream().close();
						} catch (IOException e) {
							logger.error(e.getMessage(), e);;
						}
					}
					file.getUploadedFile().delete();
				}
			}
		}
			
		((RequestStorage)StorageFactory.getStorage(StorageType.REQUEST)).destroyRequest(sre.getServletRequest());
	}

	@Override
	public void requestInitialized(ServletRequestEvent sre) {
		
		((RequestStorage)StorageFactory.getStorage(StorageType.REQUEST)).registerRequest(sre.getServletRequest());
		((SessionStorage)StorageFactory.getStorage(StorageType.SESSION)).registerSession(((HttpServletRequest)sre.getServletRequest()).getSession());
		
	}

}
