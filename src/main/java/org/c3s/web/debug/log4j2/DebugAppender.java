package org.c3s.web.debug.log4j2;

import java.io.Serializable;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.layout.PatternLayout;
import org.c3s.web.debug.Debug;

@Plugin(name = "DebugAppender", category = "Core", elementType = "appender", printObject = true)
public class DebugAppender extends AbstractAppender {

	private final ReadWriteLock rwLock = new ReentrantReadWriteLock();
	private final Lock readLock = rwLock.readLock();
	//private final Lock writeLock = rwLock.writeLock();

	private DebugAppender(String name, Filter filter,
			Layout<? extends Serializable> layout) {
		super(name, filter, layout);
		// TODO Auto-generated constructor stub
	}

    @PluginFactory
    public static DebugAppender createAppender(
    		@PluginAttribute("name") String name,
            @PluginAttribute("ignoreExceptions") boolean ignoreExceptions,
            @PluginElement("Layout") Layout<?> layout,
            @PluginElement("Filters") Filter filter) {
    	
        if (name == null) {
            LOGGER.error("No name provided for DebugAppender");
            return null;
        }  
        
        if (layout == null) {
        	layout = PatternLayout.createDefaultLayout();
        }
        return new DebugAppender(name, filter, layout);
    }
	
	
	@Override
	public void append(LogEvent event) {
		readLock.lock();
		try {
			final byte[] bytes = getLayout().toByteArray(event);
			if (bytes.length > 0) {
				Debug.getInstance().out(new String(bytes), Debug.E_ALL);
			}
		} finally {
			readLock.unlock();
		}
	}

}
