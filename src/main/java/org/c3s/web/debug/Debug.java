/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.c3s.web.debug;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.c3s.storage.StorageFactory;
import org.c3s.storage.StorageInterface;
import org.c3s.storage.StorageType;

/**
 * 
 * @author admin
 */
public class Debug {

	public static int E_INFO = 1;
	public static int E_NOTICE = 2;
	public static int E_WARNING = 4;
	public static int E_ERROR = 8;
	public static int E_ALL = E_INFO | E_NOTICE | E_WARNING | E_ERROR;

	public static Debug getInstance() {

		// Thread tr = Thread.currentThread();
		Throwable t = new Throwable();
		StackTraceElement[] st = t.getStackTrace();
		String className = st[0].getClassName();

		StorageInterface storage = StorageFactory.getStorage(StorageType.REQUEST);
		Debug instance = (storage != null)? (Debug)storage.get(className): new Debug();
		if (instance == null) {
			instance = new Debug();
			storage.put(className, instance);
		}
		
		/*
		Debug instance = (Debug) ClassHolder.getObject(className);
		if (instance == null) {
			instance = (Debug) ClassHolder.putObject(new Debug());
		}
		*/
		return instance;
	}

	private List<DebugInfo> info;

	private Debug() {
		info = new ArrayList<DebugInfo>();
	}

	public void out(Object obj) {
		out(obj, Debug.E_INFO);
	}

	public void out(Object obj, int level) {
		if (obj != null) {
			Throwable t = new Throwable();
			StackTraceElement[] st = t.getStackTrace();
			StackTraceElement el = st[1];

			String message = null;
			String traceinfo = null;

			if (obj instanceof Exception) {
				ByteArrayOutputStream str = new ByteArrayOutputStream();
				PrintStream ps = new PrintStream(str);
				((Exception) obj).printStackTrace(ps);
				message = ((Exception) obj).getMessage();
				traceinfo = str.toString();
			} else if (obj instanceof Throwable) {
				ByteArrayOutputStream str = new ByteArrayOutputStream();
				PrintStream ps = new PrintStream(str);
				((Throwable) obj).printStackTrace(ps);
				message = ((Throwable) obj).getMessage();
				traceinfo = str.toString();
			} else if (obj instanceof String) {
				message = (String) obj;
			} else {
				message = obj.toString();
			}

			info.add(new DebugInfo(level, el, message, traceinfo));
		}
	}

	public List<DebugInfo> getInfo() {

		/*
		 * String str = ""; for(int i=0; i < info.size(); i++) { str +=
		 * "<pre>\n"; //str += buff.get(i).toString(); str += "</pre>\n"; }
		 * System.out.println(str); return str;
		 */
		return info;
	}

	public class DebugInfo {
		private int error = 0;
		private StackTraceElement callerClass;
		private String errorMessage;
		private String traceInfo;

		public DebugInfo(int error, StackTraceElement callerClass,
				String errorMessage, String traceInfo) {
			this.error = error;
			this.callerClass = callerClass;
			this.errorMessage = errorMessage;
			this.traceInfo = traceInfo;
		}

		/**
		 * @return the error
		 */
		public int getError() {
			return error;
		}

		/**
		 * @return the callerClass
		 */
		public StackTraceElement getCallerClass() {
			return callerClass;
		}

		/**
		 * @return the errorMessage
		 */
		public String getErrorMessage() {
			return errorMessage;
		}

		/**
		 * @return the traceInfo
		 */
		public String getTraceInfo() {
			return traceInfo;
		}

	}

}
