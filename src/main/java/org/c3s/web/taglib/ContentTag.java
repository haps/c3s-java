/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.c3s.web.taglib;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import org.c3s.content.ContentObject;
import org.c3s.web.debug.Debug;

/**
 *
 * @author admin
 */
public class ContentTag extends ParametriziedTag {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private int random = ContentObject.CONTENT_LAST;
	private String htmlId;

	@Override
	public int doStartTag() throws JspException {
		return EVAL_BODY_INCLUDE;
	}

	@Override
	public int doEndTag() throws JspException {

		ContentObject content = ContentObject.getInstance();
		JspWriter out = pageContext.getOut();

		try {
			if (htmlId == null) {
				out.println(content.getData(name, getParameters(), random));
			} else {
				out.println(content.getData(name, getParameters(), random, htmlId));
			}
		} catch (Exception e) {
			//e.printStackTrace();
			Debug.getInstance().out(e, Debug.E_ERROR);
		}

		return EVAL_PAGE;
	}


	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param random the random to set
	 */
	public void setRandom(String random) {
		this.random = random.equalsIgnoreCase("true")?ContentObject.CONTENT_RANDOM:(random.equalsIgnoreCase("array")?ContentObject.CONTENT_ARRAY:ContentObject.CONTENT_LAST);
	}

	/**
	 * @param htmlId the htmlId to set
	 */
	public void setHtmlId(String htmlId) {
		this.htmlId = htmlId;
	}
	
	private Map<String, Object> getParametersMap() {
		Map<String, Object> map = new HashMap<String, Object>();
		for (Object key: getParameters().keySet()) {
			map.put((String) key, getValue((String)key));
		}
		return map;
	}
}
