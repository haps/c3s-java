/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.c3s.web.taglib;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.jsp.tagext.BodyTagSupport;

/**
 *
 * @author admin
 */
public abstract class ParametriziedTag extends BodyTagSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Map<String, Object> parameters = new HashMap<String, Object>();
	public void setParameter(String name, String value) {
		getParameters().put(name, value);
	}

	/**
	 * @return the parameters
	 */
	public Map<String, Object> getParameters() {
		return parameters;
	}

}
