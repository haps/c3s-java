/**
 * 
 */
package org.c3s.web.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import org.c3s.content.ContentObject;
import org.c3s.web.debug.Debug;

/**
 * @author azajac
 *
 */
public class TitleTag extends ParametriziedTag {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3252034210432406545L;

	public int doStartTag() throws JspException {
		return EVAL_BODY_INCLUDE;
	}

	@Override
	public int doEndTag() throws JspException {

		ContentObject content = ContentObject.getInstance();
		JspWriter out = pageContext.getOut();

		try {
			out.print(content.getTitle());
		} catch (Exception e) {
			//e.printStackTrace();
			Debug.getInstance().out(e, Debug.E_ERROR);
		}
		return EVAL_PAGE;
	}
	
}
