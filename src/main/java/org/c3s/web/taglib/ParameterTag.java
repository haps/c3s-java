/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.c3s.web.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;
import javax.servlet.jsp.tagext.Tag;


/**
 *
 * @author admin
 */
public class ParameterTag extends BodyTagSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7180592729588298543L;
	
	private String name;
	private String value;

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public int doStartTag() throws JspException {
		Tag parent = getParent();
		if (parent instanceof ParametriziedTag) {
			((ParametriziedTag)parent).setParameter(name, value);
		}
		return EVAL_PAGE;
	}

}
