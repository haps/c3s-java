/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.c3s.web.taglib;

import java.io.IOException;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;

import org.c3s.web.debug.Debug;
/**
 *
 * @author admin
 */
public class DebugTag extends BodyTagSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4731939487441886481L;
	private int level;


	@Override
	public int doStartTag() throws JspException {

		try {
			//pageContext.getOut().println("Hahahaha");
			//pageContext.getOut().println(Debug.getInstance().getInfo());
			JspWriter out = pageContext.getOut();
			List<Debug.DebugInfo> info = Debug.getInstance().getInfo();

			String str = "";
			for(int i=0; i < info.size(); i++) {
				Debug.DebugInfo inf = info.get(i);
				if ((inf.getError() & this.level) != 0) {
					String err_msg = inf.getErrorMessage();
					err_msg = (err_msg != null)?err_msg:"Unknown message";
					str += "<div class=\"debug_info_number\">#"+(i+1)+"</div>";
					str += "<div class=\"debug_info_message\"><pre>"+err_msg.replaceAll("\n+$", "")+"</pre></div>";
					str += "<div class=\"debug_info_class\">Class: \""+inf.getCallerClass().getClassName()+"\"</div>";
					if (inf.getTraceInfo() != null) {
						str += "<div class=\"debug_info_trace\"><pre>#"+inf.getTraceInfo()+"</pre></div>";
					}
				}
			}
			if (str.length() != 0) {
				out.print("<div class=\"debug_info\">");
				out.print("<div class=\"debug_info_head\">Debug INFO:</div>");
				out.print(str);
				out.print("</div>");
			}
			//out.println(level);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return EVAL_PAGE;
	}

	/**
	 * @param level the level to set
	 */
	public void setLevel(String level) {
		if (level.equals("E_ALL")) {
			this.level = Debug.E_ALL;
		} else if (level.equals("E_WARNING")) {
			this.level = Debug.E_WARNING;
		} else if (level.equals("E_INFO")) {
			this.level = Debug.E_INFO;
		} else {
			this.level = 0;
		}
	}

}
