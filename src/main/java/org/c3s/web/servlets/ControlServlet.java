/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.c3s.web.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.c3s.actions.ActionMapInterface;
import org.c3s.actions.xml.XMLActionMap;
import org.c3s.content.ContentObject;
import org.c3s.dispatcher.Dispatcher;
import org.c3s.dispatcher.exceptions.ForwardException;
import org.c3s.dispatcher.exceptions.StopDispatchException;
import org.c3s.query.ParametersHolder;
import org.c3s.site.GeneralSite;
import org.c3s.site.SiteLoader;
import org.c3s.storage.StorageFactory;
import org.c3s.storage.StorageType;
import org.c3s.utils.FileSystem;
import org.c3s.web.context.ApplicationContext;
import org.c3s.web.context.PageRequestContext;
import org.c3s.web.redirect.Redirector;
import org.c3s.web.servlets.constants.ServletConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author admin
 */
@SuppressWarnings("serial")
public class ControlServlet extends GenericServlet {

	private static Logger logger = LoggerFactory.getLogger(ControlServlet.class);

	@Override
	public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
		service((HttpServletRequest) request, (HttpServletResponse) response);
	}

	@SuppressWarnings("unchecked")
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		long start_time = new Date().getTime();

		//SiteLoader siteLoader = new SiteLoaderXML().load("sites.xml");
		SiteLoader siteLoader = (SiteLoader) getServletContext().getAttribute(ServletConstants.SITE_LOADER_ATTRIBUTE.getValue());
		siteLoader.registerAtStorage(StorageFactory.getStorage(StorageType.REQUEST));
		
		PrintWriter out = null;		
		GeneralSite site = null;
		try {
			site = siteLoader.getSite(request.getServerName());
		} catch (CloneNotSupportedException e) {
			throw new ServletException(e);
		}   
		
		if (site != null) {
			
			logger.info("Request server: " + request.getServerName());
			logger.info("Context path: " + getServletContext().getServletContextName());
			
			//String start_pattern = ServletConstants.WORK_PATTERN.get(ApplicationContext.getInstance());
			String start_pattern = SiteLoader.getSite().getCurrentAlias().getWorkPattern();
			//start_pattern = request.getContextPath() + start_pattern;
			//start_pattern = Utils.replaceRecursive(start_pattern, "//", "/");
			// start_pattern = start_pattern.substring(0, start_pattern.length()
			// - 1);
			ContentObject.getInstance().setFixedParameters("root", start_pattern);
			
			logger.debug("Start pattern: " + start_pattern);
			logger.debug("Request uri: " + request.getRequestURI());
			
			if (request.getRequestURI().startsWith(start_pattern)) {

				Redirector redirector = Redirector.getInstance();
				redirector.setData(start_pattern, request, response);

				PageRequestContext.getInstance().setApplicationRoot(start_pattern);

				String worked_uri = request.getRequestURI().substring(start_pattern.length());
				if (!worked_uri.startsWith("/")) {
					worked_uri = "/" + worked_uri;
				}

				// System.out.println("-------------------");
				// System.out.println(worked_uri);

				response.setContentType("text/html;charset=UTF-8");
				out = response.getWriter();
				//try {

					/*
					 * if (ClassHolder.getObject("org.c3s.siteinfo.Context") !=
					 * null) { return; }
					 */

					log("Run Control Servlet");

					if (request.getAttribute("$_GET") != null) {
						PageRequestContext.getInstance().set_Get((ParametersHolder<String>) request.getAttribute("$_GET"));
					}
					if (request.getAttribute("$_POST") != null) {
						PageRequestContext.getInstance().set_Post((ParametersHolder<String>) request.getAttribute("$_POST"));
					}

					PageRequestContext.getInstance().setRequest(request);
					PageRequestContext.getInstance().setResponce(response);
					//PageRequestContext.getInstance().setServlet(this);

					// String conf = "config.xml";
					String conf = SiteLoader.getSite().getConfig();
					logger.debug("Current site: " + SiteLoader.getSite().getCurrentAlias() + " [" + SiteLoader.getSite().getName() + "]");
					logger.debug("Site config: " + conf);

					/**
					 * Load Classes
					 */
					Dispatcher dispatcher = null;
					List<Object> injectedObjects = new ArrayList<Object>();
					try {
						//injectedObjects.add(this);
						injectedObjects.add(request);
						injectedObjects.add(response);
						
						Class<?> injectorClass = Class.forName(ServletConstants.CLASS_DISPATCHER_INJECTOR.get(ApplicationContext.getInstance()));
						logger.debug("Load Dispatcher Injector: " + injectorClass.getName());
						injectedObjects.add(injectorClass.newInstance());
						
						Class<?> dispatcherClass = Class.forName(ServletConstants.CLASS_DISPATCHER.get(ApplicationContext.getInstance()));
						logger.debug("Load Dispatcher: " + dispatcherClass.getName());
						Constructor<?> wrkConstructor = null;
						for (Constructor<?> constructor: dispatcherClass.getConstructors()) {
							if (constructor.getParameterTypes().length > 0) {
								wrkConstructor = constructor;
								break;
							}
						}
						
						if (wrkConstructor != null) {
							logger.debug("Count of injected objects: " + injectedObjects.size());
							dispatcher = (Dispatcher)wrkConstructor.newInstance(new Object[] {injectedObjects.toArray(new Object[injectedObjects.size()])});
						} else {
							dispatcher = (Dispatcher)dispatcherClass.newInstance();
						}
						
					} catch (Exception e) {
						throw new ServletException(e);
					}
					
					//disp = new CommonDispatcher();

					boolean continue_proc = true;

					String forward = null;

					while (continue_proc) {
						if (forward == null) {
							dispatcher.processUrl(worked_uri, request.getQueryString());
						} else {
							dispatcher.processUrl(forward);
						}
						if (dispatcher.hasRedirect()) {
							redirector.redirect(dispatcher.getRedirect());
							continue_proc = false;
						} else {
							try {
								//ActionMapInterface map = ActionMapHolder.getActionMap(FileSystem.newFile(conf));
								ActionMapInterface map = new XMLActionMap(FileSystem.newFile(conf));
								dispatcher.dispatch(map);
								continue_proc = false;
								/*
								 * Redirect
								 */
								if (dispatcher.hasRedirect()) {
									redirector.redirect(dispatcher.getRedirect());
								}
							} catch (ForwardException fwd) {
								forward = fwd.getForward().getUrl();
							} catch (StopDispatchException fwd) {
								continue_proc = false;
							} catch (Exception e) {
								continue_proc = false;
								throw new ServletException(e);
							}
						}
					}

					//ClassHolder.release();
					/** } finally {
					long run_time = new Date().getTime() - start_time;
					System.out.println("Servlet running at: " + ((float) run_time / 1000) + " sec");
					out.close();
				}
				*/
			}
		} else {
			throw new ServletException("Site \"" + request.getServerName() + "\" not configured");
		}
		long run_time = new Date().getTime() - start_time;
		System.out.println("Servlet running at: " + ((float) run_time / 1000) + " sec");
		if (out != null) {
			out.close();
		}
		
	}

}
