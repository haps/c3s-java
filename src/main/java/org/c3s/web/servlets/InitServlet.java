/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.c3s.web.servlets;

import java.sql.SQLException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.c3s.content.ContentObject;
import org.c3s.content.transformers.Transformer;
import org.c3s.db.DBManager;
import org.c3s.site.SiteLoader;
import org.c3s.storage.StorageFactory;
import org.c3s.storage.StorageInterface;
import org.c3s.storage.StorageType;
import org.c3s.utils.FileSystem;
import org.c3s.web.context.ApplicationContext;
import org.c3s.web.interfaces.Context;
import org.c3s.web.servlets.constants.ServletConstants;

/**
 *
 * @author admin
 */
@SuppressWarnings("serial")
public class InitServlet extends HttpServlet {
	
	public static String STORAGE_PREFIX = "storage.type.";
	public static String TEMPLATE_TRANSFORMER_PREFIX = "template.transformer.class.";
	public static String CONNECTION_PREFIX = "connection.";

	private static Logger logger = LoggerFactory.getLogger(InitServlet.class);

	Context applicationContext = ApplicationContext.getInstance();
	
	@Override
	public void init() throws ServletException {
		
		super.init();
		//getServletContext().
		
		log("Start Init Servlet");
		
		logger.info("Start Init Servlet");

		/**
		 * Set work directory to root
		 */
		//System.setProperty("user.dir", getServletContext().getRealPath(""));
		FileSystem.setServletContext(getServletContext());
		
		/**
		 * context-param
		 */
		Enumeration<String> names = (Enumeration<String>)getServletContext().getInitParameterNames();
		while(names.hasMoreElements()) {
			String name = names.nextElement();
			logger.debug("Add context parameter: " + name + " {" + getServletContext().getInitParameter(name) + "}");
			//System.out.println("Add context parameter: " + name + " {" + getServletContext().getInitParameter(name) + "}");
			applicationContext.setProperty(name, getServletContext().getInitParameter(name));
		}
		
		/**
		 * servlet init param
		 */
		names = (Enumeration<String>)getInitParameterNames();
		while(names.hasMoreElements()) {
			String name = names.nextElement();
			logger.debug("Add init parameter: " + name + " {" + getInitParameter(name) + "}");
			applicationContext.setProperty(name, getInitParameter(name));
		}

		loadConnections();
		loadStorage();
		loadTransformers();
		loadSiteLoader();
		loadClasses();
		
		
		logger.info("Finish Init Servlet");
		
		/*
		logger.info("\n" + new File(getServletContext().getRealPath("") + "/xslt/menu.xsl").getAbsolutePath() + 
				"\n" + new File("xslt/menu.xsl").getAbsolutePath() + "\n");
		
		logger.info("\n" + new File(getServletContext().getRealPath("") + "/xslt/menu.xsl").exists() + 
				"\n" + new File("xslt/menu.xsl").exists() + "\n" + 
				new File(new File("xslt/menu.xsl").getAbsolutePath()).exists() + "\n" );
		*/
	}

	protected void loadStorage() throws ServletException {
		String prefix = STORAGE_PREFIX;
		for(String name: applicationContext.getProperties().keySet()) {
			if (name.startsWith(prefix)) {
				String suffix = name.substring(prefix.length());
				String className = applicationContext.getProperty(name);
				
				try {
					StorageFactory.register(StorageType.valueOf(suffix), (StorageInterface)Class.forName(className).newInstance());
					logger.info("Load " + suffix + " storage: {" + className + "}");
				} catch (Exception e) {
					logger.error("Fail to load " + suffix + " storage: {" + className + "}");
					throw new ServletException("Fail to load " + suffix + " storage: {" + className + "}", e);
				}
				
			}
		}
	}
	
	protected void loadTransformers() throws ServletException {
		String prefix = TEMPLATE_TRANSFORMER_PREFIX;
		for(String name: applicationContext.getProperties().keySet()) {
			if (name.startsWith(prefix)) {
				String className = applicationContext.getProperty(name);
				try {
					ContentObject.registerTransformer((Transformer)Class.forName(className).newInstance());
					logger.info("Load template transformer: {" + className + "}");
				} catch (Exception e) {
					e.printStackTrace();
					logger.error("Fail to load template transformer: {" + className + "}");
					throw new ServletException("Fail to load template transformer: {" + className + "}", e);
				}
			}
		}
	}
	
	protected void loadClasses() throws ServletException {
	}
	
	
	protected void loadSiteLoader() throws ServletException {
		try {
			@SuppressWarnings("unchecked")
			Class<SiteLoader> loaderClass = (Class<SiteLoader>) Class.forName(ServletConstants.SITE_LOADER_CLASS.get(applicationContext));
			SiteLoader loader = loaderClass.newInstance();
			loader.load(ServletConstants.SITE_LOADER_PARAMETER.get(applicationContext));
			getServletContext().setAttribute(ServletConstants.SITE_LOADER_ATTRIBUTE.getValue(), loader);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Fail to load siteloader class: {" + ServletConstants.SITE_LOADER_CLASS.get(applicationContext) + "}");
			throw new ServletException("Fail to load siteloader class: {" + ServletConstants.SITE_LOADER_CLASS.get(applicationContext) + "}", e);
		}
	}
	
	protected void loadConnections() throws ServletException {
		Map<String, Map<String, Object>> holder = new HashMap<String, Map<String,Object>>();
		String prefix = CONNECTION_PREFIX;
		for(String name: applicationContext.getProperties().keySet()) {
			if (name.startsWith(prefix)) {
				String sub = name.substring(prefix.length());
				String[] pars = sub.split("\\.", 2);
				if (pars.length == 2) {
					appendConncetionPartameter(holder, pars[0], pars[1], applicationContext.getProperties().get(name));
				}
			}
		}
		
		for(String name: holder.keySet()) {
			Map<String, Object> props = holder.get(name);
			Properties properties = new Properties();
			String driver = null;
			String dsn = null;
			for(String prop: props.keySet()) {
				if ("driver".equals(prop)) {
					driver = props.get(prop).toString();
				} else if ("dsn".equals(prop)) {
					dsn = props.get(prop).toString();
				} else {
					properties.put(prop, props.get(prop));
				}
			}
			try {
				DBManager.getConnection(name, driver, dsn, properties);
			} catch (SQLException e) {
				new ServletException(e);
			}
		}
	}
	
	protected void appendConncetionPartameter(Map<String, Map<String, Object>> holder, String connection, String parameter, Object value) {
		if (!holder.containsKey(connection)) {
			holder.put(connection, new HashMap<String, Object>());
		}
		holder.get(connection).put(parameter, value);
	}
}
