package org.c3s.web.servlets.constants;

import org.c3s.web.interfaces.Context;

public enum ServletConstants {

	/**
	 * Classes
	 */
	
	CLASS_DISPATCHER("class.dispatcher", "org.c3s.dispatcher.CommonDispatcher"),
	CLASS_DISPATCHER_INJECTOR("class.dispatcher.injector", "org.c3s.dispatcher.injectors.CommonClassInjector"),
	/*
	 * File Uploader 
	 */
	UPLOAD_DIRECTORY("directory.upload", "/tmp"),
	UPLOAD_MAX_FILESIZE("upload.max_filesize", "2M"),
	MAX_POSTSIZE("upload.max_postsize", "2M"),
	/*
	 * Encoding
	 */
	ENCODING_INPUT("encoding.input", null),
	ENCODING_OUTPUT("encoding.output", "utf-8"),
	/*
	 * Application
	 */
	WORK_PATTERN("application.pattern", "/"),
	
	/**
	 * SiteLoader 
	 */
	SITE_LOADER_CLASS("site.loader.class", "org.c3s.site.AnySiteLoader"),
	SITE_LOADER_PARAMETER("site.loader.parameter", null),
	SITE_LOADER_ATTRIBUTE("site.loader.attribute", "__store_site_loader_to_context"),
	;
	private String name = "";
	private String value = "";
	
	private ServletConstants(String name, String value) {
		this.name = name;
		this.value = value;
	}
	
	public String getName() {
		return name;
	}
	
	public String getValue() {
		return value;
	}
	
	public String get(Context ctx) {
		String ret = null;
		ret = ctx.getProperty(name, value);
		return ret;
	}
}
