/**
 * 
 */
package org.c3s.web.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.c3s.site.GeneralSite;
import org.c3s.site.SiteLoader;
import org.c3s.web.servlets.constants.ServletConstants;

/**
 * @author alexeyz
 *
 */
public class FileServlet extends GeneralFileServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -549008824790483362L;

	@Override
	protected String getRootPattern(HttpServletRequest request) throws ServletException {
		SiteLoader siteLoader = (SiteLoader) getServletContext().getAttribute(ServletConstants.SITE_LOADER_ATTRIBUTE.getValue());
		
		GeneralSite site = null;
		String startPattern = null;
		if (siteLoader != null) {
			try {
				site = siteLoader.getSite(request.getServerName());
			} catch (CloneNotSupportedException e) {
				throw new ServletException(e);
			}
		}
		
		if (site != null) {
			startPattern = site.getCurrentAlias().getWorkPattern();
		} else {
			throw new ServletException("Site \"" + request.getServerName() + "\" not configured");
		}
		
		return startPattern;
	}
}
