/**
 * 
 */
package org.c3s.web.servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tika.Tika;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.sax.BodyContentHandler;
import org.c3s.utils.FileSystem;
import org.c3s.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

/**
 * @author alexeyz
 *
 */
public class GeneralFileServlet extends GenericServlet {

	private static final String parameterDirectories = "directories";  
	private static final String parameterSeparator = "separator";  
	private static final String etagRequest = "If-None-Match";  
	private static final int bufferSize = 1 << 16;
	private static final Tika tika = new Tika(); 
	/**
	 * 
	 */
	private static final long serialVersionUID = -7000185815227106840L;
	
	private static Logger logger = LoggerFactory.getLogger(GeneralFileServlet.class);

	
	/**
	 * 
	 */
	private File[] folders;  
	@Override
	public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
		service((HttpServletRequest) request, (HttpServletResponse) response);
	}

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		prepareFolders();
		
		File result = null;
		
		String startPattern = this.getRootPattern(request);
		
		logger.debug("Start pattern \"{}\"", startPattern);
		
		if (startPattern != null) {
			logger.debug("Request URI  \"{}\"", request.getRequestURI());
			if (request.getRequestURI().startsWith(startPattern)) {
				String resource = request.getRequestURI().substring(startPattern.length());
				logger.debug("Resource \"{}\"", resource);
				if (resource.startsWith("/")) {
					resource = resource.substring(1);
				}
				
				for (File folder: folders) {
					logger.debug("Folder \"{}\"", folder.getCanonicalPath());
					File file = new File(folder, resource);
					if (file.exists() && file.isFile()) {
						result = file;
						break;
					}
				}
			}
		} else {
			throw new ServletException("Cann't get root pattern for site \"" + request.getServerName() + "\"");
		}
		
		/**
		 * Send file
		 */
		
		if (result == null) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		} else {
			/**
			 * Count Etag
			 */
			String etag = "s3s//\"" + Utils.MD5(result.getName() + result.length() + result.lastModified()) + "\"";
			if (request.getHeader(etagRequest) != null && request.getHeader(etagRequest).equals(etag)) {
				response.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
			} else {
				/**
				 * Get File Info
				 */
				InputStream inputStream = new FileInputStream(result);
				Metadata metadata = new Metadata();
				ParseContext parseContext = new ParseContext();
				ContentHandler contentHandler = new BodyContentHandler(-1);
				AutoDetectParser parser = new AutoDetectParser();
				try {
					parser.parse(inputStream, contentHandler, metadata, parseContext);
				} catch (IOException e) {
					logger.error(e.getMessage(), e);
				} catch (SAXException e) {
					logger.error(e.getMessage(), e);
				} catch (TikaException e) {
					logger.error(e.getMessage(), e);
				} finally {
					inputStream.close();
				}
				/**
				 * Send Header
				 */
				String contentType = metadata.get(Metadata.CONTENT_TYPE);
				if (contentType.indexOf("text/plain") == 0) {
					contentType = tika.detect(result);
				}
				
				response.setStatus(HttpServletResponse.SC_OK);
				response.setHeader("Accept-Ranges", "bytes");
				response.setHeader("Content-Length", String.valueOf(result.length()));
				response.setHeader("Content-Type", contentType);
				response.addDateHeader("Last-Modified", result.lastModified());
				response.setHeader("ETag", etag);
				
				/** 
				 * Send Body 
				 */
				if (!request.getMethod().equalsIgnoreCase("head")) {
					inputStream = new FileInputStream(result);
					OutputStream outputStream = response.getOutputStream();
					byte[] buffer = new byte[bufferSize];
					int read = 0;
					try {
						while ((read = inputStream.read(buffer)) != -1) {
							outputStream.write(buffer, 0, read);
							outputStream.flush();
						}
					} finally {
						outputStream.close();
						inputStream.close();
					}
				}
				/**
				 * 
				 */
			}
		}
	}
	
	protected void prepareFolders() {
		if (folders == null) {
			List<File> list = new ArrayList<File>();
			list.add(FileSystem.newFile("."));
			String separator = getInitParameter(parameterSeparator);
			String directories = getInitParameter(parameterDirectories);
			if (directories != null) {
				String[] paths = directories.split(Utils.nullOrEmpty(separator)?File.pathSeparator:separator);
				for (String path: paths) {
					if (!path.trim().isEmpty()) {
						File directory = FileSystem.newFile(path.trim());
						list.add(directory);
					}
				}
			}
			folders = list.toArray(new File[]{}); 
		}
	}
	
	protected String getRootPattern(HttpServletRequest request) throws ServletException {
		String startPattern = getServletContext().getContextPath();
		return startPattern;
	}
}
