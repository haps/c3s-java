package org.c3s.web.context;

import java.util.HashMap;
import java.util.Map;

import org.c3s.query.UploadedFile;
import org.c3s.web.interfaces.Context;

public class ApplicationContext implements Context {

	private static ApplicationContext instance = null;
	
	public synchronized static ApplicationContext getInstance() {
		if (instance == null) {
			synchronized (ApplicationContext.class) {
				instance = new ApplicationContext();
			}
		}
		return instance;
	}
	
	@Override
	public String _get(String name) {
		return null;
	}

	@Override
	public String _post(String name) {
		return null;
	}

	@Override
	public UploadedFile _files(String name) {
		return null;
	}
	
	@Override
	public String getApplicationRoot() {
		return null;
	}
	
	Map<String, Object> props = new HashMap<String, Object>();
	
	public void setProperty(String name, String value) {
		props.put(name, value);
	}
	
	public String getProperty(String name) {
		return getProperty(name, null);
	}
	
	public String getProperty(String name, String default_value) {
		String value = (String)props.get(name);
		if (value == null) {
			value = default_value;
		}
		return value;
	}

	public Map<String, Object> getProperties() {
		return props;
	}
	
	@Override
	public String getApplicationPath() {
		return null;
	}
	
}
