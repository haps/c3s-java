/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.c3s.web.context;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.c3s.core.ClassHolder;
import org.c3s.query.ParametersHolder;
import org.c3s.query.UploadedFile;
import org.c3s.site.LanguageInterface;
import org.c3s.site.GeneralSite;
import org.c3s.web.interfaces.Context;

/**
 *
 * @author admin
 */
public class PageRequestContext implements Context {

	Context global = ApplicationContext.getInstance();
	
	protected PageRequestContext() {
		//GlobalContext ctx = GlobalContext.getInstance();
	}
	
	public static PageRequestContext getInstance() {
		Throwable t = new Throwable();
		StackTraceElement[] st = t.getStackTrace();
		String className = st[0].getClassName();

		PageRequestContext instance = (PageRequestContext)ClassHolder.getObject(className);
		if (instance == null) {
			instance = (PageRequestContext)ClassHolder.putObject(new PageRequestContext());
		}
		return instance;
	}

	/*
	 *
	 * Application run parameters
	 * 
	 */
	private String root = null;
	public String getApplicationRoot() {
		return root;
	}
	
	public void setApplicationRoot(String app_root) {
		root = app_root;
	}
	/*
	 *
	 * 
	 */

	private GeneralSite site;
	/**
	 * @return the site
	 */
	public GeneralSite getSite() {
		return site;
	}

	/**
	 * @param site the site to set
	 */
	public void setSite(GeneralSite site) {
		this.site = site;
	}

	/*
	 *
	 *
	 */

	private LanguageInterface lang;
	/**
	 * @return the site
	 */
	public LanguageInterface getLang() {
		return lang;
	}

	/**
	 * @param site the site to set
	 */
	public void setLang(LanguageInterface lang) {
		this.lang = lang;
	}

	private HttpServletRequest request;

	/**
	 * @return the request
	 */
	public HttpServletRequest getRequest() {
		return request;
	}

	/**
	 * @param request the request to set
	 */
	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public String getApplicationPath() {
		return servlet.getServletContext().getRealPath("");
	}
	

	private HttpServletResponse responce;

	/**
	 * @return the responce
	 */
	public HttpServletResponse getResponce() {
		return responce;
	}

	/**
	 * @param responce the responce to set
	 */
	public void setResponce(HttpServletResponse responce) {
		this.responce = responce;
	}


	private HttpServlet servlet;

	/**
	 * @return the servlet
	 */
	public HttpServlet getServlet() {
		return servlet;
	}

	/**
	 * @param servlet the servlet to set
	 */
	public void setServlet(HttpServlet servlet) {
		this.servlet = servlet;
	}

	/*
	 * 
	 */
	
	Map<String, Object> props = new HashMap<String, Object>();
	
	public void setProperty(String name, String value) {
		props.put(name, value);
	}
	
	public String getProperty(String name) {
		return getProperty(name, null);
	}
	
	public String getProperty(String name, String default_value) {
		String value = (String)props.get(name);
		if (value == null) {
			value = default_value;
		}
		return value;
	}

	public Map<String, Object> getProperties() {
		return props;
	}
	
	/*
	 * _GET _POST _FILES from request
	 */
	
	ParametersHolder<String> __get = new ParametersHolder<String>();
	ParametersHolder<String> __post = new ParametersHolder<String>();
	ParametersHolder<UploadedFile> __files = new ParametersHolder<UploadedFile>();
	
	/*
	 * (non-Javadoc)
	 * @see org.c3s.core.interfaces.IContext#_get(java.lang.String)
	 */

	public void set_Get(ParametersHolder<String> __get) {
		this.__get = __get;
	}
	
	@Override
	public String _get(String name) {
		return __get.getParameter(name);
	}
	
	public String[] _getValues(String name) {
		return __get.getParameterValues(name);
	}

	public void set_Post(ParametersHolder<String> __post) {
		this.__post = __post;
	}
	
	@Override
	public String _post(String name) {
		return __post.getParameter(name);
	}
	
	public String[] _postValues(String name) {
		return __post.getParameterValues(name);
	}

	public void set_Files(ParametersHolder<UploadedFile> __files) {
		this.__files = __files;
	}
	
	@Override
	public UploadedFile _files(String name) {
		return __files.getParameter(name);
	}
	
	public UploadedFile[] _filesValues(String name) {
		return __files.getParameterValues(name);
	}
}
