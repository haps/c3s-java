package org.c3s.web.controllers;

import org.c3s.annotations.ParameterRequest;
import org.c3s.annotations.StoredObject;
import org.c3s.dispatcher.RedirectControlerInterface;
import org.c3s.dispatcher.exceptions.StopDispatchException;
import org.c3s.dispatcher.injectors.InjectorUtils;
import org.c3s.query.ParametersHolder;
import org.c3s.query.RequestStoredData;
import org.c3s.query.RequestType;
import org.c3s.storage.StorageFactory;
import org.c3s.storage.StorageType;
import org.c3s.web.redirect.DirectRedirect;

public class PassthrowPostController {

	public void processPost(RedirectControlerInterface redirect,
			@ParameterRequest ParametersHolder<String> holder,
			@StoredObject(storage = StorageType.SESSION, name = "__fid", request = RequestType.REQUEST) RequestStoredData storedData) throws StopDispatchException {
		
		String url = holder.getParameter("__url");
		if (url != null) {
			if (storedData != null) {
				storedData.setRequestData(holder.getTreeParameter());
				String name = InjectorUtils.getAssociatedClassMethodName(storedData.getActionClass(), storedData.getActionMethod());
				StorageFactory.getStorage(StorageType.SESSION).put(name, storedData);
			}
			redirect.setRedirect(new DirectRedirect(url));
			throw new StopDispatchException();
		} else {
			storedData.remove();
		}
	}
	
}
