package org.c3s.web.controllers;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.c3s.classinfo.ClassInformationManager;
import org.c3s.dispatcher.injectors.ClassInjectorInterface;
import org.c3s.storage.StorageFactory;
import org.c3s.storage.StorageInterface;
import org.c3s.storage.StorageType;

public class PassthrowPostController1 {

	/**
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected boolean isFromPost() {
		boolean result = false;
		StorageInterface storage = StorageFactory.getStorage(StorageType.SESSION);
		if (storage.get("prepost") != null) {
			result = ((Map<String, Object>)storage.get("prepost")).get(this.getClass().getName()) != null;
		}
		return result;
	}
	
	
	/**
	 * @param methodName
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	protected String registerSubmitter(String methodName, Object parameters) throws Exception {
		String uniq = "form_" + UUID.randomUUID();
		String clazz = this.getClass().getName();
		StorageInterface storage = StorageFactory.getStorage(StorageType.SESSION);
	
		if (ClassInformationManager.getClassInformation(this.getClass()).getMethod(methodName) == null) {
			//throw new MethodInvocationException(message, e, methodName, templateName, lineNumber, columnNumber)
			throw new Exception("Method \"" + methodName + "\" not exists at class \"" + clazz + "\"");
		}
		
		if (storage.get("beans") == null) {
			storage.put("beans", new HashMap<String, Object>());
		}
		if (storage.get("submitters") == null) {
			storage.put("submitters", new HashMap<String, Object>());
		}
		if (storage.get("classes") == null) {
			storage.put("classes", new HashMap<String, Object>());
		}
		((Map<String, Object>)storage.get("beans")).put(uniq, parameters);
		((Map<String, Object>)storage.get("submitters")).put(uniq, methodName);
		((Map<String, Object>)storage.get("classes")).put(uniq, clazz);
		
		return uniq;
	}

	
	/**
	 * @param uniq
	 */
	@SuppressWarnings("unchecked")
	public void unregisterSubmitter(String uniq) {
		StorageInterface storage = StorageFactory.getStorage(StorageType.SESSION);
		((Map<String, Object>)storage.get("beans")).remove(uniq);
		((Map<String, Object>)storage.get("submitters")).remove(uniq);
		((Map<String, Object>)storage.get("classes")).remove(uniq);
	}
	
	
	/**
	 * @param uniq
	 * @param injector
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public void submit(ClassInjectorInterface injector) throws Exception {
		String clazz = this.getClass().getName();
		StorageInterface storage = StorageFactory.getStorage(StorageType.SESSION);
		/*
		if (uniq != null) {
			if (storage.get("submitters") != null && clazz.equals((String)((Map<String, Object>)storage.get("submitters")).get(uniq))) {
				Object parameters = ((Map<String, Object>)storage.get("beans")).get(uniq);
				String methodName = (String)((Map<String, Object>)storage.get("submitters")).get(uniq);
		
				injector.setNamedData(AnyObject.class.getName(), parameters);
				
				injector.invokeMethod(methodName, this);
			}
			unregisterSubmitter(uniq);
		}
		*/
	}
	
}
