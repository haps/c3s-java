package org.c3s.web.controllers;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.c3s.annotations.AnyObject;
import org.c3s.annotations.ParameterPost;
import org.c3s.dispatcher.injectors.ClassInjectorInterface;
import org.c3s.storage.StorageFactory;
import org.c3s.storage.StorageInterface;
import org.c3s.storage.StorageType;

public class DirectPostController {

	protected String registerSubmitter(String methodName) {
		return registerSubmitter(methodName, null);
	}
	
	@SuppressWarnings("unchecked")
	protected String registerSubmitter(String methodName, Object parameters) {
		String uniq = "form_" + UUID.randomUUID();
		String clazz = this.getClass().getName();
		StorageInterface storage = StorageFactory.getStorage(StorageType.SESSION);
		
		if (storage.get("beans") == null) {
			storage.put("beans", new HashMap<String, Object>());
		}
		if (storage.get("submitters") == null) {
			storage.put("submitters", new HashMap<String, Object>());
		}
		if (storage.get("classes") == null) {
			storage.put("classes", new HashMap<String, Object>());
		}
		((Map<String, Object>)storage.get("beans")).put(uniq, parameters);
		((Map<String, Object>)storage.get("submitters")).put(uniq, methodName);
		((Map<String, Object>)storage.get("classes")).put(uniq, clazz);
		
		return uniq;
	}

	@SuppressWarnings("unchecked")
	public void unregisterSubmitter(String uniq) {
		StorageInterface storage = StorageFactory.getStorage(StorageType.SESSION);
		((Map<String, Object>)storage.get("beans")).remove(uniq);
		((Map<String, Object>)storage.get("submitters")).remove(uniq);
		((Map<String, Object>)storage.get("classes")).remove(uniq);
	}
	
	@SuppressWarnings("unchecked")
	public void submit(@ParameterPost("_uniq") String uniq, ClassInjectorInterface injector) throws Exception {
		String clazz = this.getClass().getName();
		StorageInterface storage = StorageFactory.getStorage(StorageType.SESSION);
		if (uniq != null) {
			if (storage.get("submitters") != null && clazz.equals((String)((Map<String, Object>)storage.get("submitters")).get(uniq))) {
				Object parameters = ((Map<String, Object>)storage.get("beans")).get(uniq);
				String methodName = (String)((Map<String, Object>)storage.get("submitters")).get(uniq);
		
				injector.setNamedData(AnyObject.class.getName(), parameters);
				
				injector.invokeMethod(methodName, this);
			}
			unregisterSubmitter(uniq);
		}
	}
}
