package org.c3s.web.controllers;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.fileupload.FileUpload;
import org.c3s.annotations.ParameterFile;
import org.c3s.annotations.ParameterGet;
import org.c3s.annotations.ParameterPost;
import org.c3s.dispatcher.RedirectControlerInterface;
import org.c3s.dispatcher.exceptions.StopDispatchException;
import org.c3s.query.ParametersHolder;
import org.c3s.storage.StorageFactory;
import org.c3s.storage.StorageInterface;
import org.c3s.storage.StorageType;
import org.c3s.web.redirect.DirectRedirect;

public class PrePostController {
	@SuppressWarnings("unchecked")
	public void doPost(@ParameterPost ParametersHolder<String> postData, @ParameterGet ParametersHolder<String> getData,
			@ParameterFile ParametersHolder<FileUpload> filesData,	RedirectControlerInterface redirect) throws StopDispatchException {
	
		String url = postData.getParameter("_url");
		String uniq = postData.getParameter("_uniq");
		
		StorageInterface storage = StorageFactory.getStorage(StorageType.SESSION);
		
		if (url != null && uniq != null) {
			if (storage.containsKey("classes") && ((Map<String, Object>)storage.get("classes")).containsKey(uniq)) {
				String clazz = (String)((Map<String, Object>)storage.get("classes")).get(uniq);
				if (!storage.containsValue("prepost")) {
					storage.put("prepost", new HashMap<String, Object>());
				}
				Map<String, Object> link = (Map<String, Object>)((Map<String, Object>)storage.get("prepost")).put(clazz, new HashMap<String, Object>());
				link.put("_uniq", uniq);
				link.put("_post", postData);
				link.put("_get", getData);
				//link.put("_files", filesData);
				redirect.setRedirect(new DirectRedirect(url));
				throw new StopDispatchException();
			}
		}
	}
}
