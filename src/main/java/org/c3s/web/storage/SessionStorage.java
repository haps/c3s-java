package org.c3s.web.storage;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpSession;

import org.c3s.storage.AbstractStorage;
//import javax.ses;

public class SessionStorage extends AbstractStorage {

	private static Map<HttpSession, Set<ComparableTread>> sessionToThreads = new HashMap<HttpSession, Set<ComparableTread>>();
	private static Map<Thread, HttpSession> threadToSession = new HashMap<Thread, HttpSession>();
	
	private static String SESSION_NAME = SessionStorage.class.getName();
	
	public void registerSession(HttpSession session) {
		Thread thread = Thread.currentThread();
		threadToSession.put(thread, session);
		Set<ComparableTread> threads = sessionToThreads.get(session);
		if (threads == null) {
			threads = new TreeSet<ComparableTread>();
			sessionToThreads.put(session, threads);
		}
		threads.add(new ComparableTread(thread));
	}
	
	public void destroySession(HttpSession session) {
		Set<ComparableTread> threads = sessionToThreads.get(session);
		if (threads != null) {
			for (ComparableTread thread : threads) {
				threadToSession.remove(thread.getThread());
			}
		}
		sessionToThreads.remove(session);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected Map<Object, Object> getMap() {
		Thread thread = Thread.currentThread();
		Map<Object,Object> map = null;
		HttpSession session = threadToSession.get(thread);
		if (session != null) {
			String name =  SESSION_NAME + ".map";
			map = (Map<Object, Object>)session.getAttribute(name);
			if (map == null) {
				map = new LinkedHashMap<Object, Object>();
				session.setAttribute(name, map);
			}
		}
		/*
		*/
		return map;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected Map<Object, Long> getTimes() {
		Thread thread = Thread.currentThread();
		Map<Object,Long> times = null;
		HttpSession session = threadToSession.get(thread);
		if (session != null) {
			String name =  SESSION_NAME + ".times";
			times = (Map<Object, Long>)session.getAttribute(name);
			if (times == null) {
				times = new LinkedHashMap<Object, Long>();
				session.setAttribute(name, times);
			}
		}
		/*
		*/
		return times;
	}

	
}
