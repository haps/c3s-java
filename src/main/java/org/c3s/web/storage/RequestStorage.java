package org.c3s.web.storage;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.ServletRequest;

import org.c3s.storage.AbstractStorage;

public class RequestStorage extends AbstractStorage {

	private static Map<ServletRequest, Set<ComparableTread>> requestToThreads = new HashMap<ServletRequest, Set<ComparableTread>>();
	private static Map<Thread, ServletRequest> threadToRequest = new HashMap<Thread, ServletRequest>();
	
	private static String REQUEST_NAME = RequestStorage.class.getName();
	
	public void registerRequest(ServletRequest request) {
		Thread thread = Thread.currentThread();
		threadToRequest.put(thread, request);
		Set<ComparableTread> threads = requestToThreads.get(request);
		if (threads == null) {
			threads = new TreeSet<ComparableTread>();
			requestToThreads.put(request, threads);
		}
		threads.add(new ComparableTread(thread));
	}
	
	public void destroyRequest(ServletRequest request) {
		Set<ComparableTread> threads = requestToThreads.get(request);
		if (threads != null) {
			for (ComparableTread thread : threads) {
				threadToRequest.remove(thread.getThread());
			}
		}
		requestToThreads.remove(request);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected Map<Object, Object> getMap() {
		Thread thread = Thread.currentThread();
		Map<Object,Object> map = null;
		ServletRequest request = threadToRequest.get(thread);
		if (request != null) {
			String name =  REQUEST_NAME + ".map";
			map = (Map<Object, Object>)request.getAttribute(name);
			if (map == null) {
				map = new LinkedHashMap<Object, Object>();
				request.setAttribute(name, map);
			}
		}
		/*
		*/
		return map;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected Map<Object, Long> getTimes() {
		Thread thread = Thread.currentThread();
		Map<Object,Long> times = null;
		ServletRequest request = threadToRequest.get(thread);
		if (request != null) {
			String name =  REQUEST_NAME + ".times";
			times = (Map<Object, Long>)request.getAttribute(name);
			if (times == null) {
				times = new LinkedHashMap<Object, Long>();
				request.setAttribute(name, times);
			}
		}
		/*
		*/
		return times;
	}


}
