/**
 * 
 */
package org.c3s.web.storage;

/**
 * @author azajac
 *
 */
public class ComparableTread implements Comparable<ComparableTread> {

	private Thread thread;
	/**
	 * 
	 */
	public ComparableTread(Thread thread) {
		this.thread = thread;
	}

	
	public Thread getThread() {
		return thread;
	}


	@Override
	public int compareTo(ComparableTread o) {
		int result = -1;
		long tid = thread.getId();
		long oid = o.getThread().getId();
		if (tid > oid) {
			result = 1;
		} else if (tid == oid) {
			result = 0;
		}
		return result;
	}

}
