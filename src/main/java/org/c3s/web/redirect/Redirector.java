/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.c3s.web.redirect;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.c3s.redirect.RedirectInterface;
import org.c3s.storage.StorageFactory;
import org.c3s.storage.StorageType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author admin
 */
public class Redirector {
	
	@SuppressWarnings("unused")
	private static Logger logger = LoggerFactory.getLogger(Redirector.class);
	
	private String root = "";
	private HttpServletRequest request;
	private HttpServletResponse response;	
	
	public void setData(String root, HttpServletRequest request, HttpServletResponse response) {
		this.root = root.endsWith("/")?root.substring(0, root.length()-1):root;
		this.request = request;
		this.response = response;
	}
	
	public static Redirector getInstance() {
		Redirector instance = (Redirector)StorageFactory.getStorage(StorageType.REQUEST).get(Redirector.class);
		if (instance == null) {
			instance = new Redirector();
			StorageFactory.getStorage(StorageType.REQUEST).put(Redirector.class, instance);
		}
		return instance;		
	}
	
	public void redirect(RedirectInterface redirect) throws IOException {
		String url = redirect.getUrl();
		if (url != null) {
			
			//logger.info("Root is: {}", root);
			url = root + url;
			
			//logger.info("Redirect to: {}", url);
			//HttpServletRequest req = PageRequestContext.getInstance().getRequest();
			String chk_url = (new DirectRedirect(request.getRequestURI(), request.getQueryString())).getUrl();
			if (!url.equals(chk_url)) {
				response.sendRedirect(url);
			}
		}
	}
}
