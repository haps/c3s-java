/**
 * 
 */
package org.c3s.web.redirect;

import java.net.URI;
import java.net.URISyntaxException;

import org.c3s.dispatcher.PatternerInterface;
import org.c3s.redirect.RedirectInterface;

/**
 * @author user7517
 *
 */
public class RelativeRedirect implements RedirectInterface {

	//private static Logger logger = LoggerFactory.getLogger(RelativeRedirect.class);
	
	private String url;
	/**
	 * 
	 */
	public RelativeRedirect(String url, PatternerInterface patterner) {
		this(url, patterner.getActionUrl(), null);
	}
	
	public RelativeRedirect(String url, PatternerInterface patterner, String hiddenPefix) {
		this(url, patterner.getActionUrl(), hiddenPefix);
	}

	public RelativeRedirect(String url, String baseUrl, String hiddenPefix) {
		try {
			URI baseUri = new URI(hiddenPefix != null?hiddenPefix + baseUrl : baseUrl);
			URI urls = baseUri.resolve(url);
			String query = urls.getQuery();
			String path = urls.getPath();
			this.url = path + (query != null? "?" + query: "");
		} catch (URISyntaxException e) {
			this.url = "/";
		}
		//logger.debug("Redirect to: " + this.url);
	}

	/* (non-Javadoc)
	 * @see org.c3s.redirect.RedirectInterface#getUrl()
	 */
	@Override
	public String getUrl() {
		return url;
	}

}
