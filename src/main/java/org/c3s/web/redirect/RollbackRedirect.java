/**
 * 
 */
package org.c3s.web.redirect;

import java.util.Arrays;

import org.c3s.redirect.RedirectInterface;
import org.c3s.web.context.PageRequestContext;

/**
 * @author azajac
 *
 */
public class RollbackRedirect implements RedirectInterface {

	private String url = null;
	
	public RollbackRedirect(String current_url, String current_query) {
		if (current_query != null && current_query.length() > 0) {
			url = current_url;
		} else {
			String[] urls = current_url.split("/");
			if (urls.length > 0) {
				String[] new_urls = Arrays.copyOfRange(urls, 0, urls.length - 1);
				url = "/";
				for (String part : new_urls) {
					url = part + "/";
				}
			}
		}
	}
	
	public RollbackRedirect(String current_url) {
		this(current_url, null);
	}
	
	public RollbackRedirect() {
		String current_url = PageRequestContext.getInstance().getRequest().getRequestURI();
		if (current_url != null) {
			
		}
	}
	/* (non-Javadoc)
	 * @see org.c3s.core.redirect.IRedirect#getUrl()
	 */
	@Override
	public String getUrl() {
		return url;
	}

}
