/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.c3s.web.redirect;

import org.c3s.dispatcher.PatternerInterface;
import org.c3s.redirect.RedirectInterface;

/**
 *
 * @author admin
 */
public class DirectRedirect implements RedirectInterface {

	private String url = null;

	public DirectRedirect(String redirect) {
		this(redirect, null);
	}

	public DirectRedirect(String uri, String query) {
		if (query != null) {
			url = uri + ((query.length() != 0)?"?":"") + query;
		} else {
			url = uri;
		}
	}
	
	public DirectRedirect(PatternerInterface dispatcher) {
		this(dispatcher.getCurrentUrl());
	}

	public String getUrl() {
		return url;
	}
}
