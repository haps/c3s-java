/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.c3s.core;

import org.c3s.storage.StorageFactory;
import org.c3s.storage.StorageInterface;
import org.c3s.storage.StorageType;
/**
 *
 * @author admin
 */
public class ClassHolder {

	//private static ThreadStorage storage = new ThreadStorage();
	private static StorageInterface storage = StorageFactory.getStorage(StorageType.REQUEST);
	
	
	public static Object getObject(String name) {
		return storage.get(name);
	}

	public static Object putObject(Object object) {
		return putObject(object, null);
	}
	
	public static Object putObject(Object object, String name) {

		if (name == null) {
			name = object.getClass().getName();
		}
		storage.put(name, object);
		return object;
	}
	
	@Deprecated
	public static void release() {
	}

}
