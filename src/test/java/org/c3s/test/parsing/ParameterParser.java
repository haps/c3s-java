package org.c3s.test.parsing;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import org.c3s.query.HttpUtils;
import org.c3s.query.ParametersHolder;

public class ParameterParser {

	public static void main(String[] args) throws UnsupportedEncodingException {
		String query = "bbb[]=222333&bbb[]=1112223&bbb[]=lalasss";
		ParametersHolder<String> holder = HttpUtils.parseQueryString(query);
		//System.out.print(mapToString(holder.getTreeParameter("bbb[2]"), ""));
		System.out.print(holder.getStringParameter("bbb"));
	}

	@SuppressWarnings("unchecked")
	public static String mapToString(Object map, String tabs) {
		StringBuffer sb = new StringBuffer();
		if (map instanceof Map<?, ?>) {
			//sb.append(tabs);
			sb.append("Map:\n");
			Map<String, Object> smap = (Map<String, Object>)map;
			for(String key : smap.keySet()) {
				sb.append(tabs);
				sb.append("\t");
				sb.append("{" + key + "} -> ");
				sb.append(mapToString(smap.get(key), tabs + "\t"));
				//sb.append("\n");
			}
		} else {
			sb.append(map.toString());
			sb.append("\n");
		}
		return sb.toString();
	}
}
