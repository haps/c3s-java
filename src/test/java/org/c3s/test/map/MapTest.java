package org.c3s.test.map;

import java.io.File;

import org.c3s.actions.ActionMapInterface;
import org.c3s.actions.ActionMapModuleInterface;
import org.c3s.actions.xml.XMLActionMap;

public class MapTest {

	public static void main(String[] args) throws Exception {
		ActionMapInterface map = new XMLActionMap(new File("/Bugs/xml/default.xml"));
		for (ActionMapModuleInterface module: map.getModules()) {
			System.out.print(moduleToString(module, ""));
		}
		//System.out.println(map.getModules());
		//new XMLActionMap(new File("/Bugs/xml/config.xml"));
		// new XMLActionMap(new File("/Bugs/xml/default.xml"));
	}

	public static String moduleToString(ActionMapModuleInterface module, String tabs) {
		StringBuffer sb = new StringBuffer();
		
		sb.append(tabs);

		sb.append("Regexp {" + module.getRegexp() + "}, ");
		
		if (!module.getClassName().isEmpty()) {
			sb.append("Class {" + module.getClassName() + "}, ");
		}
		
		if (!module.getAction().isEmpty()) {
			sb.append("Action {" + module.getAction() + "}, ");
		}
		
		if (!module.getInstanceName().isEmpty()) {
			sb.append("Instance {" + module.getInstanceName() + "}, ");
		}

		sb.append("\n");
		
		for (ActionMapModuleInterface submodule: module.getModules()) {
			sb.append(moduleToString(submodule, tabs + "\t"));
		}
		
		return sb.toString();
	}
}
