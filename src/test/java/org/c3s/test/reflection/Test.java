package org.c3s.test.reflection;

import org.c3s.reflection.XMLReflectionObj;
import org.c3s.xml.utils.XMLUtils;
import org.w3c.dom.Document;


public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {

		Document xml = new XMLReflectionObj(new SimpleClass(), true).toXML();
		
		String out = XMLUtils.saveXML(xml).replace("><", ">\n<");
		System.out.println(out);
		/*
		SimpleClass sc = new SimpleClass();
		
		Document dom = XMLUtils.load("d:/refl.xml");
		
		sc.fromXML(dom);
		
		Document xml = sc.toXML();
		
		String out = XMLUtils.saveXML(xml).replace("><", ">\n<");
		System.out.println(out);
		*/
	}

}
