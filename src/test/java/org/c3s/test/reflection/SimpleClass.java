/**
 * 
 */
package org.c3s.test.reflection;

import java.util.ArrayList;
import java.util.List;

import org.c3s.reflection.XMLList;
import org.c3s.reflection.XMLMap;
import org.c3s.reflection.XMLReflectionObj;
import org.c3s.reflection.annotation.XMLFieldList;
import org.c3s.reflection.annotation.XMLFieldMap;
import org.c3s.reflection.annotation.XMLSimple;

/**
 * @author azajac
 *
 */
public class SimpleClass extends ParentSimple {
	
	/**
	 * 
	 */
	public SimpleClass() {
		// TODO Auto-generated constructor stub
		//field_6.put("1", new Simple2Class("1_1", "1_2"));
		//field_6.put("2", new Simple2Class("2_1", "2_2"));
		
		field_7.add(new Simple2Class("3_1", "3_2"));
	}

	@XMLSimple("Field 1")
	protected int field_1 = 2222;
	
	@XMLSimple("Field 2")
	protected boolean field_2 = false;
	
	@XMLSimple("Field 3")
	protected double field_3 = 222.33D;
	
	protected String field_4 = "field 4";
	
	@XMLSimple("Field 5")
	private String field_5 = "field 5";
	
	/**
	@XMLFieldMap("internal")
	private XMLMap<String, Simple2Class> field_6 = new XMLMap<String, Simple2Class>();
	
	*/
	@XMLFieldList("listing")
	private List<Simple2Class> field_7 = new ArrayList<Simple2Class>();
}
