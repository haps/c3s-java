package org.c3s.test.reflection;

import org.c3s.reflection.XMLReflectionObj;
import org.c3s.reflection.annotation.XMLSimple;

public class Simple2Class extends XMLReflectionObj {

	@XMLSimple("Internal 1")
	protected String inter_1 = "Internal 1";
	@XMLSimple("Internal 2")
	protected String inter_2 = "Internal 2";
	
	public Simple2Class() {
		super();
	};
	
	public Simple2Class(String op1, String op2) {
		inter_1 = op1;
		inter_2 = op2;
	}
}
